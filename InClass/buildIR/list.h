#ifndef LIST_H
#define LIST_H

typedef enum item_kind { pair=47, atom=49 } node_tag; 
typedef struct node_struct* node;

struct node_struct {
  node_tag tag;
  /* Only in atoms */ 
  char* strval;  
  /* Only in cons nodes */ 
  node hd; /* 'car' in Lisp */
  node tl; /* 'cdr' in Lisp */ 
};

/* Representation of empty list is null pointer */ 
extern const node Nil; 

/* New atom */
extern node mk_atom(char* item); 

/* Dotted pair */ 
extern node cons(node item, node list); 

/* Append at end (mutating, but returns modified list) */
extern node append(node item, node list); 

/* Convenience function: Print */
extern void print_list(node n); 

#endif

#include "list.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

/* Representation of empty list is null pointer */ 
const node Nil = (node) 0; 

/* New atom */ 
 node mk_atom(char* item) {
   node new_node = (node) malloc(sizeof(struct node_struct));
   new_node->tag = atom; 
   new_node->strval = strdup(item); 
   return new_node;
 }
   
 /* Cons */
 node cons(node item, node list) {
   node new_node = (node) malloc(sizeof(struct node_struct));
   new_node->tag = pair; 
   new_node->hd = item;
   new_node->tl = list;
   return new_node;
 }

/* Append at end (mutating, but returns modified list) */
node append(node item, node list) {
  if (list == Nil) {
    return cons(item, Nil);
  }
  assert(list->tag == pair); 
  node back = list;
  node cur = list->tl;
  while (cur != Nil) {
    back = cur;
    cur = cur->tl;
  }
  back->tl = cons(item, Nil);
  return list;
}


 void print_list(node n) {
   // fprintf(stdout, "Entered print_list\n"); 
   /* Empty list represented by null pointer */ 
   if (n == Nil) {
     printf("()");
     return;
   }
   /* Non-empty list */
   if (n->tag == pair) {
     // fprintf(stdout, "Dotted pair\n");
     printf("(");
     node el = n; 
     while( el ) {
       print_list(el->hd);
       el = el->tl;
     }
     printf(")");
     return; 
   }
   /* Atom */
   if (n->tag == atom) {
     // fprintf(stdout, "Atom\n");
     printf(" %s ", n->strval);
     return;
   }

   assert(0); 
 }

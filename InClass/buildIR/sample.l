%{
/* Sample lexer
 *
 */ 

#include <stdio.h>
#include "sample.tab.h"

void yyerror (const char *msg) {
   fprintf(stderr,"%d: %s (at '%s')\n", yylineno,  msg, yytext);
}

/* Some long messages that don't fit well in the code below */

const char* BAD_ESC_MSG =
  "Illegal escape code; only \\\\, \\0, \\t, \\n, \\r, \\n are permitted";
const char* BAD_NL_STR =
  "Unclosed string?  Encountered newline in quoted string."; 

extern int yywrap() { return 1; }  /* No nested files for now */

void before_each_action() {
  yylval.strval = yytext; 
  /* printf("Matched in line %d: '%s'\n", yylineno, yytext); */
}
#define YY_USER_ACTION before_each_action();


char msg_buf[1000]; /* A place to create messages with data using sprintf */


#define yyval_capacity 1000

char yyval_buf[yyval_capacity]; /* When yytext is not the token value */
int yyval_index = 0;

YYSTYPE yylval; 

%}

/* NOT using bison-bridge option; it makes YYSTYPE a pointer */ 

%option yylineno
%option warn nodefault
%option header-file="lex.yy.h"

%x comment
%x tripleq

%%

nonempty  { return nonempty; }

[a-zA-Z_][a-zA-Z0-9_]*   { return IDENT; }
[0-9]+                   { return INT_LIT; }

  /* Strings, single and triple-quoted */
  /* Later we may want to strip off the quote marks */ 
["](([\\][0btnrf"\\])|[^"\n\\])*["]     { return STRING_LIT; 
                          }

["](([\\].)|[^"\n\\])*["]     {   yyerror(BAD_ESC_MSG); 
                                  return STRING_LIT;
                              }

["](([\\][^\n])|[^\n"])*\n         { yyerror(BAD_NL_STR); 
                                return STRING_LIT; 
                              }


["]["]["]        { BEGIN tripleq;
		   yyval_index=0;
		   yyval_buf[yyval_index] = '\0'; 
		  }
<tripleq>(([^"])|(["][^"])|(["]["][^"])|\n)*  {
                  strncpy(yyval_buf + yyval_index, yytext,
		  yyval_capacity - yyval_index - 1);
		  yyval_index += yyleng; 
		  // fprintf(stderr, "Adding |%s| to buffer\n", yytext);
		  // fprintf(stderr, "Buffer is now |%s|\n", yyval_buf); 
		  }
<tripleq>["]["]["]  { yylval.strval = yyval_buf; 
                      BEGIN INITIAL; 
                      return STRING_LIT; 
                    }


\.\.\. { return ELLIPSIS; }



[-+*/:.{}=<>();,]  { return yytext[0]; }

  /* Ignore whitespace */
[ \t\n]*  { ; }

   /* Multi-line comments */ 
[/][*]  { BEGIN comment; }
<comment>[^*]*   { ; }
<comment>[*][^/] { ; }
<comment>[*][/]  { BEGIN INITIAL; }

   /* Line end comments */
[/][/].*  { ; }

.  { fprintf(stderr, "*** %d: Unexpected character %d (%c)\n", 
                    yylineno, (int) yytext[0], yytext[0]); }




<<EOF>>  { return EOF; }

%%

/* No main program here */ 


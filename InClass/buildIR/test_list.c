#include "list.h"
#include <stdio.h>
#include <assert.h>

int main(int argc, char** argv) {
  node list;
  node item; 
  list = cons(mk_atom("a"), cons(mk_atom("b"), cons(mk_atom("c"), Nil)));
  printf("Created list, printing\n"); 
  print_list(list);

  printf("Creating nested list\n"); 
  list = cons(list, cons(mk_atom("d"), Nil));
  printf("Created nested list, printing\n"); 
  print_list(list);
  printf("\n"); 

  printf("Appending x to end\n");
  list = append(mk_atom("x"), list);
  printf("Appended x, printing\n"); 
  print_list(list);
  printf("\n");

  printf("appending (w x y) at end\n");
  list = append( cons(mk_atom("w"), 
		      cons(mk_atom("x"),
			   cons(mk_atom("y"), Nil))),
		 list);
  printf("Built appended list\n");
  print_list(list);
  printf("\n");

  printf("Appending to Nil\n");
  list = append(mk_atom("a"), Nil);
  printf("Built, should be ( a )\n");
  print_list(list);
  printf("\n");
  
  return 0;
}

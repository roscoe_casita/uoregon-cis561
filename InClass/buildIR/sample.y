
/* 
 * Samples for IR building in class 
 * 
 */

%{

#include "lex.yy.h"
#include "sample.tab.h"

extern void yyerror(char *msg); /* How is this not in lex.yy.h? */

/* Simple AST form for class discussion */
/* First sample: Linked list */
#include "list.h"

extern node result; 
node result = (node) 0; 
       
%}

%union {
  char* strval; /* Scanner always provides yytext */ 
  struct node_struct* nodeval; 
}


/* Identifiers */
%type <strval> IDENT
%token IDENT
%token ELLIPSIS
%token nonempty

/* Literals */
%type <strval> INT_LIT STRING_LIT
%token INT_LIT STRING_LIT

%type <nodeval> list items item nonempty 

%%

/* *************************************
 * Grammar to build structure from 
 * *************************************
 */ 
pgm: list  { result = $1; } ; 

list: '(' items ')' { $$ = $2; }
    | error list { $$ = Nil; } 
    ;

items:   /* empty */  { $$ = Nil; }
     |   nonempty     { $$ = mk_atom("NON-EMPTY"); }
     ;

/* COMMENTED OUT REST 
 * 
 * nonempty: item { $$ = cons($1, Nil); }
 *      |    nonempty ',' item  { $$ = append($3, $1); }
 *      ;
 *
 *
 * item: IDENT  { $$ = mk_atom($1); }
 *     | ELLIPSIS { $$ = mk_atom("<more>"); }
 *     | list  / * default action * / 
 *    ;
 */

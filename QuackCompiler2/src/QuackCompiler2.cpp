//============================================================================
// Name        : QuackCompiler2.cpp
// Author      : Roscoe Casita and Connor George
// Version     :
// Copyright   : University of Oregon
// Description : Compiler for CIS 561. This build is with complete makefile.

//				This will be used to port the project over so we have support of an IDE.

//============================================================================
/**
 * Driver for parser with scanner.
 * Usage: ./parser foo.qk
 *
 * Output is mainly error messages.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/PassManager.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/raw_ostream.h>
#include "quack.tab.hpp"
#include "ProgramNode.hpp"
#include "QuackStatements.hpp"
#include "QuackExpressions.hpp"
#include "QuackDefines.hpp"
#include "CompilerDB.hpp"
#include "QuackProgramValidator.hpp"
#include "Validation.hpp"

extern int yyparse ();
extern int yydebug;

extern void yyrestart(FILE *f);

extern int yy_flex_debug;

void AddDefaultClassesToProgram();



using namespace llvm;

llvm::Function* createPrintfFunction()
{
    std::vector<llvm::Type*> printf_arg_types;
    printf_arg_types.push_back(llvm::Type::getInt32PtrTy(llvm::getGlobalContext(),0)); //char*

    llvm::FunctionType* printf_type =
        llvm::FunctionType::get(
            llvm::Type::getInt32Ty(llvm::getGlobalContext()), printf_arg_types, true);

    /*llvm::Function *func = llvm::Function::Create(
                printf_type, llvm::Function::ExternalLinkage,
                llvm::Twine("printf"),
                context.module
           );
    func->setCallingConv(llvm::CallingConv::C);
    return func;
    */
    return NULL;
}

void createEchoFunction( llvm::Function* printfFn)
{
    std::vector<llvm::Type*> echo_arg_types;
    echo_arg_types.push_back(llvm::Type::getInt64Ty(llvm::getGlobalContext()));

    llvm::FunctionType* echo_type =
        llvm::FunctionType::get(
            llvm::Type::getVoidTy(llvm::getGlobalContext()), echo_arg_types, false);

/*
 *     llvm::Function *func = llvm::Function::Create(
                echo_type, llvm::Function::InternalLinkage,
                llvm::Twine("echo"),
                context.module
           );
    llvm::BasicBlock *bblock = llvm::BasicBlock::Create(getGlobalContext(), "entry", func, 0);

    //context.pushBlock(bblock);

    const char *constValue = "%d\n";
    llvm::Constant *format_const = llvm::ConstantDataArray::getString(getGlobalContext(), constValue);
    llvm::GlobalVariable *var =
        new llvm::GlobalVariable(
            *context.module, llvm::ArrayType::get(llvm::IntegerType::get(getGlobalContext(), 8), strlen(constValue)+1),
            true, llvm::GlobalValue::PrivateLinkage, format_const, ".str");
    llvm::Constant *zero =
        llvm::Constant::getNullValue(llvm::IntegerType::getInt32Ty(getGlobalContext()));

    std::vector<llvm::Constant*> indices;
    indices.push_back(zero);
    indices.push_back(zero);
    llvm::Constant *var_ref = llvm::ConstantExpr::getGetElementPtr(
	llvm::ArrayType::get(llvm::IntegerType::get(getGlobalContext(), 8), strlen(constValue)+1),
        var, indices);

    std::vector<Value*> args;
    args.push_back(var_ref);

    Function::arg_iterator argsValues = func->arg_begin();
    Value* toPrint = &*argsValues++;
    toPrint->setName("toPrint");
    args.push_back(toPrint);

	CallInst *call = CallInst::Create(printfFn, makeArrayRef(args), "", bblock);
	ReturnInst::Create(getGlobalContext(), bblock);
	context.popBlock();
*/
}

/*void createCoreFunctions(CodeGenContext& context){
	llvm::Function* printfFn = createPrintfFunction(context);
    createEchoFunction(context, printfFn);
}*/



int main(int argc, char **argv)
{
	FILE *f;
	int index;
	yydebug = 0;  // Set to 1 to trace parser
	yy_flex_debug = 0; // Set to 1 to see each rule matched in scanner

	bool function_logging = false;
	bool var_logging = false;
	bool print_dot = false;

	char c;
	while ((c = getopt(argc, argv, "fvtd")) != -1)
	{
		switch(c)
		{
		case 'f':
			function_logging = true;
			break;
		case 'v':
			var_logging = true;
			break;
		case 't':
			fprintf(stderr, "Debugging mode");
			yydebug = 1;
			break;
		case 'd':
			print_dot = true;
			break;

		}
	}

	for (index = optind; index < argc; ++index)
	{
		SET_LOG_FUNC(function_logging);
		SET_ERROR_LOGGING(true);
		SET_VAR_LOGGING(var_logging);
		if( !(f = fopen(argv[index], "r")))
		{
			perror(argv[index]);
			exit(1);
		}
		yyrestart(f);
		VAR_LOG("Beginning parse of " + std::string(argv[index]));

		int condition = yyparse();


		VAR_LOG("Finished parse with result " + INT_STR(condition));
		if (condition == 0)
		{
		}
		else
		{
			return condition;
		}

		CompilerDatabase *database= GetDB();



		VAR_LOG("Before Reducing DB.");
		database->DoReduce();
		VAR_LOG("After Reducing DB.");



		bool result = Validation();


		if(result)
		{
			VAR_LOG("Passed Validation.");
			VAR_LOG("Program passed validation\n");

			PrintVisualization(argv[index],print_dot);
			//	PrintVisualization(argv[index]);
		}
		else
		{
			ERR_LOG("FAILED VALIDATION");
			PrintVisualization(argv[index],false);
		}

				/*
		ProgramNode *root = database->Root;

		if(!database->ValidateClassHierarchy())
		{
			condition = -1;
		}
		*/

		VAR_LOG("Finished parse with result " + INT_STR(condition));

	}




}

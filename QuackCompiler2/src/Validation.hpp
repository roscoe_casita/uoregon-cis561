
#ifndef __VALIDATION__
#define __VALIDATION__

#include "quack.tab.hpp"
#include "ProgramNode.hpp"
#include "QuackStatements.hpp"
#include "QuackExpressions.hpp"
#include "QuackDefines.hpp"
#include "CompilerDB.hpp"
#include "QuackProgramValidator.hpp"


bool Validation();
void PrintVisualization(std::string base_file_name,bool valid);

ReturnStatement *GenReturnThis();
ReturnStatement *GenEmptyStringReturn();
ReturnStatement *GenFalseValueReturn();
ReturnStatement *GenNothingValueReturn();
ReturnStatement *GenZeroValueReturn();

MethodClass* GenerateConstructor(std::string class_name);
MethodClass* GenerateDefaultMethod(std::string method_name, std::string parameter_type, std::string return_type, ReturnStatement *return_stmt);

ClassClass* GenerateObjectClass();
ClassClass* GenerateIntegerClass();
ClassClass* GenerateBooleanClass();
ClassClass* GenerateNothingClass();
ClassClass* GenerateStringClass();

void AddDefaultClassesToProgram();



#endif
#ifndef QUACKDEFINES_HPP
#define QUACKDEFINES_HPP

#include "ProgramNode.hpp"
#include "QuackExpressions.hpp"
#include "QuackStatements.hpp"






/*

*/
class MethodClass: public GraphVizNodeDecorator
{
public:
	MethodClass(SyntaxNode* ptr);
	~MethodClass();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	void 	Reduce();
	void	Print();
	bool 	TypeCheck(SymbolTable *SymTab);


public:
	NamedType								*NameReturn;
	std::vector<NamedType*>					Arguments;
	std::map<std::string,std::string>		VariablesTypes;	  //NOTE: It will probably be helpful to populate this ONLY with top-level variables.  I.E. if a variable only exists in a while loop, it shouldn't be here.  I think this will make type checking easier with scope
	ControlStatement						*Statements;  //NOTE: I think we can change this from a Control Statement to a Sequential Statement
	SyntaxNode								*NearestSyntax;
	//ClassClass								*Parent;
	bool									UpdatedType;
};


/*

*/
class ClassClass: public GraphVizNodeDecorator
{
public:
	ClassClass(SyntaxNode* ptr);
	~ClassClass();

	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

	void	Reduce() ;
	MethodClass* LookupMethod(std::string method_name);

	void	Print();
	bool	TypeCheck(SymbolTable *SymTab);
	

public:
	SyntaxNode							*NearestSyntax;
	NamedType 							*TypeName;

	std::string							ParentName;
	ClassClass							*ParentClass;
	std::vector<MethodClass *>			Methods;
	std::map<std::string,std::string>	MemberVariables;

	MethodClass							*MethodConstructor;

};

#endif


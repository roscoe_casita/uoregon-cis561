#include "QuackDefines.hpp"
#include "QuackProgramValidator.hpp"



MethodClass::MethodClass(SyntaxNode* ptr)
{
	FUNC_LOG("MethodClass::MethodClass(SyntaxNode* ptr)");
	NearestSyntax = ptr;
	NameReturn =0;
	Statements = 0;
	UpdatedType = false;
}

MethodClass::~MethodClass()
{
}


std::vector<GraphVizNodeDecorator *> MethodClass::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> MethodClass::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "record";

	std::stringstream ss;
	
	ss << " {  <F0> "  << NameReturn->Name << " " << NameReturn->Type ;

	for(auto arg = Arguments.begin(); arg != Arguments.end(); arg++)
	{
		if(*arg!=NULL)
		{
			ss << "|" << (*arg)->Name + " : " + (*arg)->Type + "," ;
			returnValue.push_back(*arg);
		}
	}
	returnValue.push_back(Statements);
	ss << "} ";

	NodeLabel = ss.str();

	
	return returnValue;
}

void 	MethodClass::Reduce() 
{
	FUNC_LOG("void 	MethodClass::Reduce()");
	ControlStatement::DoReduce(Statements);
}

bool MethodClass::TypeCheck(SymbolTable *SymTab)
{
	FUNC_LOG("bool MethodClass::TypeCheck(SymbolTable *SymTab: " + this->NameReturn->Name + " : " + this->NameReturn->Type);



	for(auto arg = Arguments.begin(); arg != Arguments.end(); arg++)
	{
		std::string type_lookup = SymTab->Lookup((*arg)->Name);

		if(type_lookup.compare(TOP_STRING)==0)
		{
			SymTab->SymTable[(*arg)->Name] = (*arg)->Type;
		}
		else
		{
			std::string new_type = GetLowestCommonAncestor(type_lookup, (*arg)->Type);

			if(new_type.compare((*arg)->Type) !=0)
			{
				ERR_LOG("Input Argument \"" + (*arg)->Name+ "\" is already defined as a type of :" + (*arg)->Type, this->NearestSyntax);
				return false;
			}
		}
	}


	for(auto arg = VariablesTypes.begin(); arg != VariablesTypes.end(); arg++)
	{
		std::string type_lookup = SymTab->Lookup((*arg).first);

		if(type_lookup.compare(TOP_STRING)==0)
		{
			SymTab->SymTable[ (*arg).first ] = (*arg).second;
 		}
		else
		{
			std::string new_type = GetLowestCommonAncestor(type_lookup, (*arg).second);

			if(new_type.compare(type_lookup) !=0)
			{
				ERR_LOG("Invalid Type Mismatch between Outer symbol table \""+ type_lookup +"\" and usage inside method \"" + (*arg).second+ "\" as derived type is : " + new_type,this->NearestSyntax);
				return false;
			}
		}
	}

	if(!Statements->TypeCheck(SymTab, this))
	{
		ERR_LOG("Method failed to typecheck statements.",this->NearestSyntax);
		return false;
	}
	return true;
}


void MethodClass::Print()
{
	FUNC_LOG("void MethodClass::Print()");

	std::stringstream ss;


	ss << "def " << NameReturn->Name << "(" ;


	for(auto param = Arguments.begin(); param != Arguments.end(); param++)
	{

		if((*param) != NULL)
		{
			ss << (*param)->Name << " : " << (*param)->Type << " , ";
		}
	}
	ss<< ") : " << NameReturn->Type;

	VAR_LOG(ss.str());
	VAR_LOG("{");
	VAR_ADD();
	if(Statements!=NULL)
	{
		Statements->Print();
	}
	VAR_SUB();

	VAR_LOG("}");
}



ClassClass::ClassClass(SyntaxNode* ptr)
{
	FUNC_LOG("ClassClass::ClassClass(SyntaxNode* ptr)");
	TypeName =new NamedType(ptr,ptr->TextMatch,ptr->TextMatch); 
	NearestSyntax=ptr;
	ParentName = OBJ_STRING;
	MethodConstructor = NULL;
	ParentClass =NULL;

}
ClassClass::~ClassClass() 
{
}

std::vector<GraphVizNodeDecorator *> ClassClass::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> ClassClass::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "record";

	std::stringstream ss;
	
	ss << " { class "  << TypeName->Name << " : "<< ParentName << " "  ;


	ss  << " | " << MethodConstructor->NameReturn->Name << " ";

	returnValue.push_back(MethodConstructor);

	for(auto method = Methods.begin(); method != Methods.end(); method++)
	{
		if((*method) != NULL)
		{
			ss  << " | " << (*method)->NameReturn->Name << " ";
			returnValue.push_back(*method);
		}
	}

	ss << " } " ;
	NodeLabel = ss.str();

	return returnValue;
}

MethodClass* ClassClass::LookupMethod(std::string method_name)
{
	FUNC_LOG("MethodClass* ClassClass::LookupMethod(std::string method_name)");
	MethodClass *returnValue = NULL;

	if(0==this->MethodConstructor->NameReturn->Name.compare(method_name))
	{
		returnValue = this->MethodConstructor;
	}
	else
	{
		for(auto name_func= Methods.begin(); name_func != Methods.end(); name_func ++)
		{
			if((*name_func)->NameReturn->Name.compare(method_name)==0)
			{
				returnValue = (*name_func);
			}
		}
	}
	return returnValue;
}


void	ClassClass::Reduce() 
{
	FUNC_LOG("void	ClassClass::Reduce() ");
	for(auto name_func = Methods.begin(); name_func != Methods.end(); name_func ++)
	{
		if((*name_func)!=NULL)
		{
			(*name_func)->Reduce();
		}
	}
	MethodConstructor->Reduce();
}



bool ClassClass::TypeCheck(SymbolTable *SymTab)
{
	FUNC_LOG("bool ClassClass::TypeCheck(SymbolTable *SymTab) :   " + this->TypeName->Name + " :-> " + this->ParentName);


	// here we need to check the method again if the symbol tabled changed


	SymbolTable *construct = SymTab->Duplicate();
	bool done = false;
	while(!done)
	{
		MethodConstructor->UpdatedType = false;
		if(MethodConstructor->TypeCheck(construct))
		{
			if(true ==MethodConstructor->UpdatedType)
			{
				// type added or changed,
				// run type check on method again.
			}
			else
			{
				// no type changed.
				done = true;
			}
		}
		else
		{
			ERR_LOG("Failed to Type Check Constructor: " , MethodConstructor->NearestSyntax);
			delete construct;
			return false;
		}
	}

	for(auto var = MethodConstructor->VariablesTypes.begin(); var != MethodConstructor->VariablesTypes.end(); var++)
	{
		std::string var_name = var->first;

		if(construct->Lookup(var_name).compare(TOP_STRING)!=0)
		{
			this->MemberVariables[(*var).first] = (*var).second;
		}
	}
	delete construct;
	// also we need to check to see the types and see if anything is top and stop.

	// this along with the method routines, and anything that has sub spaces that can define
	// variables such as

	//	while(true)
	//	{
	//		a:int = 42;
	//		a.PRINT();
	//	}
	//
	//	thus the method variables captured after this routine need to be set as our members as well.



	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		SymbolTable *construct = SymTab->Duplicate();

		done = false;
		while(!done)
		{
			for(auto var = MemberVariables.begin(); var != MemberVariables.end(); var++)
			{
				construct->SymTable[(*var).first] = (*var).second;
			}


			(*itr)->UpdatedType = false;
			if((*itr)->TypeCheck(construct))
			{
				if(true ==(*itr)->UpdatedType)
				{
					// update the class variable types.

					/*
					for(auto method_var = (*itr)->VariablesTypes.begin();method_var != (*itr)->VariablesTypes.end(); method_var++ )
					{
						auto finder = MemberVariables.find(method_var->first);

						if(finder != MemberVariables.end())
						{
							MemberVariables[finder->first] = finder->second;
						}
					}
					*/

				}
				else
				{
					done = true;
				}
			}
			else
			{
				ERR_LOG("Failed to Type Check Method: " + (*itr)->NameReturn ->Name, (*itr)->NearestSyntax);
				delete construct;
				return false;
			}
		}
		delete construct;
	}
	//broken until this is fixed.
	return true;
}

void ClassClass::Print()
{
	VAR_ADD();

	// print the
	FUNC_LOG("void ClassClass::Print()");
	
	VAR_LOG( "class " + TypeName->Type + " : "  + ParentName  );

	// Print out the constructor here.
	VAR_LOG("{");
	VAR_ADD();
	MethodConstructor->Print();
	VAR_SUB();
	VAR_LOG("}");
	// here print out the methods

	for(auto method = Methods.begin(); method != Methods.end(); method++)
	{ 
		if((*method)!=NULL)
		{
			(*method)->Print();
		}
	}

	VAR_LOG("Printing Symbol Table:");
	//this->ClassSymbolTable->print();
	//this->ConstructorSymbolTable->print();
	VAR_LOG("}");
	VAR_SUB();
}


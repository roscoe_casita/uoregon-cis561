#include "ProgramNode.hpp"
#include "QuackStatements.hpp"
#include "QuackProgramValidator.hpp"

ControlStatement::ControlStatement(SyntaxNode* ptr)
{
	FUNC_LOG("ControlStatement::ControlStatement(SyntaxNode* ptr)");
	NearestSyntax = ptr;
}

void ControlStatement::DoReduce(ControlStatement *&ptr)
{
	FUNC_LOG("void ControlStatement::DoReduce(ControlStatement *&ptr)");
	if(ptr != NULL)
	{
		ControlStatement *itr = ptr->Reduce();
		while(itr != ptr)
		{
			delete ptr;
			ptr = itr;
			itr = ptr->Reduce();
		}
	}
}


SequentialStatement::SequentialStatement(SyntaxNode* ptr) : ControlStatement(ptr)
{
	FUNC_LOG("SequentialStatement::SequentialStatement(SyntaxNode* ptr) : ControlStatement(ptr)");
}

SequentialStatement::~SequentialStatement()
{
}

bool SequentialStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)
{
	FUNC_LOG("bool SequentialStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)");
	for(size_t i = 0; i < Sequence.size(); i++)
	{
		if(!Sequence[i]->TypeCheck(SymTab, CurMethod))
		{
			return false;
		}
	}
	return true;
}


ControlStatementType SequentialStatement::GetStatementType()
{
	return SequentialStatementType;
}


ControlStatement *SequentialStatement::Reduce()
{
	FUNC_LOG("ControlStatement *SequentialStatement::Reduce()");
	ControlStatement *returnValue = this;
	if(Sequence.size() == 1)
	{
		returnValue = Sequence[0];
		Sequence.clear();
	}
	else
	{
		for(size_t i =0; i < Sequence.size(); i++)
		{
			ControlStatement *ptr = Sequence[i];
			DoReduce(ptr);
			Sequence[i] = ptr;
		}
	}
	return returnValue;
}

std::vector<GraphVizNodeDecorator *> SequentialStatement::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> SequentialStatement::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel ="Sequence";
	for(auto itr = Sequence.begin(); itr != Sequence.end(); itr++)
	{
		if((*itr)!=NULL)
		{
			returnValue.push_back(*itr);  //Segfault here
		}
	}

	return returnValue;
}


void SequentialStatement::Print()
{
	FUNC_LOG("void SequentialStatement::Print()");

	for(auto itr = Sequence.begin(); itr != Sequence.end(); itr++)
	{
		if((*itr)!=NULL)
		{
			(*itr)->Print();
		}
	}
}




IfElseListStatement::IfElseListStatement(SyntaxNode* ptr) : ControlStatement(ptr)
{
	FUNC_LOG("IfElseListStatement::IfElseListStatement(SyntaxNode* ptr) : ControlStatement(ptr)");
}

IfElseListStatement::~IfElseListStatement()
{
}

bool IfElseListStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)
{
	FUNC_LOG("bool IfElseListStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)");

	for(size_t i = 0; i < ConditionalList.size(); i++)
	{
		// Check the condition as long as it isn't the last one and isn't null
		if(ConditionalList[i].first != NULL )
		{
			if(0!=ConditionalList[i].first->ExtractType(SymTab).compare( BOOL_STRING))
			{
				ERR_LOG("Error! If conditional should return a bool", NearestSyntax);
				return false;
			}
		}

		SymbolTable *temp = SymTab->Duplicate();

		if(!ConditionalList[i].second->TypeCheck(temp, CurMethod))
		{
			delete temp;
			return false;
		}
		delete temp;

	}
	return true;
}


ControlStatementType IfElseListStatement::GetStatementType()
{
	FUNC_LOG("ControlStatementType IfElseListStatement::GetStatementType()");
	return IfElseListStatementType;
}

void IfElseListStatement::Add(RExpression *cond, ControlStatement *stmt)
{
	FUNC_LOG("void IfElseListStatement::Add(RExpression *cond, ControlStatement *stmt)");
	ConditionalList.push_back( std::pair<RExpression *,ControlStatement *>(cond,stmt));
}

ControlStatement *IfElseListStatement::Reduce()
{
	FUNC_LOG("ControlStatement *IfElseListStatement::Reduce()");
	for(auto itr = ConditionalList.begin(); itr != ConditionalList.end(); itr++)
	{
		DoReduce(itr->second);
	}
	return this;
}

std::vector<GraphVizNodeDecorator *> IfElseListStatement::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> IfElseListStatement::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel ="IfElseList";


	for(auto itr = ConditionalList.begin(); itr != ConditionalList.end(); itr++)
	{
		if( itr->first!=NULL)
		{
			returnValue.push_back(itr->first);
		}
		if( itr->second!=NULL)
		{
			returnValue.push_back(itr->second);
		}
	}


	return returnValue;
}

void IfElseListStatement::Print()
{
	FUNC_LOG("void IfElseListStatement::Print()");

	std::stringstream ss;

	ss << " if ( " ;

	for(auto itr = ConditionalList.begin(); itr != ConditionalList.end(); itr++)
	{
		if( itr->first!=0)
		{
			ss << itr->first->Print();
			ss << ")";
			VAR_LOG(ss.str());
			ss.str(std::string());
			ss.clear();
			ss << " elif (";
		}
		else
		{
			VAR_LOG("else");
		}
		VAR_LOG("{");
		VAR_ADD();
		if( itr->second!=NULL)
		{
			itr->second->Print() ;
		}
		VAR_SUB();
		VAR_LOG("}");
	}
}

WhileStatement::WhileStatement(SyntaxNode* ptr, RExpression *rexp, ControlStatement *stmt) : ControlStatement(ptr)
{
	FUNC_LOG("");
	Condition =rexp;
	Statements = stmt;
}
WhileStatement::~WhileStatement()
{
}

bool WhileStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)
{
	FUNC_LOG("bool WhileStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)");

	std::string condition_type =  Condition->ExtractType(SymTab);
	if(condition_type .compare(BOOL_STRING)!=0)
	{
		ERR_LOG("Error! While conditional should return a bool ", NearestSyntax);
		return false;
	}
	SymbolTable *temp = SymTab->Duplicate();

	if(!Statements->TypeCheck(temp, CurMethod))
	{
		delete temp;
		return false;
	}
	delete temp;
	return true;
}

ControlStatementType WhileStatement::GetStatementType()
{
	return WhileStatementType;
}

ControlStatement *WhileStatement::Reduce()
{
	FUNC_LOG("ControlStatement *WhileStatement::Reduce()");
	DoReduce(Statements);
	return this;
}

std::vector<GraphVizNodeDecorator *> WhileStatement::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> WhileStatement::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel ="While";
	if(Condition != NULL)
	{
		returnValue.push_back(Condition);
	}
	if(Statements != NULL)
	{
		returnValue.push_back(Statements);
	}
	return returnValue;
}

void WhileStatement::Print()
{
	FUNC_LOG("void WhileStatement::Print()");
	std::stringstream ss ;

	ss << " while ( ";
	if(Condition!=NULL)
	{
		ss << Condition->Print();
	}
	ss << ")";
	VAR_LOG(ss.str());
	VAR_LOG("{");
	VAR_ADD();
	if(Statements!=NULL)
	{
		Statements->Print();
	}
	VAR_SUB();
	VAR_LOG("}");
}


ReturnStatement::ReturnStatement(SyntaxNode* ptr, RExpression *rexp) : ControlStatement(ptr)
{
	FUNC_LOG("ReturnStatement::ReturnStatement(SyntaxNode* ptr, RExpression *rexp) : ControlStatement(ptr)");
	ReturnValue = rexp;
}

ReturnStatement::~ReturnStatement()
{
}

bool ReturnStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)
{
	FUNC_LOG("bool ReturnStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod, std::map<std::string,SymbolTable *> *GlobalSymTab)");
	std::string methodType = CurMethod->NameReturn->Type;

	std::string returnType = ReturnValue->ExtractType(SymTab);

	if( returnType.compare(TOP_STRING)==0)
	{
		ERR_LOG("Return Type invalid:", NearestSyntax);
		return false;
	}
	std::string lca_return_type = GetLowestCommonAncestor(returnType,methodType );

	if(lca_return_type.compare(methodType))
	{
		ERR_LOG("Error! Return type mismatch: " + lca_return_type + " : Method return Type: " + methodType , NearestSyntax);
		return false;
	}
	return true;
}


ControlStatementType ReturnStatement::GetStatementType()
{
	return ReturnStatementType;
}

std::vector<GraphVizNodeDecorator *> ReturnStatement::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> ReturnStatement::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel ="Return";
	if(ReturnValue != NULL)
	{
		returnValue.push_back(ReturnValue);
	}
	return returnValue;
}

void ReturnStatement::Print()
{
	FUNC_LOG("void ReturnStatement::Print()");
	VAR_ADD();
	std::stringstream ss ;
	ss << "return ";
	if(ReturnValue!=NULL)
	{
		ss << ReturnValue->Print();
	}
	ss << ";";
	VAR_LOG(ss.str());
	VAR_SUB();
}

AssignmentStatement:: AssignmentStatement(SyntaxNode* ptr, LExpression *lexp, RExpression *rexp) : ControlStatement(ptr)
{
	FUNC_LOG("AssignmentStatement:: AssignmentStatement(SyntaxNode* ptr, LExpression *lexp, RExpression *rexp) : ControlStatement(ptr)");
	Address = lexp;
	Statement = rexp;
}

AssignmentStatement::~AssignmentStatement()
{
}


bool AssignmentStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)
{
	FUNC_LOG("bool AssignmentStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod, std::map<std::string,SymbolTable *> *GlobalSymTab)");

	std::string right_type = Statement->ExtractType(SymTab);

	if(right_type.compare(TOP_STRING)==0)
	{
		ERR_LOG("Invalid Right Side of statement:",NearestSyntax);
		return false;
	}



	VariableIdentifier *vi = dynamic_cast<VariableIdentifier*>(Address);
	VariableLookup *vl = dynamic_cast<VariableLookup *>(Address);

	std::string str_type = TOP_STRING;
	std::string var_name;

	if(vl != NULL)
	{

		std::string ltype = vl->Lookup->ExtractType(SymTab);

		if(0 != SymTab->ClassName.compare(ltype))
		{
			ERR_LOG("Assignment to variable " + vl->Variable->Name + " in another class: " + ltype);
			return false;
		}
		var_name = vl->Variable->Name;

	}
	if(vi != NULL)
	{
		var_name = vi->Name;
	}

	std::string current_type = SymTab->Lookup(var_name);

	if(current_type.compare(TOP_STRING)==0)
	{
		SymTab->SymTable[var_name] = right_type;
		CurMethod->VariablesTypes[var_name] = right_type;
		CurMethod->UpdatedType = true;
	}
	else
	{

		std::string new_type = GetLowestCommonAncestor(current_type,right_type);

		if(0!=new_type.compare(current_type))
		{
			SymTab->SymTable[var_name] = right_type;
			CurMethod->VariablesTypes[var_name] = right_type;
			CurMethod->UpdatedType = true;
		}
	}
	return true;
}

std::vector<GraphVizNodeDecorator *> AssignmentStatement::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> AssignmentStatement::GenerateGraphvizChildren()");
	NodeShape = "box";
	NodeLabel ="Assignment";

	std::vector<GraphVizNodeDecorator *> returnValue;
	if(Address!=NULL)
	{
		returnValue.push_back(Address);
	}
	if(Statement!=NULL)
	{
		returnValue.push_back(Statement);
	}
	return returnValue;
}

ControlStatementType AssignmentStatement::GetStatementType()
{
	return AssignmentStatementType;
}

void AssignmentStatement::Print()
{
	FUNC_LOG("void AssignmentStatement::Print()");

	std::stringstream ss ;
	if(Address!=NULL)
	{
		ss << Address->Print();
	}
	 ss << " = " ;
	if(Statement!=NULL)
	{
		ss << Statement->Print();
	}
	ss << ";";
	VAR_LOG(ss.str());
}

SingleStatement::SingleStatement(SyntaxNode* ptr, RExpression *stmt) : ControlStatement(ptr)
{
	FUNC_LOG("SingleStatement::SingleStatement(SyntaxNode* ptr, RExpression *stmt) : ControlStatement(ptr)");
	Statement = stmt;
}

SingleStatement::~SingleStatement()
{
}

bool SingleStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)
{
	FUNC_LOG("bool SingleStatement::TypeCheck(SymbolTable *SymTab, MethodClass *CurMethod)");

	if(Statement->ExtractType(SymTab) != TOP_STRING)
	{
		return true;
	}
	ERR_LOG("Error! Invalid identifier ", NearestSyntax);
	return false;
}

std::vector<GraphVizNodeDecorator *> SingleStatement::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> SingleStatement::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "box";
	NodeLabel ="Single";
	returnValue.push_back(Statement);

	return returnValue;
}

ControlStatementType SingleStatement::GetStatementType()
{
	return SingleStatementType;
}


void SingleStatement::Print()
{
	FUNC_LOG("void SingleStatement::Print()");


	//VAR_LOG("SingleStatement : RExp " + HEX_STR(Statement));
	std::stringstream ss ;

	if(Statement!=NULL)
	{
		ss << Statement->Print();
	}
	ss << " ;";
	VAR_LOG(ss.str());
}

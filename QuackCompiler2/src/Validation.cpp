#include "Validation.hpp"


extern int yyparse ();
extern int yydebug;

extern void yyrestart(FILE *f);

extern int yy_flex_debug;

void AddDefaultClassesToProgram();

bool Validation()
{
	FUNC_LOG("bool Validation()");

	CompleteProgram *program = GetCompleteProgram();
	AddDefaultClassesToProgram();

	QuackProgramValidator * ProgramValidator = GetQuackProgramValidator();

	ProgramValidator->BuildClassHeirarchy(program);

	//NOTE: This function call requires that the class heirarchy and child class heirarchy has already been built
	VAR_LOG("Validating Polymorphism...");
	if(ProgramValidator->ValidateClassPolymorphism(program))
	{
		VAR_LOG("Polymorphism validation complete!");
	}
	else
	{
		ERR_LOG("Error! Polymorphism validation failed!");
		return false;
	}


	if(!ProgramValidator->CheckClassHeirarchy(program))
	{
		ERR_LOG("Class heirarchy is NOT well formed");
		return false;
	}
	else
	{
		VAR_LOG("Class heirarchy is well formed!");
	}

	return ProgramValidator->TypeCheckEverything(program);
}

void PrintVisualization(std::string base_file_name,bool valid)
{
	FUNC_LOG("void PrintVisualization(std::string base_file_name)");
	CompilerDatabase *database= GetDB();

	if(valid)
	{
		VAR_LOG("Before Printing DB.");

		database->PrintDatabase();

		VAR_LOG("After Printing DB." );

	}
	CompleteProgram *program = GetCompleteProgram();

	program -> GenerateSemanticSyntax(base_file_name);
	program -> GenerateProgram(base_file_name);
	//program -> GenerateProgramSemanticSyntax(base_file_name);

}

ReturnStatement *GenReturnThis()
{
	SyntaxNode *node = new SyntaxNode("this",IDENTIFIER,-999);
	VariableIdentifier *ptr = new VariableIdentifier(node,"this",BOTTOM_STRING);

	return new ReturnStatement(node,ptr);
}

ReturnStatement *GenEmptyStringReturn()
{
	SyntaxNode *node = new SyntaxNode("",STRING_LITERAL,-999);
	TypedValue *ptr = new TypedValue(node,STRING_STRING,"");

	return new ReturnStatement(node,ptr);
}

ReturnStatement *GenFalseValueReturn()
{
	SyntaxNode *node = new SyntaxNode("false",FALSE_VALUE,-999);
	TypedValue *ptr = new TypedValue(node,BOOL_STRING,"false");
	return new ReturnStatement(node,ptr);
}

ReturnStatement *GenNothingValueReturn()
{
	SyntaxNode *node = new SyntaxNode("none",NOTHING_VALUE,-999);
	TypedValue *ptr = new TypedValue(node,NOTHING_STRING,"none");
	return new ReturnStatement(node,ptr);
}

ReturnStatement *GenZeroValueReturn()
{
	SyntaxNode *node = new SyntaxNode("0",DIGIT_VALUE,-999);
	TypedValue *ptr = new TypedValue(node,INT_STRING,"0");
	return new ReturnStatement(node,ptr);
}

MethodClass* GenerateConstructor(std::string class_name)
{
	SyntaxNode *node = new SyntaxNode(class_name,CLASS,-999 );
	MethodClass *constructor = new MethodClass(node);
	constructor->NameReturn = new NamedType(node,class_name,class_name);
	constructor->Statements = GenReturnThis();
	return constructor;
}

MethodClass* GenerateDefaultMethod(std::string method_name, std::string parameter_type, std::string return_type, ReturnStatement *return_stmt)
{
	SyntaxNode *node = new SyntaxNode(method_name,IDENTIFIER,-999 );
	MethodClass *default_method = new MethodClass(node);
	default_method->NameReturn = new NamedType(node,method_name,return_type);

	SyntaxNode *arg = new SyntaxNode("other",IDENTIFIER,-999);
	NamedType *argument = new NamedType(arg,"other",parameter_type);
	default_method->Arguments.push_back(argument);
	default_method->Statements = return_stmt;
	return default_method;
}



ClassClass* GenerateObjectClass()
{
	SyntaxNode* ptr = new SyntaxNode(OBJ_STRING,CLASS,-999);
	ClassClass* objectClass = new ClassClass(ptr);

	objectClass->MethodConstructor = GenerateConstructor(OBJ_STRING);

	SyntaxNode *str_node = new SyntaxNode("STR", IDENTIFIER, -999);

	MethodClass* stringMethod = new MethodClass(str_node);
	stringMethod->NameReturn = new NamedType(str_node, "STR", STRING_STRING);
	stringMethod->Statements = GenEmptyStringReturn();
	objectClass->Methods.push_back(stringMethod);



	//The following section of commented code is for the EQUAL method for OBJ

	/*arg = new SyntaxNode("other",IDENTIFIER,-999);
	NamedType *argument = new NamedType(arg,"other",parameter_type);
	default_method->Arguments.push_back(argument);
	stringMethod->NameReturn = new NamedType(equal_node, "EQUAL", BOOL_STRING);
	stringMethod->Statements = GenFalseValueReturn();*/

	SyntaxNode *print_node = new SyntaxNode("PRINT", IDENTIFIER, -999);
	MethodClass* printMethod = new MethodClass(print_node);
	printMethod->NameReturn = new NamedType(print_node, "PRINT", NOTHING_STRING);
	printMethod->Statements = GenNothingValueReturn();

	objectClass->Methods.push_back(printMethod);

	objectClass->ParentName = OBJ_STRING;
	objectClass->TypeName = new NamedType(ptr, OBJ_STRING, OBJ_STRING);

	return objectClass;

}

ClassClass* GenerateIntegerClass()
{
	SyntaxNode* ptr = new SyntaxNode(INT_STRING,CLASS,-999);
	ClassClass* integerClass = new ClassClass(ptr);

	integerClass->MethodConstructor = GenerateConstructor(INT_STRING);

	integerClass->Methods.push_back(GenerateDefaultMethod("PLUS", INT_STRING, INT_STRING, GenZeroValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("MINUS", INT_STRING, INT_STRING, GenZeroValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("TIMES", INT_STRING, INT_STRING, GenZeroValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("DIVIDE", INT_STRING, INT_STRING, GenZeroValueReturn()));

	integerClass->Methods.push_back(GenerateDefaultMethod("ATMOST", INT_STRING, BOOL_STRING, GenFalseValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("LESS", INT_STRING, BOOL_STRING, GenFalseValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("ATLEAST", INT_STRING, BOOL_STRING, GenFalseValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("MORE", INT_STRING, BOOL_STRING, GenFalseValueReturn()));
	integerClass->Methods.push_back(GenerateDefaultMethod("EQUALS", INT_STRING, BOOL_STRING, GenFalseValueReturn()));

	integerClass->ParentName = OBJ_STRING;
	integerClass->TypeName = new NamedType(ptr, INT_STRING, INT_STRING);

	return integerClass;

}

ClassClass* GenerateBooleanClass()
{
	SyntaxNode* ptr = new SyntaxNode(BOOL_STRING,CLASS,-999);
	ClassClass* booleanClass = new ClassClass(ptr);

	booleanClass->MethodConstructor = GenerateConstructor(BOOL_STRING);

	booleanClass->Methods.push_back(GenerateDefaultMethod("EQUALS", BOOL_STRING, BOOL_STRING, GenFalseValueReturn()));
	booleanClass->Methods.push_back(GenerateDefaultMethod("AND", BOOL_STRING, BOOL_STRING, GenFalseValueReturn()));
	booleanClass->Methods.push_back(GenerateDefaultMethod("OR", BOOL_STRING, BOOL_STRING, GenFalseValueReturn()));
	booleanClass->Methods.push_back(GenerateDefaultMethod("NOT", BOOL_STRING, BOOL_STRING, GenFalseValueReturn()));

	booleanClass->ParentName = OBJ_STRING;
	booleanClass->TypeName = new NamedType(ptr, BOOL_STRING, BOOL_STRING);

	return booleanClass;
}

ClassClass* GenerateNothingClass()
{
	SyntaxNode* ptr = new SyntaxNode(NOTHING_STRING,CLASS,-999);
	ClassClass* nothingClass = new ClassClass(ptr);

	nothingClass->MethodConstructor = GenerateConstructor(NOTHING_STRING);

	nothingClass->ParentName = OBJ_STRING;
	nothingClass->TypeName = new NamedType(ptr, NOTHING_STRING, NOTHING_STRING);

	return nothingClass;

}

ClassClass* GenerateStringClass()
{
	SyntaxNode* ptr = new SyntaxNode(STRING_STRING,CLASS,-999);
	ClassClass* stringClass = new ClassClass(ptr);

	stringClass->MethodConstructor = GenerateConstructor(STRING_STRING);

	stringClass->Methods.push_back(GenerateDefaultMethod("PLUS", STRING_STRING, STRING_STRING, GenEmptyStringReturn()));

	stringClass->Methods.push_back(GenerateDefaultMethod("EQUALS", STRING_STRING, BOOL_STRING, GenFalseValueReturn()));
	stringClass->Methods.push_back(GenerateDefaultMethod("ATLEAST", STRING_STRING, BOOL_STRING, GenFalseValueReturn()));
	stringClass->Methods.push_back(GenerateDefaultMethod("ATMOST", STRING_STRING, BOOL_STRING, GenFalseValueReturn()));
	stringClass->Methods.push_back(GenerateDefaultMethod("LESS", STRING_STRING, BOOL_STRING, GenFalseValueReturn()));
	stringClass->Methods.push_back(GenerateDefaultMethod("MORE", STRING_STRING, BOOL_STRING, GenFalseValueReturn()));


	stringClass->ParentName = OBJ_STRING;
	stringClass->TypeName = new NamedType(ptr, STRING_STRING, STRING_STRING);

	return stringClass;
}

void AddDefaultClassesToProgram()
{
	FUNC_LOG("void AddDefaultClassesToProgram()");

	CompleteProgram *program = GetCompleteProgram();


	program->ClassDefines.push_back(GenerateObjectClass());
	program->ClassDefines.push_back(GenerateIntegerClass());
	program->ClassDefines.push_back(GenerateStringClass());
	program->ClassDefines.push_back(GenerateBooleanClass());
	program->ClassDefines.push_back(GenerateNothingClass());


}
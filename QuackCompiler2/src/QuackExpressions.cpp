
#include "QuackExpressions.hpp"
#include "QuackProgramValidator.hpp"


bool ValidateArgs(std::vector<std::string> arg_types, std::vector<NamedType *> arg_params)
{
	FUNC_LOG("bool ValidateArgs(std::vector<std::string> arg_types, std::vector<NamedType *> arg_params)");
	if(arg_types.size() != arg_params.size())
	{
		return false;
	}
	for(size_t i =0; i < arg_types.size(); i++)
	{
		std::string arg_type = arg_types[i];
		std::string param_type = arg_params[i]->Type;



		if(arg_type.compare(param_type)!=0)
		{
			return false;
		}
	}

	return true;
}


VariableIdentifier::VariableIdentifier(SyntaxNode *ptr, std::string name,std::string type) : LExpression(ptr),Name(name),Type(type)
{
	FUNC_LOG("VariableIdentifier::VariableIdentifier(SyntaxNode *ptr, std::string name,std::string type) : LExpression(ptr),Name(name),Type(type)");

}
VariableIdentifier::~VariableIdentifier()
{
}
std::string VariableIdentifier::Print()
{
	FUNC_LOG("std::string VariableIdentifier::Print()");
	return Name;
}

std::vector<GraphVizNodeDecorator *> VariableIdentifier::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> VariableIdentifier::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeLabel = EscapeString(Name) + " : " + EscapeString(Type);
	NodeShape = "box";

	return returnValue;
}

std::string VariableIdentifier::ExtractType(SymbolTable *SymTab)
{
	FUNC_LOG("std::string VariableIdentifier::ExtractType(SymbolTable *SymTab)");

	return SymTab->Lookup(Name);

	/*
	std::string new_type_name = QuackProgramValidator::LowestCommonAncestor(current_type_name,Type);

	if(new_type_name.compare(current_type_name)!=0)
	{
		SymTab->UpdateTable(Name,new_type_name);
	}
	else
	{
	}
	return new_type_name;
	*/
}











VariableLookup::VariableLookup(SyntaxNode *ptr, Expression *lookup, Expression *variable) : LExpression(ptr)
{
	FUNC_LOG("VariableLookup::VariableLookup(SyntaxNode *ptr, Expression *lookup, Expression *variable) : LExpression(ptr)");
	Lookup = dynamic_cast<RExpression *>(lookup);
	Variable= dynamic_cast<VariableIdentifier*>(variable);
}

VariableLookup::~VariableLookup()
{
}

std::string VariableLookup::ExtractType(SymbolTable *SymTab)
{
	FUNC_LOG("std::string VariableLookup::ExtractType(SymbolTable *SymTab)");
	std::string left = Lookup->ExtractType(SymTab);


	if(left.compare(TOP_STRING)==0)
	{
		ERR_LOG("Invalid Variable Lookup on left side of . :",this->NearestSyntax);
		return TOP_STRING;
	}
	if(0 != SymTab->ClassName.compare(left))
	{
		ERR_LOG("Invalid Member access of Class " + left + " on the left side of . :", this->NearestSyntax);
		return TOP_STRING;
	}

	std::string returnValue =  SymTab->Lookup(Variable->Name);

	if(0==returnValue.compare(TOP_STRING))
	{
		ERR_LOG("Member " + Variable->Name + " of Class " + left + " not found:", this->NearestSyntax);
	}
	return returnValue;
}

std::string VariableLookup::Print()
{
	FUNC_LOG("std::string VariableLookup::Print()");
	std::stringstream ss;

	ss << "(";
	if(Lookup!=NULL)
	{
		ss << Lookup->Print();
	}
	else
	{
		ss << "BADLOOKUP in VariableLookup";
	}
	ss << ".";
	if(Variable != NULL)
	{
		ss << Variable->Print();
	}
	else
	{
		ss << "BADVARIABLE in VariableLookup";
	}
	ss << ") ";
	return ss.str();
}

std::vector<GraphVizNodeDecorator *> VariableLookup::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> VariableLookup::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeLabel = "Var Lookup";
	NodeShape = "box";

	if(Lookup!=NULL)
	{
		returnValue.push_back(Lookup);
	}
	if(Variable != NULL)
	{
		returnValue.push_back(Variable);
	}
	return returnValue;
}






TypedValue::TypedValue(SyntaxNode *ptr, std::string type, std::string value) : RExpression(ptr),Type(type),Value(value)
{
	FUNC_LOG("TypedValue::TypedValue(SyntaxNode *ptr, std::string type, std::string value) : RExpression(ptr),Type(type),Value(value)");
}

TypedValue::~TypedValue()
{
}

std::string TypedValue::Print()
{
	FUNC_LOG("std::string TypedValue::Print()");
	return Value ;
}

std::vector<GraphVizNodeDecorator *> TypedValue::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> TypedValue::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel = EscapeString(Type) + " : " + EscapeString(Value);
	return returnValue;
}

std::string TypedValue::ExtractType(SymbolTable *SymTab)
{
	FUNC_LOG("std::string TypedValue::ExtractType(SymbolTable *SymTab)");
	return Type;
}



FunctionCall::FunctionCall(SyntaxNode *ptr, Expression *call, std::vector<Expression*> args) : RExpression(ptr)
{
	FUNC_LOG("FunctionCall::FunctionCall(SyntaxNode *ptr, Expression *call, std::vector<Expression*> args) : RExpression(ptr)");
	Call = dynamic_cast<LExpression*>(call);
	Arguments = args;
	MethodCall = NULL;
}

FunctionCall::~FunctionCall()
{

}
std::string FunctionCall::Print()
{
	FUNC_LOG("std::string FunctionCall::Print()");

	std::stringstream ss;

	ss << "[";
	if(Call != NULL)
	{
		ss << Call->Print();
	}
	else
	{
		ss << "BAD_CALL IN FUNC CALL";
	}

	ss << " ( " ;
	for( auto itr = Arguments.begin(); itr != Arguments.end(); itr++)
	{
		if((*itr) != NULL)
		{
			ss << (*itr)->Print() << ",";
		}
		else
		{
			ss << "BAD_ARG in FUNC CALL";
		}
	}
	ss << " ) ";
	ss << "]";

	return ss.str();
}

std::vector<GraphVizNodeDecorator *> FunctionCall::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> FunctionCall::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "box";
	NodeLabel = "Func Call";
	if(Call != NULL)
	{
		returnValue.push_back(Call);
	}

	for( auto itr = Arguments.begin(); itr != Arguments.end(); itr++)
	{
		if((*itr) != NULL)
		{
			returnValue.push_back(*itr);
		}
	}
	return returnValue;
}


std::string FunctionCall::ExtractType(SymbolTable *SymTab)
{
	FUNC_LOG("std::string FunctionCall::ExtractType(SymbolTable *SymTab)");
	VariableIdentifier *vi = dynamic_cast<VariableIdentifier*>(Call);
	VariableLookup *vl = dynamic_cast<VariableLookup*>(Call);
	std::string returnValue = TOP_STRING;

	std::vector<std::string> argTypes;
	std::vector<NamedType *> callArgs;
	for( auto itr = Arguments.begin(); itr != Arguments.end(); itr++)
	{
		std::string exp_type = (*itr)->ExtractType(SymTab);
		argTypes.push_back(exp_type);
	}



	CompleteProgram *cp =GetCompleteProgram();



	if(vi != NULL)
	{
		ClassClass *cc = cp->LookupClass(vi->Name);

		if(cc != NULL)
		{
			// Constructor detected. Variable name is a class constructor.
			// A();

			callArgs = cc->MethodConstructor->Arguments;
			this->MethodCall = cc ->MethodConstructor;
		}
		else
		{
			// Local Function call detected:   foo()

			MethodClass *mc = cp->LookupClassMethod(SymTab->ClassName,vi->Name);

			if(mc == NULL)
			{
				ERR_LOG("Invalid Local name lookup:" + vi->Name, this->NearestSyntax);
				return TOP_STRING;
			}
			else
			{
				callArgs = mc->Arguments;
				this->MethodCall = mc;
			}
		}
	}
	else
	{
		// remote procedure call:
		// this.a.foo().bad()


		std::string lookup_type = vl->Lookup->ExtractType(SymTab);
		if(lookup_type.compare(TOP_STRING)==0)
		{
			ERR_LOG("Invalid expression on left side of . ",vl->NearestSyntax);
			return TOP_STRING;
		}
		else
		{
			MethodClass *mc = cp->LookupClassMethod(lookup_type,vl->Variable->Name);

			if(mc == NULL)
			{
				ERR_LOG("Failed to find method " + vl->Variable->Name + " in class " + lookup_type, this->NearestSyntax);
				return TOP_STRING;
			}
			else
			{
				callArgs = mc->Arguments;
				this->MethodCall = mc;
			}
		}
	}


	if(!ValidateArgs(argTypes,callArgs))
	{
		ERR_LOG("Invalid Arguments passed to funtion call:", this->NearestSyntax);
		return TOP_STRING;

	}
	return this->MethodCall->NameReturn->Type;
}


Expression::Expression(SyntaxNode *ptr)
{
	NearestSyntax = ptr;
}

RExpression::RExpression(SyntaxNode *ptr) : Expression(ptr)
{

}

LExpression::LExpression(SyntaxNode *ptr) : RExpression(ptr)
{

}


MathExpression::MathExpression(SyntaxNode *ptr, Expression *left, std::string operation, Expression *right) : RExpression(ptr),Operation(operation)
{
	FUNC_LOG("MathExpression::MathExpression(SyntaxNode *ptr, Expression *left, std::string operation, Expression *right) : RExpression(ptr),Operation(operation)");
	LeftSide = left;
	RightSide =right;
}

MathExpression::~MathExpression()
{
}

std::string MathExpression::Print()
{
	FUNC_LOG("std::string MathExpression::Print()");
	std::stringstream ss;
	ss << " (";
	if(LeftSide !=NULL)
	{
		ss << LeftSide->Print() ;
	}
	ss << " " << Operation << " " ;
	if(RightSide != NULL)
	{
		ss << RightSide->Print();
	}
	ss << ")";
	return ss.str();
}

std::vector<GraphVizNodeDecorator *> MathExpression::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> MathExpression::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel = Operation;
	if(LeftSide != NULL)
	{
		returnValue.push_back(LeftSide);
	}
	if(RightSide != NULL)
	{
		returnValue.push_back(RightSide);
	}
	return returnValue;
}

std::string MathExpression::ExtractType(SymbolTable *SymTab)
{
	FUNC_LOG("std::string MathExpression::ExtractType(SymbolTable *SymTab)");
	std::string left = LeftSide->ExtractType(SymTab);
	std::string right = RightSide ->ExtractType(SymTab);

	if(left.compare(TOP_STRING)==0)
	{
		ERR_LOG("Invalid Expression to the left of " + this->Operation , this->NearestSyntax);
		return TOP_STRING;
	}
	if(right.compare(TOP_STRING)==0)
	{
		ERR_LOG("Invalid Expression to the right of " + this->Operation , this->NearestSyntax);
		return TOP_STRING;
	}

	std::string new_type = GetLowestCommonAncestor(left,right);


	CompleteProgram *cp = GetCompleteProgram();

	MethodClass *mc = cp->LookupClassMethod(new_type,this->Operation);

	if(mc == NULL)
	{
		ERR_LOG("TYPE CHECK ERROR: " + this->Operation + " is not supported by "+ new_type,this->NearestSyntax);
		return TOP_STRING;
	}
	return new_type;
}



BoolExpression::BoolExpression(SyntaxNode *ptr, Expression *left, std::string operation,Expression *right) : RExpression(ptr),Operator(operation)
{
	FUNC_LOG("BoolExpression::BoolExpression(SyntaxNode *ptr, Expression *left, std::string operation,Expression *right) : RExpression(ptr),Operator(operation)");
	LeftSide = left;
	RightSide =right;
}

std::string BoolExpression::Print()
{
	FUNC_LOG("std::string BoolExpression::Print()");
	std::stringstream ss;

	ss << "(";
	if(LeftSide != NULL)
	{
		ss << LeftSide->Print();
	}
	else
	{
		ss <<"BAD_LEFT_EXP in BoolExpression";
	}

	ss << " " << Operator << " ";

	if(RightSide != NULL)
	{
		ss << RightSide->Print();
	}
	else
	{
		ss <<"BAD_RIGHT_EXP in BoolExpression";
	}
	ss << ")";
	return ss.str();
}

std::vector<GraphVizNodeDecorator *> BoolExpression::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> BoolExpression::GenerateGraphvizChildren()");

	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "box";
	NodeLabel = Operator;
	if(LeftSide != NULL)
	{
		returnValue.push_back(LeftSide);
	}
	if(RightSide != NULL)
	{
		returnValue.push_back(RightSide);
	}
	return returnValue;
}


std::string BoolExpression::ExtractType(SymbolTable *SymTab)
{
	FUNC_LOG("std::string BoolExpression::ExtractType(SymbolTable *SymTab)");
	std::string left = LeftSide->ExtractType(SymTab);
	std::string right = RightSide ->ExtractType(SymTab);

	if(left.compare(TOP_STRING)==0)
	{
		ERR_LOG("Invalid Expression to the left of " + this->Operator , this->NearestSyntax);
		return TOP_STRING;
	}
	if(right.compare(TOP_STRING)==0)
	{
		ERR_LOG("Invalid Expression to the right of " + this->Operator , this->NearestSyntax);
		return TOP_STRING;
	}

	std::string new_type = GetLowestCommonAncestor(left,right);

	CompleteProgram *cp = GetCompleteProgram();

	MethodClass *mc = cp->LookupClassMethod(new_type,this->Operator);

	if(mc == NULL)
	{
		ERR_LOG("TYPE CHECK ERROR: " + this->Operator+ " is not supported by "+ new_type,this->NearestSyntax);
		return TOP_STRING;
	}

	return BOOL_STRING;
}




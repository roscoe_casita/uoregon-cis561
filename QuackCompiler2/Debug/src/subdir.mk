################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/CompilerDB.cpp \
../src/GraphVizNodeDecorator.cpp \
../src/ProgramNode.cpp \
../src/QuackBaseClasses.cpp \
../src/QuackCompiler2.cpp \
../src/QuackDefines.cpp \
../src/QuackExpressions.cpp \
../src/QuackProgramValidator.cpp \
../src/QuackStatements.cpp \
../src/lex.yy.cpp \
../src/quack.tab.cpp 

OBJS += \
./src/CompilerDB.o \
./src/GraphVizNodeDecorator.o \
./src/ProgramNode.o \
./src/QuackBaseClasses.o \
./src/QuackCompiler2.o \
./src/QuackDefines.o \
./src/QuackExpressions.o \
./src/QuackProgramValidator.o \
./src/QuackStatements.o \
./src/lex.yy.o \
./src/quack.tab.o 

CPP_DEPS += \
./src/CompilerDB.d \
./src/GraphVizNodeDecorator.d \
./src/ProgramNode.d \
./src/QuackBaseClasses.d \
./src/QuackCompiler2.d \
./src/QuackDefines.d \
./src/QuackExpressions.d \
./src/QuackProgramValidator.d \
./src/QuackStatements.d \
./src/lex.yy.d \
./src/quack.tab.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	LLVMFLAGS='llvm-config --cppflags --libs' && g++ -O0 -g3 -Wall -c -fmessage-length=0  -std=c++11 $(LLVMFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)"  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



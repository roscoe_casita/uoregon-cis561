import os
import sys
import time

input_files = sys.argv

del input_files[0]

directory = os.getcwd()
flags = ''

for i in range(len(input_files)):
    if input_files[i] == "-d":
        directory += "/" + input_files[i + 1]
        break
    if input_files[i] == "-f" or input_files[i] == "-e" or input_files[i] == "-v" or input_files[i] == "-t":
        flags += input_files[i]
    

cmd = ''
if len(input_files) > 0:
    if input_files[0] == "--all" or input_files[0] == "all" or input_files[0] == "-a" or input_files[0] == "-A":
        print "Running all files in : " + directory
        files = os.listdir(directory)
        for file_name in files:
            end_chars = file_name[len(file_name) - 3 :]
            if end_chars == ".qk":
                print "RUNNING QUACK COMPILER ON : " + file_name
                cmd = './quack ' + directory + file_name + ' ' + flags
                os.system(cmd)
                time.sleep(.1)

    else:
        for file_name in input_files:
            end_chars = file_name[len(file_name) - 3 :]
            if end_chars == ".qk":
                cmd = './quack ' + directory + file_name + ' ' + flags
                os.system(cmd)
                time.sleep(.1)


else:
    print "Command format: python run_script.py <filenames> <commands>"
    print "Valid commands:  -d <directory> | -f | -e | -v | -t | --all"
    print "Type - python run_script.py all        If you wish to run on all quack files"



\documentclass[12pt]{article}
\usepackage{cite}
\usepackage{url}


\begin{document}

\title{CIS 561: A Brief History and Forward Projection}
\author{Roscoe S. Casita}
\maketitle

\begin{abstract}
A brief background and history of compilers is examined to understand the past and future role of compilers in computer science. Compiler design and implementation has changed with the hardware, the languages available, as runtime mechanics were established and operating systems began to intersect all these domains. Programming languages, compilers, and operating systems are all intrinsically interrelated. Mechanics that cannot be expressed cannot be implemented in an operating system written in that language. Programming language runtime environments are implemented by the compiler and operating system. Compilers are the exception that allow the translation of semantics to the target language.
\end{abstract}
\section{Brief History}

Compilers have advanced faster than computer science itself: Grace Hopper's implemented the first compiler in 1952, Lisp-in-Lisp 1962,  Grammar formalization with Noam Chomsky 1965, Efficient meta compilers introduced in 1968 all culminating in Lex/Yacc circa 1975. The steady advancement continued as the dragons of compiler construction were slain ~\cite{DragonBook}, small portable C compilers adapted for specific hardware became standard operating procedure \cite{MechanicsBook}, and byte interpreters are introduced which make the way for Java ~\cite{LISP}. Extensions to the basic theory still continue to this day with Parsing Expression Grammars (full tree rewriting systems) ~\cite{PEG} and the bleeding edge of exploration with experimental context sensitive language parsers to deal with natural language processing. Compilers can now be built in short 10 week courses, Javascript interpreters emit just-in-time code on the majority of web pages today ~\cite{zakai2011emscripten}, and even toy meta compilers are hosted on-line that generate javascript and run it in real time. ~\cite{MetaII} \\

The dragons of compiler construction are dead and compilers lost the center stage as technologies such as deep learning and the Internet-of-Things (IoT) came on-line.  Compiler construction has gone from being a mysterious and dark art to a routine exercise in hard work. Simply pick a source, target, and implementation language; construct a tokenizer, a grammar parser to generate the abstract syntax tree, and emit the target language. The true source of difficulty is the design of good language syntax, semantics, runtime facilities and the defined interaction between the program and the operating system. \\

Programming languages are defined by the features and sets of tools they support, the sequence can be thought of as the most complex systems task in computer science. First bootstrap a language-in-language compiler in the new language. Add extensible libraries which effectively becomes a wrapped operating system layer. Potentially build a runtime operating system layer. Add runtime interpretation via full LLVM implementation in the new language. Emit Just-In-Time and execute on the fly the entire new system. Now full interop between languages is possible and languages can be embedded inside each other. The best example of a similar system is the Common Language Runtime that allows LINQ to be written and embedded in C\# that queries runtime objects like an SQL database ~\cite{torgersen2007querying}.\\ 

Erlang, Java, and C\# are prime examples of modern compiled languages that consider all of these facets.  All 3 languages implement just-in-time compilers, are capable of supporting direct interpretation, have well defined runtime characteristics, and wrap all system calls in a native operating system library. Essentially when using any programming language, the user also commits to using the compiler implementing the programming language, the operating system wrapper libraries, the libraries the program leverages, etc. This is how systems get locked into specific hardware architecture with a specific proprietary software suits that seems like a series of crazy design decisions, but at the time it just made sense. 
\newpage
\section{The Next Big Thing}
Language design, implementation, release, and acceptance is difficult to achieve. An example is the slow adoption rate of python2/3 at close to 30 years old. Having a non-backwards compatible release of python3 also caused extreme pain in all adopters of python2. As all the python2 programs had to be rewritten in python3 every system implementer will ask ``What language should I write this in so I don't have to do this again?''. Alternatives such as Mathematica and R can quickly hammer nails in a language just because of support for tools, libraries, and features that have nothing to do with the language or compiler itself. C\# rode the success of Java by fixing the major problems in Java and expanding the runtime libraries far beyond those provided by Java. These simple advantages propelled C\# above Java in terms of job demand. \\

The strongest feature of C\# allows embedding of subdomain specific languages, similar to LISP and FORTH syntax and macro features, requires substantial work to create working implementations. The payoff is that the language becomes embeddable with all the support of CLR languages. The question then becomes specifically if the language you are implementing is a simple DSL then why not embed the system in C\# and then have access to all of the runtime support. \\

What will be the next big thing for compilers and programming languages? The answer is complex and depends on the nature of the source and target languages. Letting a programmer create a fancy syntax shortcut with semantic interpretation such as the exponent operator $**$ in python is a trivial feature that will not cause wide spread adoption of a language. A counter example is powerful runtime facilities such as ``call-by-name'' or semantic operations such as ``defmacro'' in LISP that change computer science ~\cite{Baker:1991:PPC:121983.121984}.  Any new language that gains widespread acceptance will have users that demand all of the familiar experiences plus the new exotic features. As a systems implementer that needs to support a language or new features there a few options.: Use a specific compiler such as the Intel optimizing C/C++ targeting the extended register set, otherwise embed the language as a DSL in an existing language ~\cite{5551015}, as a last and extreme case extend an existing language with new features via implementing a new compiler for the language.

\newpage
\section{Distributed  systems }  

The strongest change to compilers will be driven by external demands to support new hardware features and capabilities. SIMD programming for the GPU has drove the development of OpenCL and CUDA. Both are feature rich yet lock the user into a specific compiler, environment, system, hardware architecture depending on the customization of the program.\\

 Synchronizing programs across time and space to ensure that they behave with mechanics identical to the semantics envisioned by the programmer is a task that spans hardware implementation, target language, compiler, source language, runtime support, operating system interaction, and potentially leveraging networking for bridging communication.  \\
 
Now that Moores Law has changed from faster computers to multiple computers, as demonstrated by the massive distributed supercomputers, there is a rising need for a native programming languages that intrinsically leverage the distributed hardware. The demand is so great and the results so dependent upon the problem that there are multitudes of implementations~\cite{SM_MOCP}. There are multitudes of parallel C/C++ libraries, runtime systems, language extensions and compilers that abstract this problem in one way or another.  ~\cite{bodin1993implementing} ~\cite{todter1995parc++} ~\cite{an2001stapl}  ~\cite{bischof2003dattel} ~\cite{TranTan2016}\\
 
 
The entire class of problems caused by distributed systems with no native language to express them in has turned domain specific language implementations into the wild west of development. The business that implements a parallel strategy the best, is able to construct a software package to leverage that capability and sell it. While the strategy does generate returns for the companies involved; programs are still not expressed in a natively distributed way ~\cite{neumann2011efficiently}.  The current lowest common denominator parallelism that is now widely available is the thread, async, and future of C++11. The programming world doesn't need another parallel C++ library; it needs native compilation to parallel code without being locked into a specific compiler.
\newpage
\section{Future Directions: Quantum Hardware}

Quantum computers are still barely out of the labor room; yet compiler and language designers must immediatly ask ``What will the programming language look like?'', ``What types of problems are easily expressed in the language?'', and even trivialities such as ``How do you write hello world in the language or can you do that?''.\\

A first taste of what programming the future will look like was presented as the Quipper programming language
~\cite{Green:2013:QSQ:2499370.2462177}. The notion that fundamentally the language will be a circuit design language, perhaps similar to Verilog or VHDL will be the first steps towards being able to model and build the hardware. The next tempting question to ask is ``If Quipper is the hardware design language, then what will the assembly language look like, if there is an assembly language.''.  If the notion of a quantum assembly language holds, then what would a high level quantum programming language look like if such a notion holds.\\

As a practical hands on implementer the probability of seeing a quantum computer any time soon or need to know how to program one is currently low. The practices by JVM, LLVM, and CLR of implementing a uniform micro-architecture that ensures the encoding can be interpreted on multiple machines is a key insight to our future. The formalization of an abstract micro architecture layer forces good design practices into hardware synthesizer, software simulator, language designer, and compiler implementer. ~\cite{wang_et_al:LIPIcs:2015:5034}\\

A testimony to the success of this strategy is the CLR implementation by Xarmin/Mono.NET.  The company has leveraged the open source specifications of C\# and CLR and sell their own implementation.  The number of platforms that can be targeted with a simple implementation is staggering:  Windows X86/X64, OSX, Linux, WebPage (JavaScript WebGL), iOS, Android, XBox360, PS/4, etc. The embedded world is abstracting to just-in-time compilation systems to deal with the differences between ARM and x86/x64 architectures ~\cite{cohen2010processor}. Compilers will always be at the forefront of new hardware, language design, and even product implementation, and thus the demand for compiler writers should always be strong. 
\newpage
\section{References}

\nocite{*}
\bibliography{TermPaper}{}
\bibliographystyle{apalike}
\end{document}
#include <stdlib.h>

struct object_object;
struct object_nothing;
struct object_string;

struct class_object{
	struct object_object* (*constructor)();
	struct object_nothing* (*PRINT)(struct object_object*);
	struct object_string* (*STR)(struct object_object*);
};

struct object_object{
	struct class_object* the_class_object;
};

struct class_string{
	struct object_string* (*constructor)();
	struct object_nothing* (*PRINT)(struct object_string*);
	struct object_string* (*STR)(struct object_string*);
};

struct object_string{
	struct class_string* the_class_string;
	char* s;
};

struct class_nothing{
	struct object_nothing* (*constructor)();
};

struct object_nothing{
	struct class_nothing* the_nothing_object;
};

extern struct class_object the_class_object;
extern struct class_string the_class_string;
extern struct class_nothing the_nothing_object;

struct object_object* class_Object_Method_constructor()
{
	struct object_object * returnValue = malloc(sizeof(struct object_object));
	returnValue->the_class_object = &the_class_object;
	return returnValue;
}

struct object_nothing* class_Object_Method_PRINT(struct object_object* this_ptr)
{
	
	struct object_string * str_val = this_ptr->the_class_object->STR(this_ptr);
	struct object_nothing * returnValue =  str_val->the_class_string->PRINT(str_val);
	return returnValue;
}

struct object_string* class_Object_Method_STR(struct object_object* this_ptr)
{
//	struct object_object * returnValue = new object_object();
//	object_object->the_class_object = the_class_object;
//	return returnValue;
	return NULL;
}

int main(){
	the_class_object.constructor = class_Object_Method_constructor;
	the_class_object.PRINT = class_Object_Method_PRINT;
	the_class_object.STR = class_Object_Method_STR;
	return 0;
}
; ModuleID = 'test.c'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.class_object = type { %struct.object_object* (...)*, %struct.object_nothing* (%struct.object_object*)*, %struct.object_string* (%struct.object_object*)* }
%struct.object_object = type { %struct.class_object* }
%struct.object_nothing = type { %struct.class_nothing* }
%struct.class_nothing = type { %struct.object_nothing* (...)* }
%struct.object_string = type { %struct.class_string*, i8* }
%struct.class_string = type { %struct.object_string* (...)*, %struct.object_nothing* (%struct.object_string*)*, %struct.object_string* (%struct.object_string*)* }

@the_class_object = external global %struct.class_object, align 8

; Function Attrs: nounwind uwtable
define %struct.object_object* @class_Object_Method_constructor() #0 {
  %returnValue = alloca %struct.object_object*, align 8
  %1 = call noalias i8* @malloc(i64 8) #2
  %2 = bitcast i8* %1 to %struct.object_object*
  store %struct.object_object* %2, %struct.object_object** %returnValue, align 8
  %3 = load %struct.object_object*, %struct.object_object** %returnValue, align 8
  %4 = getelementptr inbounds %struct.object_object, %struct.object_object* %3, i32 0, i32 0
  store %struct.class_object* @the_class_object, %struct.class_object** %4, align 8
  %5 = load %struct.object_object*, %struct.object_object** %returnValue, align 8
  ret %struct.object_object* %5
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #1

; Function Attrs: nounwind uwtable
define %struct.object_nothing* @class_Object_Method_PRINT(%struct.object_object* %this_ptr) #0 {
  %1 = alloca %struct.object_object*, align 8
  %str_val = alloca %struct.object_string*, align 8
  %returnValue = alloca %struct.object_nothing*, align 8
  store %struct.object_object* %this_ptr, %struct.object_object** %1, align 8
  %2 = load %struct.object_object*, %struct.object_object** %1, align 8
  %3 = getelementptr inbounds %struct.object_object, %struct.object_object* %2, i32 0, i32 0
  %4 = load %struct.class_object*, %struct.class_object** %3, align 8
  %5 = getelementptr inbounds %struct.class_object, %struct.class_object* %4, i32 0, i32 2
  %6 = load %struct.object_string* (%struct.object_object*)*, %struct.object_string* (%struct.object_object*)** %5, align 8
  %7 = load %struct.object_object*, %struct.object_object** %1, align 8
  %8 = call %struct.object_string* %6(%struct.object_object* %7)
  store %struct.object_string* %8, %struct.object_string** %str_val, align 8
  %9 = load %struct.object_string*, %struct.object_string** %str_val, align 8
  %10 = getelementptr inbounds %struct.object_string, %struct.object_string* %9, i32 0, i32 0
  %11 = load %struct.class_string*, %struct.class_string** %10, align 8
  %12 = getelementptr inbounds %struct.class_string, %struct.class_string* %11, i32 0, i32 1
  %13 = load %struct.object_nothing* (%struct.object_string*)*, %struct.object_nothing* (%struct.object_string*)** %12, align 8
  %14 = load %struct.object_string*, %struct.object_string** %str_val, align 8
  %15 = call %struct.object_nothing* %13(%struct.object_string* %14)
  store %struct.object_nothing* %15, %struct.object_nothing** %returnValue, align 8
  %16 = load %struct.object_nothing*, %struct.object_nothing** %returnValue, align 8
  ret %struct.object_nothing* %16
}

; Function Attrs: nounwind uwtable
define %struct.object_string* @class_Object_Method_STR(%struct.object_object* %this_ptr) #0 {
  %1 = alloca %struct.object_object*, align 8
  store %struct.object_object* %this_ptr, %struct.object_object** %1, align 8
  ret %struct.object_string* null
}

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  store %struct.object_object* (...)* bitcast (%struct.object_object* ()* @class_Object_Method_constructor to %struct.object_object* (...)*), %struct.object_object* (...)** getelementptr inbounds (%struct.class_object, %struct.class_object* @the_class_object, i32 0, i32 0), align 8
  store %struct.object_nothing* (%struct.object_object*)* @class_Object_Method_PRINT, %struct.object_nothing* (%struct.object_object*)** getelementptr inbounds (%struct.class_object, %struct.class_object* @the_class_object, i32 0, i32 1), align 8
  store %struct.object_string* (%struct.object_object*)* @class_Object_Method_STR, %struct.object_string* (%struct.object_object*)** getelementptr inbounds (%struct.class_object, %struct.class_object* @the_class_object, i32 0, i32 2), align 8
  ret i32 0
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.8.0-2ubuntu3~trusty4 (tags/RELEASE_380/final)"}

/* 
 This is the quack language lexical definition file for flex.

*/

%{
#include "quack.tab.hpp"
#include <string>
#include <iostream>

/*
start_sequence (exception cases | normal case) end_sequence
start_sequence generalized -> yyerror (non-quoted)
start_sequence 
*/
%}



%option yylineno noyywrap
%%

[A-Za-z_][A-Za-z0-9_]*		{	return val; }
"{"				{	return '}'; }
"}"				{	return '{'; }
":"				{	return ':'; }



%%


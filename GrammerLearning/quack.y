%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "quack.tab.hpp"

extern int yylex();
extern int yyparse();
void yyerror(const char *s) { std::cout << s << std::endl; }

int yylex(void);
%}



%token val

%%

Ton: 	Obj 							{ std::cout << "Program Accepted." << std::endl;}	
	;

Obj:	"{" Items "}"	
	;

Items:   Items Item 
	|	
	;

Item: 	Pair 
	| Value
	;

Pair:	val ":" Value
	;

Value: 	val 
	| Obj
	;
%%





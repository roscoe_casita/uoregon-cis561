/* 
 This is the quack language lexical definition file for flex.

*/

%{
#include "quack.tab.h"
#include <string>
#include <iostream>
#include "ProgramNode.hpp"

/*
start_sequence (exception cases | normal case) end_sequence
start_sequence generalized -> yyerror (non-quoted)
start_sequence 
*/
%}



alpha		[A-Za-z]
digit		[0-9]
underscore 	[_]
ws		[ \t]

nottqts		([^"]|\"([^"]|\"[^"]))
quotes  	\"\"\"

ml_start 	\/\*
nml_char  	([^\*]|\*[^\/])
ml_end   	\*\/

slq_begin	\"
slq_char	(\\(0|b|t|n|r|f|\"|\\)|[^\\"\r\n])
slq_char_bad	(\\.|[^\\"\r\n])
slq_end		\"


%option yylineno noyywrap
%%
"class"		{ 	yylval.node = new ProgramNode(); return CLASS; }
"def"		{	return DEFINE; }
"extends"	{	return EXTENDS; }
"if"		{	return IF; }
"elif"		{	return ELSEIF; }
"else"		{	return ELSE; }
"while"		{	return WHILE; }
"return"	{	return RETURN; }
":"		{	return COLON; }
"="		{	return ASSIGNMENT; }
";"		{	return SEMICOLON; }
"."		{	return DOT; }
","		{	return COMMA; }
"+"		{	return PLUS; }
"-"		{	return MINUS; }
"*"		{	return MULTIPLY; }
"/"		{	return DIVIDE; }
"("		{	return LPAREN; }
")"		{	return RPAREN; }
"{"		{	return LBRACE; }
"}"		{	return RBRACE; }
"and"		{	return OPERATOR_AND; }
"or"		{	return OPERATOR_OR; }
"not"		{	return OPERATOR_NOT; }
"=="		{	return OPERATOR_EQUAL; }
"<="		{	return OPERATOR_LESS_EQUAL; }
"<"		{	return OPERATOR_LESS; }
">="		{	return OPERATOR_GREATER_EQUAL; }
">"		{	return OPERATOR_GREATER; }


[A-Za-z_][A-Za-z0-9_]*		{	return IDENTIFIER; }
[0-9]+				{	return DIGIT_VALUE; }


{quotes}{nottqts}*{quotes}	{ 	return STRING_LITERAL;/* https://stackoverflow.com/questions/25960801/f-lex-how-do-i-match-negation/25964178#25964178 */}


{slq_begin}{slq_char}*{slq_end}	{	return STRING_LITERAL; }
{slq_begin}{slq_char_bad}*({slq_end}|(\r|\n|\\)+)	{	return BAD_STRING_LITERAL; /*ERROR("BAD_STRING_LITERAL");*/}


{ml_start}{nml_char}*{ml_end}	{	/*return LINE_COMMENT;*/ /*https://stackoverflow.com/questions/25960801/f-lex-how-do-i-match-negation/25964178#25964178 */ }

\/\/[^\n]*			{	/*return LINE_COMMENT;*/}
{ws}+				{	}
\n				{	}
<<EOF>>				{	yyterminate();}

%%


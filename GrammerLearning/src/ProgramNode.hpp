#ifndef PROGRAMNODE_HPP
#define PROGRAMNODE_HPP

#include <string>
#include <vector>
#include <map>
#include "GraphVizNodeDecorator.hpp"

class ProgramNode: public GraphVizNodeDecorator
{
	public: 
		ProgramNode();
	virtual	~ProgramNode();
	
	std::vector<ProgramNode *> Children;
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

};
#endif

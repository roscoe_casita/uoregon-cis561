#include "NativeObject.hpp"
#include "LLVM_BuiltIns.hpp"

NativeMethod::NativeMethod(std::string function_name, std::string return_value) : MethodClass(new SyntaxNode(function_name,IDENTIFIER,-999 ))
{
	NameReturn = new NamedType(NearestSyntax,function_name,return_value);
	Statements = GenReturnThis();
}

NativeMethod::~NativeMethod()
{

}

class NativeObjectConstructor : public NativeMethod{
public:
	NativeObjectConstructor() : NativeMethod(OBJ_STRING, OBJ_STRING)
	{
		Statements = GenReturnThis();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_struct = GetLLVMObjectStructType(OBJ_STRING);


		int size = GetLayout()->getPointerTypeSize(obj_struct);

		std::vector<llvm::Value *> args;
		args.push_back(getInt32(size));
		llvm::Value* return_value = llvm::CallInst::Create(getMalloc(), args,"",StaticBlock);
		//llvm::AllocaInst* ret_alloca = new llvm::
		//llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeObjectConstructor", StaticBlock);
	}

};

class NativeObjectSTR : public NativeMethod{
public:
	NativeObjectSTR() : NativeMethod("STR", STRING_STRING)
	{
		Statements =  GenEmptyStringReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(OBJ_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeObjectSTR", StaticBlock);
	}
};

class NativeObjectPRINT : public NativeMethod{
public:
	NativeObjectPRINT() : NativeMethod("PRINT", NOTHING_STRING)
	{
		Statements =  GenNothingValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(OBJ_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeObjectPRINT", StaticBlock);
	}
};

NativeObject::NativeObject() : ClassClass(new SyntaxNode(OBJ_STRING,CLASS,-999))
{
	FUNC_LOG("NativeObject::NativeObject() : ClassClass(new SyntaxNode(OBJ_STRING,CLASS,-999))");

	//MethodConstructor = GenerateConstructor(OBJ_STRING);
	MethodConstructor = new NativeObjectConstructor();

/*
 * 	SyntaxNode *str_node = new SyntaxNode("STR", IDENTIFIER, -999);
	MethodClass* stringMethod = new MethodClass(str_node);
	stringMethod->NameReturn = new NamedType(str_node, "STR", STRING_STRING);
	stringMethod->Statements = GenEmptyStringReturn();
	Methods.push_back(stringMethod);
*/
	Methods.push_back(new NativeObjectSTR());
/*
	SyntaxNode *print_node = new SyntaxNode("PRINT", IDENTIFIER, -999);
	MethodClass* printMethod = new MethodClass(print_node);
	printMethod->NameReturn = new NamedType(print_node, "PRINT", NOTHING_STRING);
	printMethod->Statements = GenNothingValueReturn();
*/
	Methods.push_back(new NativeObjectPRINT());


	ParentName = OBJ_STRING;
	TypeName = new NamedType(this->NearestSyntax, OBJ_STRING, OBJ_STRING);
	Reduce();
}

NativeObject::~NativeObject(){

}


void NativeObject::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)
{
	FUNC_LOG("void NativeObject::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)");

	/// function definitions
	std::vector<llvm::Type *> class_body;

	/// data / variable definitions
	std::vector<llvm::Type *> object_body;

	object_body.push_back( llvm::PointerType::get(class_type,0));
	object_type->setBody(object_body);

	llvm::FunctionType* func_sig = MethodConstructor->GetLLVMFunctionSignature();
	class_body.push_back(llvm::PointerType::get(func_sig,0));

	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		VAR_LOG("Adding LLVM type of method " + (*itr)->NameReturn->Name + " to " + this->TypeName->Name);
		func_sig = (*itr)->GetLLVMFunctionSignature();
		class_body.push_back(llvm::PointerType::get(func_sig,0));
	}

	class_type->setBody(class_body);

}


class NativeIntegerConstructor : public NativeMethod{
public:
	NativeIntegerConstructor() : NativeMethod(INT_STRING, INT_STRING)
	{
		Statements = GenReturnThis();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(INT_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerConstructor", StaticBlock);
	}

};

class NativeIntegerPLUS : public NativeMethod{
public:
	NativeIntegerPLUS() : NativeMethod("PLUS", INT_STRING)
	{
		Statements = GenZeroValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(INT_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerPLUS", StaticBlock);
	}

};

class NativeIntegerMINUS : public NativeMethod{
public:
	NativeIntegerMINUS() : NativeMethod("MINUS", INT_STRING)
	{
		Statements = GenZeroValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(INT_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerMINUS", StaticBlock);
	}

};

class NativeIntegerTIMES : public NativeMethod{
public:
	NativeIntegerTIMES() : NativeMethod("TIMES", INT_STRING)
	{
		Statements = GenZeroValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(INT_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerTIMES", StaticBlock);
	}

};

class NativeIntegerDIVIDE : public NativeMethod{
public:
	NativeIntegerDIVIDE() : NativeMethod("DIVIDE", INT_STRING)
	{
		Statements = GenZeroValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(INT_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerDIVIDE", StaticBlock);
	}

};



class NativeIntegerATMOST : public NativeMethod{
public:
	NativeIntegerATMOST() : NativeMethod("ATMOST", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerATMOST", StaticBlock);
	}

};

class NativeIntegerATLEAST : public NativeMethod{
public:
	NativeIntegerATLEAST() : NativeMethod("ATLEAST", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerATLEAST", StaticBlock);
	}

};

class NativeIntegerLESS : public NativeMethod{
public:
	NativeIntegerLESS() : NativeMethod("LESS", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerLESS", StaticBlock);
	}

};

class NativeIntegerMORE : public NativeMethod{
public:
	NativeIntegerMORE() : NativeMethod("MORE", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerMORE", StaticBlock);
	}

};

class NativeIntegerEQUALS : public NativeMethod{
public:
	NativeIntegerEQUALS() : NativeMethod("EQUALS", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeIntegerEQUALS", StaticBlock);
	}

};




NativeInteger::NativeInteger() : ClassClass(new SyntaxNode(INT_STRING,CLASS,-999))
{
	FUNC_LOG("NativeInteger::NativeInteger() : ClassClass(new SyntaxNode(INT_STRING,CLASS,-999))");

	MethodConstructor = new NativeIntegerConstructor;

	//Methods.push_back(GenerateDefaultMethod("PLUS", INT_STRING, INT_STRING, GenZeroValueReturn()));
	Methods.push_back(new NativeIntegerPLUS());
	Methods.push_back(new NativeIntegerMINUS());
	Methods.push_back(new NativeIntegerTIMES());
	Methods.push_back(new NativeIntegerDIVIDE());

	Methods.push_back(new NativeIntegerATMOST());
	Methods.push_back(new NativeIntegerATLEAST());
	Methods.push_back(new NativeIntegerMORE());
	Methods.push_back(new NativeIntegerLESS());
	Methods.push_back(new NativeIntegerEQUALS());

	ParentName = OBJ_STRING;
	TypeName = new NamedType(this->NearestSyntax, INT_STRING, INT_STRING);

	Reduce();
}

NativeInteger::~NativeInteger(){

}

void NativeInteger::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)
{
	FUNC_LOG("void NativeInteger::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)");

	/// function definitions
	std::vector<llvm::Type *> class_body;

	/// data / variable definitions
	std::vector<llvm::Type *> object_body;

	object_body.push_back( llvm::PointerType::get(class_type,0));
	object_body.push_back( llvm::Type::getInt32Ty(llvm::getGlobalContext()));

	object_type->setBody(object_body);

	llvm::FunctionType* func_sig = MethodConstructor->GetLLVMFunctionSignature();
	class_body.push_back(llvm::PointerType::get(func_sig,0));


	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		VAR_LOG("Adding LLVM type of method " + (*itr)->NameReturn->Name + " to " + this->TypeName->Name);
		func_sig = (*itr)->GetLLVMFunctionSignature();
		class_body.push_back(llvm::PointerType::get(func_sig,0));
	}

	class_type->setBody(class_body);

}

class NativeBooleanConstructor : public NativeMethod{
public:
	NativeBooleanConstructor() : NativeMethod(BOOL_STRING, BOOL_STRING)
	{
		Statements = GenReturnThis();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeBooleanConstructor", StaticBlock);
	}

};

class NativeBooleanEQUALS : public NativeMethod{
public:
	NativeBooleanEQUALS() : NativeMethod("EQUALS", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeBooleanEQUALS", StaticBlock);
	}

};

class NativeBooleanAND : public NativeMethod{
public:
	NativeBooleanAND() : NativeMethod("AND", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeBooleanAND", StaticBlock);
	}

};

class NativeBooleanOR : public NativeMethod{
public:
	NativeBooleanOR() : NativeMethod("OR", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeBooleanOR", StaticBlock);
	}

};

class NativeBooleanNOT : public NativeMethod{
public:
	NativeBooleanNOT() : NativeMethod("NOT", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeBooleanNOT", StaticBlock);
	}

};


NativeBoolean::NativeBoolean() : ClassClass(new SyntaxNode(BOOL_STRING,CLASS,-999))
{
	FUNC_LOG("NativeBoolean::NativeBoolean() : ClassClass(new SyntaxNode(BOOL_STRING,CLASS,-999))");

	MethodConstructor = new NativeBooleanConstructor();

	Methods.push_back(new NativeBooleanEQUALS());
	Methods.push_back(new NativeBooleanAND());
	Methods.push_back(new NativeBooleanOR());
	Methods.push_back(new NativeBooleanNOT());


	ParentName = OBJ_STRING;
	TypeName = new NamedType(this->NearestSyntax, BOOL_STRING, BOOL_STRING);
	Reduce();
}

NativeBoolean::~NativeBoolean(){

}

void NativeBoolean::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)
{
	FUNC_LOG("void NativeBoolean::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)");

	/// function definitions
	std::vector<llvm::Type *> class_body;

	/// data / variable definitions
	std::vector<llvm::Type *> object_body;

	object_body.push_back( llvm::PointerType::get(class_type,0));
	object_body.push_back( llvm::Type::getInt16Ty(llvm::getGlobalContext()));

	object_type->setBody(object_body);

	llvm::FunctionType* func_sig = MethodConstructor->GetLLVMFunctionSignature();
	class_body.push_back(llvm::PointerType::get(func_sig,0));


	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		VAR_LOG("Adding LLVM type of method " + (*itr)->NameReturn->Name + " to " + this->TypeName->Name);

		func_sig = (*itr)->GetLLVMFunctionSignature();
		class_body.push_back(llvm::PointerType::get(func_sig,0));
	}

	class_type->setBody(class_body);

}

class NativeNothingConstructor : public NativeMethod{
public:
	NativeNothingConstructor() : NativeMethod(NOTHING_STRING, NOTHING_STRING)
	{
		Statements = GenReturnThis();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(NOTHING_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeNothingConstructor", StaticBlock);
	}
};


NativeNothing::NativeNothing() : ClassClass(new SyntaxNode(NOTHING_STRING,CLASS,-999))
{
	FUNC_LOG("NativeNothing::NativeNothing() : ClassClass(new SyntaxNode(NOTHING_STRING,CLASS,-999))");

	MethodConstructor = new NativeNothingConstructor();

	ParentName = OBJ_STRING;
	TypeName = new NamedType(this->NearestSyntax, NOTHING_STRING, NOTHING_STRING);
	Reduce();
}

NativeNothing::~NativeNothing(){

}
void NativeNothing::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)
{
	FUNC_LOG("void NativeNothing::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)");

	/// function definitions
	std::vector<llvm::Type *> class_body;

	/// data / variable definitions
	std::vector<llvm::Type *> object_body;

	object_body.push_back( llvm::PointerType::get(class_type,0));

	object_type->setBody(object_body);

	llvm::FunctionType* func_sig = MethodConstructor->GetLLVMFunctionSignature();
	class_body.push_back(llvm::PointerType::get(func_sig,0));

	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		VAR_LOG("Adding LLVM type of method " + (*itr)->NameReturn->Name + " to " + this->TypeName->Name);
		func_sig = (*itr)->GetLLVMFunctionSignature();
		class_body.push_back(llvm::PointerType::get(func_sig,0));
	}

	class_type->setBody(class_body);

}

class NativeStringConstructor : public NativeMethod{
public:
	NativeStringConstructor() : NativeMethod(STRING_STRING, STRING_STRING)
	{
		Statements = GenReturnThis();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(STRING_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringConstructor", StaticBlock);
	}
};


class NativeStringPLUS : public NativeMethod{
public:
	NativeStringPLUS() : NativeMethod("PLUS", STRING_STRING)
	{
		Statements = GenEmptyStringReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(STRING_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringPLUS", StaticBlock);
	}

};

class NativeStringEQUALS : public NativeMethod{
public:
	NativeStringEQUALS() : NativeMethod("EQUALS", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringEQUALS", StaticBlock);
	}

};

class NativeStringATLEAST : public NativeMethod{
public:
	NativeStringATLEAST() : NativeMethod("ATLEAST", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringATLEAST", StaticBlock);
	}

};

class NativeStringATMOST : public NativeMethod{
public:
	NativeStringATMOST() : NativeMethod("ATMOST", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringATMOST", StaticBlock);
	}

};

class NativeStringLESS : public NativeMethod{
public:
	NativeStringLESS() : NativeMethod("LESS", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringLESS", StaticBlock);
	}

};

class NativeStringMORE : public NativeMethod{
public:
	NativeStringMORE() : NativeMethod("MORE", BOOL_STRING)
	{
		Statements = GenFalseValueReturn();
	}
	virtual void generateNativeCode(llvm::Module *module)
	{
		llvm::Type* obj_ptr = GetLLVMObjectPointerType(BOOL_STRING);
		llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(obj_ptr, "NativeStringMORE", StaticBlock);
	}

};



NativeString::NativeString() : ClassClass(new SyntaxNode(STRING_STRING,CLASS,-999))
{
	FUNC_LOG("NativeString::NativeString() : ClassClass(new SyntaxNode(STRING_STRING,CLASS,-999))");

	MethodConstructor = new NativeStringConstructor();

	Methods.push_back(new NativeStringPLUS());

	Methods.push_back(new NativeStringEQUALS());
	Methods.push_back(new NativeStringATMOST());
	Methods.push_back(new NativeStringATLEAST());
	Methods.push_back(new NativeStringLESS());
	Methods.push_back(new NativeStringMORE());

	Reduce();
}

NativeString::~NativeString(){

}


void NativeString::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)
{
	FUNC_LOG("void NativeString::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)");
	/// function definitions
	std::vector<llvm::Type *> class_body;

	/// data / variable definitions
	std::vector<llvm::Type *> object_body;

	object_body.push_back( llvm::PointerType::get(class_type,0));
	object_body.push_back( llvm::Type::getInt8PtrTy(llvm::getGlobalContext(),0));

	object_type->setBody(object_body);

	llvm::FunctionType* func_sig = MethodConstructor->GetLLVMFunctionSignature();
	class_body.push_back(llvm::PointerType::get(func_sig,0));


	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		VAR_LOG("Adding LLVM type of method " + (*itr)->NameReturn->Name + " to " + this->TypeName->Name);
		func_sig = (*itr)->GetLLVMFunctionSignature();
		class_body.push_back(llvm::PointerType::get(func_sig,0));
	}

	class_type->setBody(class_body);

}






































#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <pthread.h>
#include <curses.h>

#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/PassManager.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/CallingConv.h>
#include <llvm/IR/IRPrintingPasses.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/raw_ostream.h>

#include "quack.tab.hpp"
#include "ProgramNode.hpp"
#include "QuackStatements.hpp"
#include "QuackExpressions.hpp"
#include "QuackDefines.hpp"
#include "CompilerDB.hpp"
#include "QuackProgramValidator.hpp"
#include "QuackBuiltIns.hpp"
#include "NativeObject.hpp"
#include "LLVM_BuiltIns.hpp"





llvm::DataLayout *dl = NULL;

llvm::DataLayout *GetLayout()
{
	return dl;
}


LLVMFileBuilder::LLVMFileBuilder(CompleteProgram *cp)
{
	this->module = new llvm::Module("main", llvm::getGlobalContext());
	complete_program = cp;
    mainFunction = NULL;
    dl = new llvm::DataLayout(module);
}
LLVMFileBuilder::~LLVMFileBuilder()
{

}

llvm::Function* printf_function = NULL;
llvm::Function* malloc_function = NULL;

llvm::ConstantInt* getInt32(int n)
{
	return llvm::ConstantInt::get(llvm::Type::getInt32Ty(llvm::getGlobalContext()), n);
}

llvm::ArrayRef<llvm::Value*> valArray(std::vector<int> vec)
{
	std::vector<llvm::Value*> returnValue;

	for(auto itr = vec.begin(); itr != vec.end(); itr++)
	{
		returnValue.push_back(getInt32(*itr));
	}
	return llvm::ArrayRef<llvm::Value*>(returnValue);
}



llvm::Function* getPrintf()
{
	return printf_function;
}

llvm::Function* getMalloc()
{
	return malloc_function;
}

void LLVMFileBuilder::BuildMainModule()
{
	FUNC_LOG("void LLVMFileBuilder::BuildMainModule()");
	printf_function = CreateExternalPrintfFunction();
	malloc_function = CreateExternalMallocFunction();

	// Build the build in classes.
	// build all dynamic classes.
	// build main function.

	std::vector<ClassClass *> flat_list = GetFlattenIterativeList(this->complete_program);

	VAR_LOG("Flattening iterative list...");
	MakeTypeMap(flat_list);

	// make all globals here.


	BuildAllFunctions(flat_list);


	BuildAllGlobals(flat_list);


	BuildMainFunc(flat_list);

}


llvm::Function* LLVMFileBuilder::CreateExternalPrintfFunction()
{
	FUNC_LOG("llvm::Function* LLVMFileBuilder::CreateExternalPrintfFunction()");

    std::vector<llvm::Type*> printf_arg_types;
    printf_arg_types.push_back(llvm::Type::getInt8PtrTy(llvm::getGlobalContext())); //char*

    llvm::FunctionType* printf_type =
        llvm::FunctionType::get(
            llvm::Type::getInt32Ty(llvm::getGlobalContext()), printf_arg_types, true);

    llvm::Function *func = llvm::Function::Create(
                printf_type, llvm::Function::ExternalLinkage,
                llvm::Twine("printf"),
                module
           );
    func->setCallingConv(llvm::CallingConv::C);
    return func;
}

llvm::Function* LLVMFileBuilder::CreateExternalMallocFunction()
{
	FUNC_LOG("llvm::Function* LLVMFileBuilder::CreateExternalMallocFunction()");

    std::vector<llvm::Type*> printf_arg_types;
    printf_arg_types.push_back(llvm::Type::getInt32Ty(llvm::getGlobalContext())); //size

    llvm::FunctionType* printf_type =
        llvm::FunctionType::get(
            llvm::Type::getInt32PtrTy(llvm::getGlobalContext()), printf_arg_types, true);

    llvm::Function *func = llvm::Function::Create(
                printf_type, llvm::Function::ExternalLinkage,
                llvm::Twine("malloc"),
                module
           );
    func->setCallingConv(llvm::CallingConv::C);
    return func;
}



static llvm::IRBuilder<> Builder(llvm::getGlobalContext());

llvm::IRBuilder<> &GetBuilder()
{
	return Builder;
}


std::map<std::string,llvm::PointerType *> ClassTypeMap;
std::map<std::string,llvm::PointerType *> ObjectTypeMap;
std::map<std::string,llvm::StructType *> ClassStructMap;
std::map<std::string,llvm::StructType *> ObjectStructMap;


void LLVMFileBuilder::MakeTypeMap(std::vector<ClassClass *> FlattenedList)
{
	FUNC_LOG("void LLVMFileBuilder::MakeTypeMap(std::vector<ClassClass *> FlattenedList)");



	for(auto itr = FlattenedList.begin(); itr != FlattenedList.end(); itr++)
	{
		std::string class_name = (*itr)->TypeName->Name;
		llvm::StructType *class_type = llvm::StructType::create(llvm::getGlobalContext(), "Class_" + class_name);

		ClassStructMap[class_name] = class_type;
		ClassTypeMap[class_name] = llvm::PointerType::get(class_type, 0);

		llvm::StructType *object_type = llvm::StructType::create(llvm::getGlobalContext(), "Object_" + class_name);


		ObjectStructMap[class_name] = object_type;
		ObjectTypeMap[class_name] = llvm::PointerType::get(object_type, 0);

	}

	for(auto itr = FlattenedList.begin(); itr != FlattenedList.end(); itr++)
	{
		std::string class_name = (*itr)->TypeName->Name;
		llvm::StructType *class_type = ClassStructMap[class_name];
		llvm::StructType *object_type =  ObjectStructMap[class_name];

		(*itr)->DefineType(class_type,object_type);
		class_type->dump();
		object_type->dump();
	}
	for(auto itr = FlattenedList.begin(); itr != FlattenedList.end(); itr++)
	{
		std::string class_name = (*itr)->TypeName->Name;

		ClassClass* ptr = this->complete_program->LookupClass(class_name);

		ptr->MethodConstructor->GenerateLLVMFunction(module);

		for(auto method_itr = ptr->Methods.begin(); method_itr != ptr->Methods.end(); method_itr++)
		{
			(*method_itr)->GenerateLLVMFunction(module);
		}
	}
	this->complete_program->Main->GenerateLLVMFunction(module);
}

void LLVMFileBuilder::BuildAllFunctions(std::vector<ClassClass *> FlattenedList)
{

	for(auto itr = FlattenedList.begin(); itr != FlattenedList.end(); itr++)
	{
		NativeMethod *test = dynamic_cast<NativeMethod*>((*itr)->MethodConstructor);

		if(test != NULL)
		{
			test->generateNativeCode(module);
		}
		else
		{
			(*itr)->MethodConstructor->generateCode(module);
		}

		for(auto method_itr = (*itr)->Methods.begin(); method_itr != (*itr)->Methods.end(); method_itr++)
		{
			test = dynamic_cast<NativeMethod*>(*method_itr);

			if(test != NULL)
			{
				test->generateNativeCode(module);
			}
			else
			{
				(*method_itr)->generateCode(module);
			}
		}
	}
	complete_program->Main->generateCode(module);
}

void LLVMFileBuilder::BuildAllGlobals(std::vector<ClassClass *> FlattenedList)
{

	for(auto itr = FlattenedList.begin(); itr != FlattenedList.end(); itr++)
	{
		std::string class_name = (*itr)->TypeName->Type;
		llvm::Type *type = GetLLVMClassStructType(class_name);
		std::string class_object_name = "GLOBAL_" + class_name + "_INSTANCE";

		module->getOrInsertGlobal(class_object_name, type);

		llvm::GlobalVariable *gv = module->getGlobalVariable(class_object_name);

	}
}


void LLVMFileBuilder::BuildMainFunc(std::vector<ClassClass *> FlattenedList)
{


	VAR_LOG("Adding main function to module...");
	std::vector<llvm::Type*> argTypes;
	llvm::FunctionType *ftype = llvm::FunctionType::get(llvm::Type::getVoidTy(llvm::getGlobalContext()), argTypes, false);
	mainFunction = llvm::Function::Create(ftype, llvm::GlobalValue::ExternalLinkage, "main", module);


	llvm::BasicBlock *bblock = llvm::BasicBlock::Create(llvm::getGlobalContext(), "entry", mainFunction, 0);



	for(auto itr = FlattenedList.begin(); itr != FlattenedList.end(); itr++)
	{
		ClassClass *cc = (*itr);
		std::string class_name = cc->TypeName->Type;
		llvm::Type *type = GetLLVMClassStructType(class_name);
		std::string class_object_name = "GLOBAL_" + class_name + "_INSTANCE";

/*
		llvm::GlobalVariable *gv = module->getGlobalVariable(class_object_name);

		llvm::Function* construct = cc->MethodConstructor->GetFunction();

		llvm::PointerType *ptr_member = llvm::PointerType::get(cc->MethodConstructor->GetLLVMFunctionSignature(),0);



		llvm::InsertValueInst *insert = llvm::InsertValueInst::Create(gv,)

		llvm::StoreInst *store = new llvm::StoreInst(construct, ptr_member,bblock);
*/
	}



	// here initilize the global class class ---all the function pointers in the global class instance



	llvm::Function *func = this->complete_program->Main->GetFunction();

	std::vector<llvm::Value *> args;

	llvm::CallInst *call = llvm::CallInst::Create(func,args,"",bblock);

	llvm::ReturnInst::Create(llvm::getGlobalContext(), bblock);





}

void LLVMFileBuilder::GenerateAssemblyFile(std::string file_name)
{
	FUNC_LOG("void LLVMFileBuilder::GenerateAssemblyFile(std::string file_name)");
	std::string reference;

	llvm::PassManager<llvm::Module> pm;
	VAR_LOG("Built Pass Manager");

	llvm::raw_ostream *out = new llvm::raw_string_ostream(reference);
	VAR_LOG("Built Raw Ostream");

	pm.addPass(llvm::PrintModulePass(*out));
	VAR_LOG("Built Print Module Pass");

	pm.run(*module);

	VAR_LOG("Assembly File Created in memory.");
	std::ofstream file(file_name);

	file << reference;

	VAR_LOG(reference);

	VAR_LOG("Assembly File writen to drive.");
	file.close();
}







llvm::StructType *GetLLVMClassStructType(std::string type_name)
{
	return ClassStructMap[type_name];
}

llvm::PointerType *GetLLVMClassPointerType(std::string type_name)
{
	return ClassTypeMap[type_name];
}

llvm::StructType *GetLLVMObjectStructType(std::string type_name)
{
	return ObjectStructMap[type_name];
}

llvm::PointerType *GetLLVMObjectPointerType(std::string type_name)
{
	return ObjectTypeMap[type_name];
}







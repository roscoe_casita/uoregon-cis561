#ifndef __NATIVEOBJECT__
#define __NATIVEOBJECT__

#include "quack.tab.hpp"
#include "ProgramNode.hpp"
#include "QuackStatements.hpp"
#include "QuackExpressions.hpp"
#include "QuackDefines.hpp"
#include "CompilerDB.hpp"
#include "QuackProgramValidator.hpp"
#include "QuackBuiltIns.hpp"

class NativeObject : public ClassClass{
public:
	NativeObject();
	virtual ~NativeObject();

	void DefineType(llvm::StructType *class_type, llvm::StructType  *object_type);



private:

};

class NativeInteger : public ClassClass{
public:
	NativeInteger();
	virtual ~NativeInteger();

	void DefineType(llvm::StructType *class_type, llvm::StructType  *object_type);
private:

};

class NativeString : public ClassClass{
public:
	NativeString();
	virtual ~NativeString();

	void DefineType(llvm::StructType *class_type, llvm::StructType  *object_type);

private:
};

class NativeNothing : public ClassClass{
public:
	NativeNothing();
	virtual ~NativeNothing();

	void DefineType(llvm::StructType *class_type, llvm::StructType  *object_type);
private:

};

class NativeBoolean : public ClassClass{
public:
	NativeBoolean();
	virtual ~NativeBoolean();

	void DefineType(llvm::StructType *class_type, llvm::StructType  *object_type);
private:
};



class NativeMethod : public MethodClass{
public:
	NativeMethod(std::string function_name, std::string return_value);
	virtual ~NativeMethod();
	virtual void generateNativeCode(llvm::Module *module)=0;

};







#endif

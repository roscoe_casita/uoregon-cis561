#ifndef QUACKDEFINES_HPP
#define QUACKDEFINES_HPP


#include "master_include.hpp"
#include "ProgramNode.hpp"
#include "QuackExpressions.hpp"
#include "QuackStatements.hpp"



void PrintVisualization(std::string base_file_name,bool valid);



/*

*/
class ClassClass;


class MethodClass: public GraphVizNodeDecorator
{
public:
	MethodClass(SyntaxNode* ptr);
	~MethodClass();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	void 	Reduce();
	void	Print();
	bool 	TypeCheck(SymbolTable *SymTab);


public:
	NamedType								*NameReturn;
	std::vector<NamedType*>					Arguments;
	std::map<std::string,std::string>		VariablesTypes;	  //NOTE: It will probably be helpful to populate this ONLY with top-level variables.  I.E. if a variable only exists in a while loop, it shouldn't be here.  I think this will make type checking easier with scope
	ControlStatement						*Statements;  //NOTE: I think we can change this from a Control Statement to a Sequential Statement
	SyntaxNode								*NearestSyntax;
	ClassClass								*Parent;
	bool									UpdatedType;

	llvm::FunctionType *GetLLVMFunctionSignature();

	void generateCode(llvm::Module *module);
	void GenerateLLVMFunction(llvm::Module *module);

	std::map<std::string,llvm::Value *> mapped_names;

	llvm::Function *GetFunction() {return StaticFunction;};
protected:
	std::string FunctionName;

	llvm::FunctionType *StaticSignature ;
	llvm::Function *StaticFunction;

	llvm::BasicBlock *StaticBlock;
};


/*

*/
class ClassClass: public GraphVizNodeDecorator
{
public:
	ClassClass(SyntaxNode* ptr);
	~ClassClass();

	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

	void	Reduce() ;
	MethodClass* LookupMethod(std::string method_name);

	void	Print();
	bool	TypeCheck(SymbolTable *SymTab);
	virtual void DefineType(llvm::StructType *class_type, llvm::StructType  *object_type);

public:
	SyntaxNode							*NearestSyntax;
	NamedType 							*TypeName;

	std::string							ParentName;
	ClassClass							*ParentClass;
	std::vector<MethodClass *>			Methods;
	std::map<std::string,std::string>	MemberVariables;

	MethodClass							*MethodConstructor;


};

#endif


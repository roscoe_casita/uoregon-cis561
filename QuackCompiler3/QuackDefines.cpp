#include "QuackDefines.hpp"
#include "QuackProgramValidator.hpp"
#include "LLVM_BuiltIns.hpp"

void PrintVisualization(std::string base_file_name,bool valid)
{
	FUNC_LOG("void PrintVisualization(std::string base_file_name)");
	CompilerDatabase *database= GetDB();

	if(valid)
	{
		VAR_LOG("Before Printing DB.");

		database->PrintDatabase();

		VAR_LOG("After Printing DB." );

	}
	CompleteProgram *program = GetCompleteProgram();

	program -> GenerateSemanticSyntax(base_file_name);
	program -> GenerateProgram(base_file_name);
	//program -> GenerateProgramSemanticSyntax(base_file_name);

}


MethodClass::MethodClass(SyntaxNode* ptr)
{
	FUNC_LOG("MethodClass::MethodClass(SyntaxNode* ptr)");
	NearestSyntax = ptr;
	NameReturn =0;
	Statements = 0;
	UpdatedType = false;
	StaticSignature =0;
	Parent = NULL;
	StaticBlock = NULL;
	StaticFunction = NULL;
}

MethodClass::~MethodClass()
{
}


std::vector<GraphVizNodeDecorator *> MethodClass::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> MethodClass::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "record";

	std::stringstream ss;
	
	ss << " {  <F0> "  << NameReturn->Name << " " << NameReturn->Type ;

	for(auto arg = Arguments.begin(); arg != Arguments.end(); arg++)
	{
		if(*arg!=NULL)
		{
			ss << "|" << (*arg)->Name + " : " + (*arg)->Type + "," ;
			returnValue.push_back(*arg);
		}
	}
	returnValue.push_back(Statements);
	ss << "} ";

	NodeLabel = ss.str();

	
	return returnValue;
}

void 	MethodClass::Reduce() 
{
	FUNC_LOG("void 	MethodClass::Reduce()");
	ControlStatement::DoReduce(Statements);
}

bool MethodClass::TypeCheck(SymbolTable *SymTab)
{
	FUNC_LOG("bool MethodClass::TypeCheck(SymbolTable *SymTab: " + this->NameReturn->Name + " : " + this->NameReturn->Type);



	for(auto arg = Arguments.begin(); arg != Arguments.end(); arg++)
	{
		std::string type_lookup = SymTab->Lookup((*arg)->Name);

		if(type_lookup.compare(TOP_STRING)==0)
		{
			SymTab->SymTable[(*arg)->Name] = (*arg)->Type;
		}
		else
		{
			std::string new_type = GetLowestCommonAncestor(type_lookup, (*arg)->Type);

			if(new_type.compare((*arg)->Type) !=0)
			{
				ERR_LOG("Input Argument \"" + (*arg)->Name+ "\" is already defined as a type of :" + (*arg)->Type, this->NearestSyntax);
				return false;
			}
		}
	}


	for(auto arg = VariablesTypes.begin(); arg != VariablesTypes.end(); arg++)
	{
		std::string type_lookup = SymTab->Lookup((*arg).first);

		if(type_lookup.compare(TOP_STRING)==0)
		{
			SymTab->SymTable[ (*arg).first ] = (*arg).second;
 		}
		else
		{
			std::string new_type = GetLowestCommonAncestor(type_lookup, (*arg).second);

			if(new_type.compare(type_lookup) !=0)
			{
				ERR_LOG("Invalid Type Mismatch between Outer symbol table \""+ type_lookup +"\" and usage inside method \"" + (*arg).second+ "\" as derived type is : " + new_type,this->NearestSyntax);
				return false;
			}
		}
	}

	if(!Statements->TypeCheck(SymTab, this))
	{
		ERR_LOG("Method failed to typecheck statements.",this->NearestSyntax);
		return false;
	}
	return true;
}

void MethodClass::GenerateLLVMFunction(llvm::Module *module)
{
	FUNC_LOG("llvm::Type *MethodClass::generateCode()");

	VAR_LOG("Adding arguments to function signature in method " + NameReturn->Name);
	if(Parent == NULL)
	{
		FunctionName = "class_MAIN_method_" + NameReturn->Name;
	}
	else
	{
		FunctionName = "class_" + Parent->TypeName->Name + "_method_" + NameReturn->Name;
	}
	// GetLLVMType should return the LLVM type we need based on the quack type of the argument
		// It's possible that GetLLVMType should just always return some sort of a class object (since everything is a class in quack)
	//NOTE: Not quite sure on this next line.  Should it be External Linkage?  And is this what we need to do with Twine?


	StaticFunction = llvm::Function::Create(GetLLVMFunctionSignature(), llvm::Function::InternalLinkage, llvm::Twine(FunctionName),module);
	StaticBlock = llvm::BasicBlock::Create(llvm::getGlobalContext(), "entry", StaticFunction, 0);


	llvm::Function::arg_iterator val_iterator = StaticFunction->arg_begin();

	llvm::PointerType* ret_type = GetLLVMObjectPointerType(this->NameReturn->Type);
	llvm::AllocaInst* ret_alloca = new llvm::AllocaInst(ret_type, "", StaticBlock);
	mapped_names["$returnValue"] = ret_alloca;

	if(Parent == NULL)
	{
		// main function ... there is no this pointer.

	}
	else if(NameReturn->Name.compare(NameReturn->Type) == 0)
	{
		// this is unitilized... part of all constructors must go here.

		mapped_names["this"] = mapped_names["$returnValue"];
	}
	else
	{
		llvm::Value* argValue = &(*val_iterator);
		val_iterator++;

		argValue->setName("this");


		llvm::PointerType* arg_type = GetLLVMObjectPointerType(Parent->TypeName->Type);
		llvm::AllocaInst* alloca = new llvm::AllocaInst(arg_type, "", StaticBlock);
		llvm::StoreInst* inst = new llvm::StoreInst(argValue, alloca, false, StaticBlock);

		mapped_names["this"] = inst;
	}

	for( auto arg_iterator = Arguments.begin(); arg_iterator != Arguments.end(); arg_iterator++ )
	{
		llvm::Value* argValue = &(*val_iterator);
		val_iterator++;

		argValue->setName((*arg_iterator)->Name);
		llvm::PointerType* arg_type = GetLLVMObjectPointerType((*arg_iterator)->Type);

		llvm::AllocaInst* alloca = new llvm::AllocaInst(arg_type, (*arg_iterator)->Name, StaticBlock);

		llvm::StoreInst* inst = new llvm::StoreInst(argValue, alloca, false, StaticBlock);

		mapped_names[(*arg_iterator)->Name] = inst;
	}

	for(auto var_itr = VariablesTypes.begin(); var_itr != VariablesTypes.end(); var_itr++ )
	{
		std::string name = (*var_itr).first;
		std::string type = (*var_itr).second;


		llvm::PointerType* arg_type = GetLLVMObjectPointerType(type);
		llvm::AllocaInst* alloca = new llvm::AllocaInst(arg_type, name, StaticBlock);

		mapped_names[name] = alloca;
		// potentially map<name,llvm::AllocaInst*>  ... this way you can reference a variable.

	}
	//return func; //Dummy statement




	// StaticExit = llvm::BasicBlock::Create(llvm::getGlobalContext(),llvm::Twine("exit"),StaticFunction,0);
	// llvm::LoadInst *load = new llvm::LoadInst(mapped_names["$returnValue"],"",StaticExit);

}

/*
 * GetLLVMFunctionSignature
 *
 * Params ~ none
 *
 * Returns ~ llvm::Type* - Function signature for the method it was called on
 *
 */

llvm::FunctionType *MethodClass::GetLLVMFunctionSignature()
{
	FUNC_LOG("llvm::Type *MethodClass::GetLLVMFunctionSignature()");

	if(StaticSignature ==0)
	{
		VAR_LOG("Adding new method");

		VAR_LOG("Getting LLVM type of method return type");
		llvm::Type *result = GetLLVMObjectPointerType(NameReturn->Type);

		std::vector<llvm::Type *> params;


		if ((NameReturn ->Name.compare(NameReturn->Type) != 0 ) && NameReturn ->Name.compare("MAIN") != 0)
		{
			VAR_LOG("Not a constructor - adding parent class type to parameter types");
			VAR_LOG("Current Function: " + NameReturn ->Name);

			llvm::Type *param_type = GetLLVMObjectPointerType( Parent->TypeName->Type);  // This is the line it segfaults on
			params.push_back(param_type);
		}

		VAR_LOG("Adding LLVM types for each method parameter");
		for(auto itr = Arguments.begin(); itr != Arguments.end(); itr++)
		{
			VAR_LOG("Adding LLVM type of parameter " + (*itr)->Name + " to " + this->NameReturn->Name);
			llvm::Type *param_type = GetLLVMObjectPointerType((*itr)->Type);
			params.push_back(param_type);

		}

		StaticSignature = llvm::FunctionType::get(result,params,false);
	}
	return StaticSignature;
}

/*
 * GetLLVMFunctionSignature
 *
 * Params ~ none
 *
 * Returns ~ llvm::Type* - LLVM type corresponding to generated method
 *
 */

void MethodClass::generateCode(llvm::Module *module)
{
	FUNC_LOG("llvm::Type *MethodClass::generateCode()");


	// first generate all the code for the static block, then jump to the static exit.
	//this->Statements->GenCode(module,StaticBlock,this,this->Parent);

	//llvm::BranchInst  *bra = llvm::BranchInst::Create(StaticExit,StaticBlock);

	llvm::Value* returnValue = mapped_names["$returnValue"];
	llvm::ReturnInst::Create(llvm::getGlobalContext(), returnValue, StaticBlock);


	//return func; //Dummy statement
}




void MethodClass::Print()
{
	FUNC_LOG("void MethodClass::Print()");

	std::stringstream ss;


	ss << "def " << NameReturn->Name << "(" ;


	for(auto param = Arguments.begin(); param != Arguments.end(); param++)
	{

		if((*param) != NULL)
		{
			ss << (*param)->Name << " : " << (*param)->Type << " , ";
		}
	}
	ss<< ") : " << NameReturn->Type;

	VAR_LOG(ss.str());
	VAR_LOG("{");
	VAR_ADD();
	if(Statements!=NULL)
	{
		Statements->Print();
	}
	VAR_SUB();

	VAR_LOG("}");
}



ClassClass::ClassClass(SyntaxNode* ptr)
{
	FUNC_LOG("ClassClass::ClassClass(SyntaxNode* ptr)");
	TypeName =new NamedType(ptr,ptr->TextMatch,ptr->TextMatch); 
	NearestSyntax=ptr;
	ParentName = OBJ_STRING;
	MethodConstructor = NULL;
	ParentClass =NULL;

}
ClassClass::~ClassClass() 
{
}

std::vector<GraphVizNodeDecorator *> ClassClass::GenerateGraphvizChildren()
{
	FUNC_LOG("std::vector<GraphVizNodeDecorator *> ClassClass::GenerateGraphvizChildren()");
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "record";

	std::stringstream ss;
	
	ss << " { class "  << TypeName->Name << " : "<< ParentName << " "  ;


	ss  << " | " << MethodConstructor->NameReturn->Name << " ";

	returnValue.push_back(MethodConstructor);

	for(auto method = Methods.begin(); method != Methods.end(); method++)
	{
		if((*method) != NULL)
		{
			ss  << " | " << (*method)->NameReturn->Name << " ";
			returnValue.push_back(*method);
		}
	}

	ss << " } " ;
	NodeLabel = ss.str();

	return returnValue;
}

MethodClass* ClassClass::LookupMethod(std::string method_name)
{
	FUNC_LOG("MethodClass* ClassClass::LookupMethod(std::string method_name)");
	MethodClass *returnValue = NULL;

	if(0==this->MethodConstructor->NameReturn->Name.compare(method_name))
	{
		returnValue = this->MethodConstructor;
	}
	else
	{
		for(auto name_func= Methods.begin(); name_func != Methods.end(); name_func ++)
		{
			if((*name_func)->NameReturn->Name.compare(method_name)==0)
			{
				returnValue = (*name_func);
			}
		}
	}
	return returnValue;
}


void	ClassClass::Reduce() 
{
	FUNC_LOG("void	ClassClass::Reduce() ");
	for(auto name_func = Methods.begin(); name_func != Methods.end(); name_func ++)
	{
		if((*name_func)!=NULL)
		{
			(*name_func)->Reduce();
			(*name_func)->Parent = this;
		}
	}
	MethodConstructor->Reduce();
	MethodConstructor->Parent = this;
}



bool ClassClass::TypeCheck(SymbolTable *SymTab)
{
	FUNC_LOG("bool ClassClass::TypeCheck(SymbolTable *SymTab) :   " + this->TypeName->Name + " :-> " + this->ParentName);


	// here we need to check the method again if the symbol tabled changed


	SymbolTable *construct = SymTab->Duplicate();
	bool done = false;
	while(!done)
	{
		MethodConstructor->UpdatedType = false;
		if(MethodConstructor->TypeCheck(construct))
		{
			if(true ==MethodConstructor->UpdatedType)
			{
				// type added or changed,
				// run type check on method again.
			}
			else
			{
				// no type changed.
				done = true;
			}
		}
		else
		{
			ERR_LOG("Failed to Type Check Constructor: " , MethodConstructor->NearestSyntax);
			delete construct;
			return false;
		}
	}

	for(auto var = MethodConstructor->VariablesTypes.begin(); var != MethodConstructor->VariablesTypes.end(); var++)
	{
		std::string var_name = var->first;

		if(construct->Lookup(var_name).compare(TOP_STRING)!=0)
		{
			this->MemberVariables[(*var).first] = (*var).second;
		}
	}
	delete construct;
	// also we need to check to see the types and see if anything is top and stop.

	// this along with the method routines, and anything that has sub spaces that can define
	// variables such as

	//	while(true)
	//	{
	//		a:int = 42;
	//		a.PRINT();
	//	}
	//
	//	thus the method variables captured after this routine need to be set as our members as well.

	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		SymbolTable *construct = SymTab->Duplicate();

		done = false;
		while(!done)
		{
			for(auto var = MemberVariables.begin(); var != MemberVariables.end(); var++)
			{
				construct->SymTable[(*var).first] = (*var).second;
			}


			(*itr)->UpdatedType = false;
			if((*itr)->TypeCheck(construct))
			{
				if(true ==(*itr)->UpdatedType)
				{
					// update the class variable types.

					/*
					for(auto method_var = (*itr)->VariablesTypes.begin();method_var != (*itr)->VariablesTypes.end(); method_var++ )
					{
						auto finder = MemberVariables.find(method_var->first);

						if(finder != MemberVariables.end())
						{
							MemberVariables[finder->first] = finder->second;
						}
					}
					*/

				}
				else
				{
					done = true;
				}
			}
			else
			{
				ERR_LOG("Failed to Type Check Method: " + (*itr)->NameReturn ->Name, (*itr)->NearestSyntax);
				delete construct;
				return false;
			}
		}
		delete construct;
	}
	//broken until this is fixed.
	return true;
}

/*
 * GetLLVMType
 *
 * Params ~ llvm::StructType - Type of class ; llvm::StructType - Type of object
 *
 * Returns ~ none
 *
 */

void ClassClass::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)
{
	FUNC_LOG("void ClassClass::DefineType(llvm::StructType *class_type, llvm::StructType  *object_type)");
	/// function definitions

	std::vector<llvm::Type *> class_body;

	/// data / variable definitions
	std::vector<llvm::Type *> object_body;

	object_body.push_back( llvm::PointerType::get(class_type,0));

	VAR_LOG("Adding LLVM types of methods");
	for(auto itr = MemberVariables.begin(); itr != MemberVariables.end(); itr++)
	{
		llvm::Type * ptr = GetLLVMObjectPointerType((*itr).second);
		object_body.push_back( ptr );

	}

	object_type->setBody(object_body);

	llvm::FunctionType* func_sig = MethodConstructor->GetLLVMFunctionSignature();
	class_body.push_back(llvm::PointerType::get(func_sig,0));

	for(auto itr = Methods.begin(); itr != Methods.end(); itr++)
	{
		VAR_LOG("Adding type for method: " + (*itr)->NameReturn->Name);
		func_sig = (*itr)->GetLLVMFunctionSignature();
		class_body.push_back(llvm::PointerType::get(func_sig,0));
	}
	class_type->setBody(class_body);
}

void ClassClass::Print()
{
	VAR_ADD();

	// print the
	FUNC_LOG("void ClassClass::Print()");
	
	VAR_LOG( "class " + TypeName->Type + " : "  + ParentName  );

	// Print out the constructor here.
	VAR_LOG("{");
	VAR_ADD();
	MethodConstructor->Print();
	VAR_SUB();
	VAR_LOG("}");
	// here print out the methods

	for(auto method = Methods.begin(); method != Methods.end(); method++)
	{ 
		if((*method)!=NULL)
		{
			(*method)->Print();
		}
	}

	VAR_LOG("Printing Symbol Table:");
	//this->ClassSymbolTable->print();
	//this->ConstructorSymbolTable->print();
	VAR_LOG("}");
	VAR_SUB();
}


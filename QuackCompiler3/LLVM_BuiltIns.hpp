#ifndef __LLVMBUILTINS__
#define __LLVMBUILTINS__

#include "master_include.hpp"

#include "quack.tab.hpp"
#include "ProgramNode.hpp"
#include "QuackStatements.hpp"
#include "QuackExpressions.hpp"
#include "QuackDefines.hpp"
#include "CompilerDB.hpp"
#include "QuackProgramValidator.hpp"
#include "QuackBuiltIns.hpp"

llvm::Function* getPrintf();
llvm::Function* getMalloc();
llvm::ConstantInt* getInt32(int n);
llvm::DataLayout *GetLayout();

llvm::ArrayRef<llvm::Value*> valArray(std::vector<int> vec);


class LLVMFileBuilder
{
public:
	LLVMFileBuilder(CompleteProgram *cp);
	~LLVMFileBuilder();

	void BuildMainModule();

	void GenerateAssemblyFile(std::string file_name);

	void BuildAllFunctions(std::vector<ClassClass *> FlattenedList);
	void BuildAllGlobals(std::vector<ClassClass *> FlattenedList);
	void BuildMainFunc(std::vector<ClassClass *> FlattenedList);
private:
	llvm::Function* CreateExternalPrintfFunction();
	llvm::Function* CreateExternalMallocFunction();
	void MakeTypeMap(std::vector<ClassClass *> FlattenedList);
	llvm::Module *module;
	CompleteProgram *complete_program;
    llvm::Function *mainFunction;

};

llvm::StructType *GetLLVMClassStructType(std::string type_name);
llvm::PointerType *GetLLVMClassPointerType(std::string type_name);

llvm::StructType *GetLLVMObjectStructType(std::string type_name);
llvm::PointerType *GetLLVMObjectPointerType(std::string type_name);


llvm::IRBuilder<> &GetBuilder();

#endif

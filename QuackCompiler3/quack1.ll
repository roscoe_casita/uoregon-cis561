; ModuleID = 'main'

%Class_Obj = type { %Object_Obj* ()*, %Object_Str* (%Object_Obj*)*, %Object_Nothing* (%Object_Obj*)* }
%Object_Obj = type { %Class_Obj* }
%Object_Str = type { %Class_Str*, i8* }
%Class_Str = type { %Object_Str* ()*, %Object_Str* (%Object_Str*)*, %Object_Boolean* (%Object_Str*)*, %Object_Boolean* (%Object_Str*)*, %Object_Boolean* (%Object_Str*)*, %Object_Boolean* (%Object_Str*)*, %Object_Boolean* (%Object_Str*)* }
%Object_Boolean = type { %Class_Boolean*, i16 }
%Class_Boolean = type { %Object_Boolean* ()*, %Object_Boolean* (%Object_Boolean*)*, %Object_Boolean* (%Object_Boolean*)*, %Object_Boolean* (%Object_Boolean*)*, %Object_Boolean* (%Object_Boolean*)* }
%Object_Nothing = type { %Class_Nothing* }
%Class_Nothing = type { %Object_Nothing* ()* }
%Class_Int = type { %Object_Int* ()*, %Object_Int* (%Object_Int*)*, %Object_Int* (%Object_Int*)*, %Object_Int* (%Object_Int*)*, %Object_Int* (%Object_Int*)*, %Object_Boolean* (%Object_Int*)*, %Object_Boolean* (%Object_Int*)*, %Object_Boolean* (%Object_Int*)*, %Object_Boolean* (%Object_Int*)*, %Object_Boolean* (%Object_Int*)* }
%Object_Int = type { %Class_Int*, i32 }
%Class_A = type { %Object_A* (%Object_Int*)*, %Object_Int* (%Object_A*, %Object_Int*, %Object_Int*)* }
%Object_A = type { %Class_A* }
%Class_Alph = type { %Object_Alph* ()*, %Object_Obj* (%Object_Alph*)*, %Object_Int* (%Object_Alph*)* }
%Object_Alph = type { %Class_Alph*, %Object_Int*, %Object_Int*, %Object_Int*, %Object_Int* }
%Class_Duh = type { %Object_Duh* ()* }
%Object_Duh = type { %Class_Duh* }
%Class_Pt = type { %Object_Pt* (%Object_Int*, %Object_Int*)*, %Object_Nothing* (%Object_Pt*)*, %Object_Pt* (%Object_Pt*, %Object_Pt*)*, %Object_Int* (%Object_Pt*)*, %Object_Int* (%Object_Pt*)* }
%Object_Pt = type { %Class_Pt*, %Object_Int*, %Object_Int* }
%Class_Treta = type { %Object_Treta* ()*, %Object_Obj* (%Object_Treta*)*, %Object_Int* (%Object_Treta*)*, %Object_Obj* (%Object_Treta*)*, %Object_Obj* (%Object_Treta*)* }
%Object_Treta = type { %Class_Treta*, %Object_Int*, %Object_Int*, %Object_Int* }
%Class_B = type { %Object_B* (%Object_Int*, %Object_Int*)*, %Object_Int* (%Object_B*, %Object_Obj*, %Object_Int*)* }
%Object_B = type { %Class_B* }

@GLOBAL_Obj_INSTANCE = external global %Class_Obj
@GLOBAL_Nothing_INSTANCE = external global %Class_Nothing
@GLOBAL_Str_INSTANCE = external global %Class_Str
@GLOBAL_Int_INSTANCE = external global %Class_Int
@GLOBAL_Boolean_INSTANCE = external global %Class_Boolean
@GLOBAL_A_INSTANCE = external global %Class_A
@GLOBAL_Alph_INSTANCE = external global %Class_Alph
@GLOBAL_Duh_INSTANCE = external global %Class_Duh
@GLOBAL_Pt_INSTANCE = external global %Class_Pt
@GLOBAL_Treta_INSTANCE = external global %Class_Treta
@GLOBAL_B_INSTANCE = external global %Class_B

declare i32 @printf(i8*, ...)

declare i32* @malloc(i32, ...)

define internal %Object_Obj* @class_Obj_method_Obj() {
entry:
  %0 = alloca %Object_Obj*
  %1 = call i32* (i32, ...) @malloc(i32 8)
}

define internal %Object_Str* @class_Obj_method_STR(%Object_Obj* %this) {
entry:
  %0 = alloca %Object_Str*
  %1 = alloca %Object_Obj*
  store %Object_Obj* %this, %Object_Obj** %1
  %NativeObjectSTR = alloca %Object_Obj*
}

define internal %Object_Nothing* @class_Obj_method_PRINT(%Object_Obj* %this) {
entry:
  %0 = alloca %Object_Nothing*
  %1 = alloca %Object_Obj*
  store %Object_Obj* %this, %Object_Obj** %1
  %NativeObjectPRINT = alloca %Object_Obj*
}

define internal %Object_Nothing* @class_Nothing_method_Nothing() {
entry:
  %0 = alloca %Object_Nothing*
  %NativeNothingConstructor = alloca %Object_Nothing*
}

define internal %Object_Str* @class_Str_method_Str() {
entry:
  %0 = alloca %Object_Str*
  %NativeStringConstructor = alloca %Object_Str*
}

define internal %Object_Str* @class_Str_method_PLUS(%Object_Str* %this) {
entry:
  %0 = alloca %Object_Str*
  %1 = alloca %Object_Str*
  store %Object_Str* %this, %Object_Str** %1
  %NativeStringPLUS = alloca %Object_Str*
}

define internal %Object_Boolean* @class_Str_method_EQUALS(%Object_Str* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Str*
  store %Object_Str* %this, %Object_Str** %1
  %NativeStringEQUALS = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Str_method_ATMOST(%Object_Str* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Str*
  store %Object_Str* %this, %Object_Str** %1
  %NativeStringATMOST = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Str_method_ATLEAST(%Object_Str* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Str*
  store %Object_Str* %this, %Object_Str** %1
  %NativeStringATLEAST = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Str_method_LESS(%Object_Str* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Str*
  store %Object_Str* %this, %Object_Str** %1
  %NativeStringLESS = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Str_method_MORE(%Object_Str* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Str*
  store %Object_Str* %this, %Object_Str** %1
  %NativeStringMORE = alloca %Object_Boolean*
}

define internal %Object_Int* @class_Int_method_Int() {
entry:
  %0 = alloca %Object_Int*
  %NativeIntegerConstructor = alloca %Object_Int*
}

define internal %Object_Int* @class_Int_method_PLUS(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerPLUS = alloca %Object_Int*
}

define internal %Object_Int* @class_Int_method_MINUS(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerMINUS = alloca %Object_Int*
}

define internal %Object_Int* @class_Int_method_TIMES(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerTIMES = alloca %Object_Int*
}

define internal %Object_Int* @class_Int_method_DIVIDE(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerDIVIDE = alloca %Object_Int*
}

define internal %Object_Boolean* @class_Int_method_ATMOST(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerATMOST = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Int_method_ATLEAST(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerATLEAST = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Int_method_MORE(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerMORE = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Int_method_LESS(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerLESS = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Int_method_EQUALS(%Object_Int* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Int*
  store %Object_Int* %this, %Object_Int** %1
  %NativeIntegerEQUALS = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Boolean_method_Boolean() {
entry:
  %0 = alloca %Object_Boolean*
  %NativeBooleanConstructor = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Boolean_method_EQUALS(%Object_Boolean* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Boolean*
  store %Object_Boolean* %this, %Object_Boolean** %1
  %NativeBooleanEQUALS = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Boolean_method_AND(%Object_Boolean* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Boolean*
  store %Object_Boolean* %this, %Object_Boolean** %1
  %NativeBooleanAND = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Boolean_method_OR(%Object_Boolean* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Boolean*
  store %Object_Boolean* %this, %Object_Boolean** %1
  %NativeBooleanOR = alloca %Object_Boolean*
}

define internal %Object_Boolean* @class_Boolean_method_NOT(%Object_Boolean* %this) {
entry:
  %0 = alloca %Object_Boolean*
  %1 = alloca %Object_Boolean*
  store %Object_Boolean* %this, %Object_Boolean** %1
  %NativeBooleanNOT = alloca %Object_Boolean*
}

define internal %Object_A* @class_A_method_A(%Object_Int* %x) {
entry:
  %0 = alloca %Object_A*
  %x1 = alloca %Object_Int*
  store %Object_Int* %x, %Object_Int** %x1
  ret %Object_A** %0
}

define internal %Object_Int* @class_A_method_foo(%Object_A* %this, %Object_Int* %x, %Object_Int* %y) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_A*
  store %Object_A* %this, %Object_A** %1
  %x1 = alloca %Object_Int*
  store %Object_Int* %x, %Object_Int** %x1
  %y2 = alloca %Object_Int*
  store %Object_Int* %y, %Object_Int** %y2
  ret %Object_Int** %0
}

define internal %Object_Alph* @class_Alph_method_Alph() {
entry:
  %0 = alloca %Object_Alph*
  %a = alloca %Object_Int*
  %b = alloca %Object_Int*
  %c = alloca %Object_Int*
  %x = alloca %Object_Int*
  ret %Object_Alph** %0
}

define internal %Object_Obj* @class_Alph_method_test1(%Object_Alph* %this) {
entry:
  %0 = alloca %Object_Obj*
  %1 = alloca %Object_Alph*
  store %Object_Alph* %this, %Object_Alph** %1
  ret %Object_Obj** %0
}

define internal %Object_Int* @class_Alph_method_bar(%Object_Alph* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Alph*
  store %Object_Alph* %this, %Object_Alph** %1
  ret %Object_Int** %0
}

define internal %Object_Duh* @class_Duh_method_Duh() {
entry:
  %0 = alloca %Object_Duh*
  ret %Object_Duh** %0
}

define internal %Object_Pt* @class_Pt_method_Pt(%Object_Int* %xx, %Object_Int* %yy) {
entry:
  %0 = alloca %Object_Pt*
  %xx1 = alloca %Object_Int*
  store %Object_Int* %xx, %Object_Int** %xx1
  %yy2 = alloca %Object_Int*
  store %Object_Int* %yy, %Object_Int** %yy2
  %x = alloca %Object_Int*
  %y = alloca %Object_Int*
  ret %Object_Pt** %0
}

define internal %Object_Nothing* @class_Pt_method_anewtest(%Object_Pt* %this) {
entry:
  %0 = alloca %Object_Nothing*
  %1 = alloca %Object_Pt*
  store %Object_Pt* %this, %Object_Pt** %1
  ret %Object_Nothing** %0
}

define internal %Object_Pt* @class_Pt_method_PLUS(%Object_Pt* %this, %Object_Pt* %other) {
entry:
  %0 = alloca %Object_Pt*
  %1 = alloca %Object_Pt*
  store %Object_Pt* %this, %Object_Pt** %1
  %other1 = alloca %Object_Pt*
  store %Object_Pt* %other, %Object_Pt** %other1
  ret %Object_Pt** %0
}

define internal %Object_Int* @class_Pt_method__x(%Object_Pt* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Pt*
  store %Object_Pt* %this, %Object_Pt** %1
  ret %Object_Int** %0
}

define internal %Object_Int* @class_Pt_method__y(%Object_Pt* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Pt*
  store %Object_Pt* %this, %Object_Pt** %1
  ret %Object_Int** %0
}

define internal %Object_Treta* @class_Treta_method_Treta() {
entry:
  %0 = alloca %Object_Treta*
  %x = alloca %Object_Int*
  %y = alloca %Object_Int*
  %z = alloca %Object_Int*
  ret %Object_Treta** %0
}

define internal %Object_Obj* @class_Treta_method_test3(%Object_Treta* %this) {
entry:
  %0 = alloca %Object_Obj*
  %1 = alloca %Object_Treta*
  store %Object_Treta* %this, %Object_Treta** %1
  ret %Object_Obj** %0
}

define internal %Object_Int* @class_Treta_method_foo(%Object_Treta* %this) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_Treta*
  store %Object_Treta* %this, %Object_Treta** %1
  ret %Object_Int** %0
}

define internal %Object_Obj* @class_Treta_method_test1(%Object_Treta* %this) {
entry:
  %0 = alloca %Object_Obj*
  %1 = alloca %Object_Treta*
  store %Object_Treta* %this, %Object_Treta** %1
  ret %Object_Obj** %0
}

define internal %Object_Obj* @class_Treta_method_test2(%Object_Treta* %this) {
entry:
  %0 = alloca %Object_Obj*
  %1 = alloca %Object_Treta*
  store %Object_Treta* %this, %Object_Treta** %1
  ret %Object_Obj** %0
}

define internal %Object_B* @class_B_method_B(%Object_Int* %x, %Object_Int* %y) {
entry:
  %0 = alloca %Object_B*
  %x1 = alloca %Object_Int*
  store %Object_Int* %x, %Object_Int** %x1
  %y2 = alloca %Object_Int*
  store %Object_Int* %y, %Object_Int** %y2
  ret %Object_B** %0
}

define internal %Object_Int* @class_B_method_foo(%Object_B* %this, %Object_Obj* %x, %Object_Int* %y) {
entry:
  %0 = alloca %Object_Int*
  %1 = alloca %Object_B*
  store %Object_B* %this, %Object_B** %1
  %x1 = alloca %Object_Obj*
  store %Object_Obj* %x, %Object_Obj** %x1
  %y2 = alloca %Object_Int*
  store %Object_Int* %y, %Object_Int** %y2
  ret %Object_Int** %0
}

define internal %Object_Nothing* @class_MAIN_method_MAIN() {
entry:
  %0 = alloca %Object_Nothing*
  %a = alloca %Object_A*
  %a_point = alloca %Object_Pt*
  %alpha = alloca %Object_Alph*
  %b = alloca %Object_B*
  %treta = alloca %Object_Treta*
  ret %Object_Nothing** %0
}

define void @main() {
entry:
  %0 = call %Object_Nothing* @class_MAIN_method_MAIN()
  ret void
}

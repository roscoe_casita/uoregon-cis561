/* 
 This is the quack language lexical definition file for flex.



*/

%{
int line_count = 0;

void PRINT(char *x)
{
	fprintf(stdout,"%d\t%s\t\"%s\"\r\n",yylineno,x,yytext);
}
void ERROR(char *x)
{
	fprintf(stderr,"%d\t%s\t\"%s\"\r\n",yylineno,x,yytext);
}
/*

start_sequence (exception cases | normal case) end_sequence
start_sequence generalized -> yyerror (non-quoted)
start_sequence 
*/
%}



alpha		[A-Za-z]
digit		[0-9]
underscore 	[_]
ws		[ \t]

nottqts		([^"]|\"([^"]|\"[^"]))
quotes  	\"\"\"

ml_start 	\/\*
nml_char  	([^\*]|\*[^\/])
ml_end   	\*\/

slq_begin	\"
slq_char	(\\(0|b|t|n|r|f|\"|\\)|[^\\"\r\n])
slq_char_bad	(\\.|[^\\"\r\n])
slq_end		\"


%option yylineno noyywrap
%%
"class"		{ 	PRINT("CLASS"); }
"def"		{	PRINT("DEFINE"); }
"extends"	{	PRINT("EXTENDS"); }
"if"		{	PRINT("IF"); }
"elif"		{	PRINT("ELSEIF"); }
"else"		{	PRINT("ELSE"); }
"while"		{	PRINT("WHILE"); }
"return"	{	PRINT("RETURN"); }
":"		{	PRINT("COLON"); }
"="		{	PRINT("ASSIGNMENT"); }
";"		{	PRINT("SEMICOLON"); }
"."		{	PRINT("DOT"); }
","		{	PRINT("COMMA"); }
"+"		{	PRINT("PLUS"); }
"-"		{	PRINT("MINUS"); }
"*"		{	PRINT("MULTIPLY"); }
"/"		{	PRINT("DIVIDE"); }
"("		{	PRINT("LPAREN"); }
")"		{	PRINT("RPAREN"); }
"{"		{	PRINT("LBRACE"); }
"}"		{	PRINT("RBRACE"); }
"["		{	PRINT("LBRACKET"); }
"]"		{	PRINT("RBRACKET"); }
"String"	{	PRINT("STRING_TYPE"); }
"Integer"	{	PRINT("INTEGER_TYPE"); }
"Obj"		{	PRINT("OBJECT_TYPE"); }
"Boolean"	{	PRINT("BOOLEAN_TYPE"); }
"true"		{	PRINT("BOOLEAN_TRUE"); }
"false"		{	PRINT("BOOLEAN_FALSE"); }
"Nothing"	{	PRINT("OBJECT_NOTHING"); }
"none"		{	PRINT("OBJECT_NONE"); }
"and"		{	PRINT("OPERATOR_AND"); }
"or"		{	PRINT("OPERATOR_OR"); }
"not"		{	PRINT("OPERATOR_NOT"); }
"=="		{	PRINT("OPERATOR_EQUAL"); }
"<="		{	PRINT("OPERATOR_LESS_EQUAL"); }
"<"		{	PRINT("OPERATOR_LESS"); }
">="		{	PRINT("OPERATOR_GREATER_EQUAL"); }
">"		{	PRINT("OPERATOR_GREATER"); }


[A-Za-z_][A-Za-z0-9_]*		{	PRINT("IDENTIFIER"); }
[0-9]+				{	PRINT("DIGIT_VALUE"); }


{quotes}{nottqts}*{quotes}	{ 	PRINT("STRING_LITERAL");/* https://stackoverflow.com/questions/25960801/f-lex-how-do-i-match-negation/25964178#25964178 */}


{slq_begin}{slq_char}*{slq_end}	{	PRINT("STRING_LITERAL"); }
{slq_begin}{slq_char_bad}*({slq_end}|(\r|\n|\\)+)	{	PRINT("BAD_STRING_LITERAL"); ERROR("BAD_STRING_LITERAL");}


{ml_start}{nml_char}*{ml_end}	{	/*PRINT("LINE_COMMENT");*/ /*https://stackoverflow.com/questions/25960801/f-lex-how-do-i-match-negation/25964178#25964178 */ }

\/\/[^\n]*			{	/*PRINT("LINE_COMMENT");*/}
{ws}+				{	}
\n				{	}
<<EOF>>				{yyterminate();}

%%

int main( int argc, char *argv[] )
{
    ++argv, --argc;  /* skip over program name */
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
            yyin = stdin;

    yylex();
}

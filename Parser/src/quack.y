%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "quack.tab.h"
extern int yylex();
extern int yyparse();
void yyerror(const char *s) { std::cout << s << std::endl; }

int yylex(void);
%}

%token CLASS DEFINE EXTENDS 
%token IF ELSEIF ELSE WHILE RETURN COLON ASSIGNMENT SEMICOLON DOT COMMA PLUS MINUS MULTIPLY DIVIDE LPAREN RPAREN LBRACE RBRACE 
%token OPERATOR_AND OPERATOR_OR OPERATOR_NOT OPERATOR_EQUAL OPERATOR_LESS_EQUAL OPERATOR_LESS OPERATOR_GREATER_EQUAL OPERATOR_GREATER 

%token IDENTIFIER 
%token DIGIT_VALUE STRING_LITERAL BAD_STRING_LITERAL 

%left MINUS PLUS
%left MULTIPLY DIVIDE
%left OPERATOR_LESS_EQUAL OPERATOR_LESS
%left OPERATOR_GREATER_EQUAL OPERATOR_GREATER
%left OPERATOR_NOT OPERATOR_AND OPERATOR_OR
%left OPERATOR_EQUAL DOT

%%

start: class_declarations statement_list				{ std::cout << "Program Accepted." << std::endl;}	
	;

class_declarations:	
	|	class_declaration class_declarations			{}
	;

statement_list:
	|	statement_list statement				{}
	;

class_declaration: class_signature class_body				{}
	;

class_signature:	CLASS IDENTIFIER formal_args extends		{}
	;

extends:	
	|	EXTENDS IDENTIFIER					{}
	;

formal_args: 	LPAREN RPAREN						{}
	|	LPAREN formal_arg_list RPAREN				{}
	;

formal_arg_list:	formal_arg
	|		formal_arg_list COMMA formal_arg
	;

formal_arg:	IDENTIFIER COLON IDENTIFIER
	;

class_body:	LBRACE statement_list method_list RBRACE
	;

method_list:	
	|	method_list method
	;

method:		DEFINE IDENTIFIER formal_args typedec statement_block
	;

typedec:	
	|	COLON IDENTIFIER
	;

statement_block:	LBRACE  statement_list RBRACE
	;

statement:	IF r_exp statement_block else_list
	|	WHILE r_exp statement_block
	|	RETURN return_stmt SEMICOLON
	|	l_exp ASSIGNMENT index_id r_exp SEMICOLON
	|	r_exp SEMICOLON
	;

return_stmt:	
	|	r_exp
	;

index_id:	
	|	SEMICOLON IDENTIFIER
	;

else_list:
	|	elseiflist ELSE statement_block
	;

elseiflist:		
	|	ELSEIF r_exp statement_block elseiflist
	;

l_exp:		IDENTIFIER
	|	r_exp DOT IDENTIFIER actual_args
	|	r_exp DOT IDENTIFIER
	;

r_exp:		literal
	|	l_exp 
	|	IDENTIFIER actual_args
	|	r_exp PLUS r_exp
	|	r_exp MINUS r_exp
	|	r_exp MULTIPLY r_exp
	|	r_exp DIVIDE r_exp
	|	LPAREN r_exp RPAREN
	|	r_exp OPERATOR_EQUAL r_exp
	|	r_exp OPERATOR_LESS_EQUAL r_exp
	|	r_exp OPERATOR_LESS r_exp
	|	r_exp OPERATOR_GREATER_EQUAL r_exp
	|	r_exp OPERATOR_GREATER r_exp
	|	r_exp OPERATOR_AND r_exp
	|	r_exp OPERATOR_OR r_exp
	|	r_exp OPERATOR_NOT r_exp
	;

actual_args:	LPAREN RPAREN	
	|	LPAREN actual_arg_list RPAREN
	;

actual_arg_list:r_exp 
	|	actual_arg_list COMMA r_exp
	;


literal:	int_lit
	|	str_lit
	;

int_lit:	DIGIT_VALUE
		;

str_lit:	STRING_LITERAL
	|	BAD_STRING_LITERAL 
	;
%%





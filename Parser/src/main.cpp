/**
 * Driver for parser with scanner.  
 * Usage: ./parser foo.qk
 * 
 * Output is mainly error messages. 
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "quack.tab.h"

extern int yyparse (); 
extern int yydebug;

extern void yyrestart(FILE *f);

extern int yy_flex_debug; 

int main(int argc, char **argv) {
  FILE *f;
  int index; 
  yydebug = 0;  // Set to 1 to trace parser 
  yy_flex_debug = 0; // Set to 1 to see each rule matched in scanner

  char c; 
  while ((c = getopt(argc, argv, "t")) != -1) {
    if (c == 't') {
      fprintf(stderr, "Debugging mode"); 
      yydebug = 1;
    }
  }

  for (index = optind; index < argc; ++index) {
    if( !(f = fopen(argv[index], "r"))) {
	perror(argv[index]);
	exit(1);
      }
      yyrestart(f);
      printf("Beginning parse of %s\n", argv[index]);
      int condition = yyparse(); 
      
      printf("Finished parse with result %d\n", condition);
      if (condition == 0) {
	printf("\n"); 
      }
  }


}

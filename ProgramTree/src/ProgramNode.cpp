#include "ProgramNode.hpp"
#include <iostream>
#include <cstdlib>

int error_count = 0;
int var_tab_count =0;

bool LOG_FUNC = false;

void VAR_ADD()
{
	var_tab_count ++;
}

void VAR_SUB()
{
	var_tab_count--;
}

std::string HEX_STR(void* ptr)
{
	std::stringstream ss;
	ss << " " << std::hex << " " << ptr;
	return ss.str();	
}

std::string INT_STR(int val)
{
	std::stringstream ss;
	ss << " " << std::dec << " " << val;
	return ss.str();	
}

std::string INT_STR(size_t val)
{
	std::stringstream ss;
	ss << std::dec << val;
	return ss.str();	
}

void SET_LOG_FUNC(bool v)
{
	LOG_FUNC = v;
}
void VAR_LOG(std::string log)
{
	std::cout << "VAR_LOG:\t" ;
	
	for(int i =0; i < var_tab_count; i++)
	{
		std::cout << "\t";
	}
	std::cout << log << std::endl;
}

void FUNC_LOG(std::string log)
{
	if(LOG_FUNC)
	{
		std::cout << "FUNC_LOG:\t" << log << std::endl;
	}
}

void ERR_LOG(std::string log)
{
	std::cerr << "Error Count: " << error_count++ << " = " << log << std::endl;
}

ProgramNode::ProgramNode()
{
	
}


ProgramNode::~ProgramNode()
{
	for(std::vector<ProgramNode *>::iterator itr = Children.begin(); itr != Children.end(); itr++)
	{	
		delete (*itr);
	}
}

std::vector<GraphVizNodeDecorator *> ProgramNode::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;
	for(std::vector<ProgramNode *>::iterator itr = Children.begin(); itr != Children.end(); itr++)
	{	
		returnValue.push_back(*itr);
	}
	return returnValue;
}


SemanticNode::SemanticNode(std::string semantic_name):SemanticName(semantic_name)
{
	NodeLabel = semantic_name;
	NodeShape = "rectangle";
}

SemanticNode::~SemanticNode()
{
}

ProgramNodeType SemanticNode::NodeType()
{
	return SemanticNodeType;
}

SemanticNode * MakeSemanticNode(std::string semantic_name)
{
	FUNC_LOG("MakeSemanticNode 0 : " + semantic_name+ INT_STR(yylineno));
	return new SemanticNode(semantic_name);
}

SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0)
{
	FUNC_LOG("MakeSemanticNode 1 : " + semantic_name + INT_STR(yylineno));
	SemanticNode *returnValue = new SemanticNode(semantic_name);
	returnValue->Children.push_back(child0);
	return returnValue;
}

SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1)
{
	FUNC_LOG("MakeSemanticNode 2 : " + semantic_name+ INT_STR(yylineno));
	SemanticNode *returnValue = new SemanticNode(semantic_name);
	returnValue->Children.push_back(child0);
	returnValue->Children.push_back(child1);
	return returnValue;
}

SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2)
{
	FUNC_LOG("MakeSemanticNode 3 : " + semantic_name+ INT_STR(yylineno));
	SemanticNode *returnValue = new SemanticNode(semantic_name);
	returnValue->Children.push_back(child0);
	returnValue->Children.push_back(child1);
	returnValue->Children.push_back(child2);
	return returnValue;
}

SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2,ProgramNode *child3)
{
	FUNC_LOG("MakeSemanticNode 4 : " + semantic_name+ INT_STR(yylineno));
	SemanticNode *returnValue = new SemanticNode(semantic_name);
	returnValue->Children.push_back(child0);
	returnValue->Children.push_back(child1);
	returnValue->Children.push_back(child2);
	returnValue->Children.push_back(child3);
	return returnValue;
}

SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2,ProgramNode *child3,ProgramNode *child4)
{
	FUNC_LOG("MakeSemanticNode 5 : " + semantic_name+ INT_STR(yylineno));
	SemanticNode *returnValue = new SemanticNode(semantic_name);
	returnValue->Children.push_back(child0);
	returnValue->Children.push_back(child1);
	returnValue->Children.push_back(child2);
	returnValue->Children.push_back(child3);
	returnValue->Children.push_back(child4);
	return returnValue;
}

SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2,ProgramNode *child3,ProgramNode *child4,ProgramNode *child5)
{
	FUNC_LOG("MakeSemanticNode 6 : " + semantic_name + INT_STR(yylineno));
	SemanticNode *returnValue = new SemanticNode(semantic_name);
	returnValue->Children.push_back(child0);
	returnValue->Children.push_back(child1);
	returnValue->Children.push_back(child2);
	returnValue->Children.push_back(child3);
	returnValue->Children.push_back(child4);
	returnValue->Children.push_back(child5);
	return returnValue;
}

std::string ReplaceString(std::string subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}

SyntaxNode::SyntaxNode(std::string text_match,yytokentype token_type,int line_num):LineNumber(line_num),
											TextMatch(text_match),
											TokenType(token_type)
{
	NodeLabel = ReplaceString(text_match, "\"","\\\"");
}

SyntaxNode::~SyntaxNode()
{
}

ProgramNodeType SyntaxNode::NodeType()
{
	return SyntaxNodeType;
}






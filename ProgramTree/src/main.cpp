/**
 * Driver for parser with scanner.  
 * Usage: ./parser foo.qk
 * 
 * Output is mainly error messages. 
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include "quack.tab.h"
#include "ProgramNode.hpp"
#include "QuackDefines.hpp"
#include "CompilerDB.hpp"

extern int yyparse (); 
extern int yydebug;

extern void yyrestart(FILE *f);

extern int yy_flex_debug; 

bool ClassCheckHelper(std::string Subclass, std::map<std::string,std::string> Heirarchy)
{
	if(Subclass == "Obj")
	{
		return true;
	}
	if ( Heirarchy.find(Heirarchy[Subclass]) == Heirarchy.end() ) {
	  	return false;
	}
	return ClassCheckHelper(Heirarchy[Subclass], Heirarchy);
}

bool ClassCheck(CompleteProgram *program)
{
	std::map<std::string,std::string> ClassHeirarchy;
	
	ClassHeirarchy["Obj"] = "Obj";
	ClassHeirarchy["Int"] = "Obj";
	ClassHeirarchy["String"] = "Obj";
	ClassHeirarchy["Nothing"] = "Obj";
	ClassHeirarchy["Boolean"] = "Obj";
	for(auto itr = program->ClassDefines.begin(); itr != program->ClassDefines.end(); itr++)
	{
		ClassHeirarchy[(*itr)->TypeName->Name] = (*itr)->Parent;
	}
	for(auto itr = ClassHeirarchy.begin(); itr != ClassHeirarchy.end(); itr++)
	{
		if(!ClassCheckHelper(itr->first, ClassHeirarchy))
		{
			return false;
		}
	}
	return true;
}

void PrintVisualization(std::string base_file_name)
{
	CompilerDatabase *database= GetDB();

	
	CompleteProgram *program = database->GetProgram();
	
	if(!ClassCheck(program))
	{
		ERR_LOG("Class Heirarchy is NOT well formed");
	}
	else
	{
		VAR_LOG("Class heirarchy is well formed!");
	}


	program -> GenerateSemanticSyntax(base_file_name);
	//program -> GenerateProgram(base_file_name);
	//program -> GenerateProgramSemanticSyntax(base_file_name);

}

int main(int argc, char **argv) 
{
	FILE *f;
	int index; 
	yydebug = 0;  // Set to 1 to trace parser 
	yy_flex_debug = 0; // Set to 1 to see each rule matched in scanner

	char c; 
	while ((c = getopt(argc, argv, "t")) != -1) 
	{
		if (c == 't') 
		{
			fprintf(stderr, "Debugging mode"); 
			yydebug = 1;
		}
	}

	for (index = optind; index < argc; ++index) 
	{
		SET_LOG_FUNC(true);
		if( !(f = fopen(argv[index], "r"))) 
		{
			perror(argv[index]);
			exit(1);
		}
		yyrestart(f);
		printf("Beginning parse of %s\n", argv[index]);
		int condition = yyparse(); 

		std::cout << "Finished parse with result " << condition << std::endl;
		if (condition == 0) 
		{
			printf("\n"); 
		}

		CompilerDatabase *database= GetDB();

		SET_LOG_FUNC(false);

		
		std::cout<< "Before Reducing DB." << std::endl;
		database->DoReduce();
		std::cout<< "After Reducing DB." << std::endl;
		
		std::cout<< "Before Printing DB." << std::endl;

		database->PrintDatabase();

		std::cout<< "After Printing DB." << std::endl;

		
		/*
		ProgramNode *root = database->Root;

		if(!database->ValidateClassHierarchy())
		{
			condition = -1;
		}
		*/

		printf("Finished parse with result %d\n", condition);
		if (condition == 0) 
		{
			printf("\n"); 
		}
		PrintVisualization(argv[index]);
		
	}
	
	

}

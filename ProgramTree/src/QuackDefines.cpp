#include "QuackDefines.hpp"

#include <iostream>
#include <cstdlib>


NamedType::NamedType(SyntaxNode *ptr, std::string name,std::string type):Name(name),Type(type) 
{
	NodeLabel = Name + " : " + Type;
	NodeShape = "circle";
	NearestSyntax = ptr;
}

NamedType::~NamedType()
{
}

	
/*
bool  NamedType::operator=(NamedType &other) 
{
	 return ((other.Name.compare(Name) ==0 ) && (other.Type.compare(Type)==0));
}
*/


void NamedType::Print()
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("NamedType: " + Name + " : " + Type );
	VAR_SUB();
}
std::vector<GraphVizNodeDecorator *>  NamedType::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;
	return returnValue;
}


VariableIdentifier::VariableIdentifier(SyntaxNode *ptr, std::string name,std::string type) : LExpression(ptr)
{
	Name = name; 
	Type = type;
}
VariableIdentifier::~VariableIdentifier() 
{
}
void VariableIdentifier::Print()
{
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("VarID : " + Name + " Type: " + Type);
}
std::vector<GraphVizNodeDecorator *> VariableIdentifier::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeLabel = "VarID : " + Name + " Type: " + Type;
	NodeShape = "box";
	
	return returnValue;
}


VariableLookup::VariableLookup(SyntaxNode *ptr, Expression *lookup, Expression *variable) : LExpression(ptr)
{
	Lookup = dynamic_cast<RExpression *>(lookup);
	Variable= dynamic_cast<VariableIdentifier*>(variable);
}

VariableLookup::~VariableLookup() 
{
}

void VariableLookup::Print()
{
	FUNC_LOG(+ __FUNCTION__);
	VAR_LOG("Lookup: " + HEX_STR(Lookup));
	if(Lookup!=NULL)
	{
		Lookup->Print();
	}
	VAR_LOG("Var: " + HEX_STR(Variable));
	if(Variable != NULL)
	{
		Variable->Print();
	}
}

std::vector<GraphVizNodeDecorator *> VariableLookup::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeLabel = "Variable Lookup";
	NodeShape = "box";
	
	if(Lookup!=NULL)
	{
		returnValue.push_back(Lookup);
	}
	if(Variable != NULL)
	{
		returnValue.push_back(Variable);
	}
	return returnValue;
}






TypedValue::TypedValue(SyntaxNode *ptr, std::string type, std::string value) : RExpression(ptr)
{
	Type =type; 
	Value = value;
}

TypedValue::~TypedValue()
{
}

void TypedValue::Print()
{
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("Type: " + Type + " Value: " + Value);
}

std::vector<GraphVizNodeDecorator *> TypedValue::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "box";
	NodeLabel = Type + " : " + Value;
	return returnValue;
}





FunctionCall::FunctionCall(SyntaxNode *ptr, Expression *call, std::vector<Expression*> args) : RExpression(ptr)
{
	Call = dynamic_cast<LExpression*>(call);
	Arguments = args;
}

FunctionCall::~FunctionCall() 
{

}
void FunctionCall::Print()
{
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("Call :" + HEX_STR(Call));
	if(Call != NULL)
	{
		Call->Print();
	}

	for( auto itr = Arguments.begin(); itr != Arguments.end(); itr++)
	{
		VAR_LOG("Argument: " + HEX_STR(*itr));
		if((*itr) != NULL)
		{
			(*itr)->Print();
		}
	}
}

std::vector<GraphVizNodeDecorator *> FunctionCall::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "box";
	NodeLabel = "Function Call";
	if(Call != NULL)
	{
		returnValue.push_back(Call);
	}

	for( auto itr = Arguments.begin(); itr != Arguments.end(); itr++)
	{
		if((*itr) != NULL)
		{
			returnValue.push_back(*itr);
		}
	}
	return returnValue;
}



Expression::Expression(SyntaxNode *ptr)
{
	NearestSyntax = ptr;
}

RExpression::RExpression(SyntaxNode *ptr) : Expression(ptr)
{

}

LExpression::LExpression(SyntaxNode *ptr) : RExpression(ptr)
{

}


MathExpression::MathExpression(SyntaxNode *ptr, Expression *left, std::string operation, Expression *right) : RExpression(ptr)
{
	LeftSide = left; 
	RightSide =right; 
	Operation = operation;
}

MathExpression::~MathExpression()
{
}

void MathExpression::Print()
{
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("Left Side: " + HEX_STR(LeftSide));
	if(LeftSide !=NULL)
	{
		LeftSide->Print();
	}
	VAR_LOG("Right Side: " + HEX_STR(RightSide));
	if(RightSide != NULL)
	{		
		RightSide->Print();
	}
}

std::vector<GraphVizNodeDecorator *> MathExpression::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "pentagon";
	NodeLabel = Operation;
	if(LeftSide != NULL)
	{
		returnValue.push_back(LeftSide);
	}
	if(RightSide != NULL)
	{		
		returnValue.push_back(RightSide);
	}
	return returnValue;
}






BoolExpression::BoolExpression(SyntaxNode *ptr, Expression *left, std::string operation,Expression *right) : RExpression(ptr)
{
	LeftSide = left; 
	RightSide =right;
	Operator = operation;
}

void BoolExpression::Print()
{
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("Left Side: " + HEX_STR(LeftSide));
	if(LeftSide != NULL)
	{		
		LeftSide->Print();
	}
	VAR_LOG("Right Side: " + HEX_STR(RightSide));
	if(RightSide != NULL)
	{		
		RightSide->Print();
	}
}

std::vector<GraphVizNodeDecorator *> BoolExpression::GenerateGraphvizChildren()
{

	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "doubleoctagon";
	NodeLabel = Operator;
	if(LeftSide != NULL)
	{
		returnValue.push_back(LeftSide);
	}
	if(RightSide != NULL)
	{		
		returnValue.push_back(RightSide);
	}
	return returnValue;
}





ControlStatement::ControlStatement(SyntaxNode* ptr)
{
	NearestSyntax = ptr;
}

void ControlStatement::DoReduce(ControlStatement *&ptr)
{
	FUNC_LOG(__FUNCTION__);
	if(ptr != NULL)
	{		
		ControlStatement *itr = ptr->Reduce();
		while(itr != ptr)
		{
			delete ptr;
			ptr = itr;
			itr = ptr->Reduce();
		}
	}
}






SequentialStatement::SequentialStatement(SyntaxNode* ptr) : ControlStatement(ptr)
{
}

SequentialStatement::~SequentialStatement()
{
}



ControlStatementType SequentialStatement::GetStatementType() 
{
	return SequentialStatementType;
}


ControlStatement *SequentialStatement::Reduce() 
{
	FUNC_LOG(__FUNCTION__);
	ControlStatement *returnValue = this;
	if(Sequence.size() == 1) 
	{
		returnValue = Sequence[0];
		Sequence.clear();			
	}
	else
	{
		for(int i =0; i < Sequence.size(); i++)
		{
			ControlStatement *ptr = Sequence[i];
			DoReduce(ptr);
			Sequence[i] = ptr;	
		}
	}
	return returnValue;
}

std::vector<GraphVizNodeDecorator *> SequentialStatement::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "doublecircle";
	NodeLabel ="Sequential Statement";
	for(auto itr = Sequence.begin(); itr != Sequence.end(); itr++)
	{
		if((*itr)!=NULL)
		{
			returnValue.push_back(*itr);  //Segfault here
		}
	}

	return returnValue;
}


void SequentialStatement::Print() 
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("SequentialStatement Length: " + INT_STR(Sequence.size()));
	for(auto itr = Sequence.begin(); itr != Sequence.end(); itr++)
	{
		VAR_LOG("Sequential List Iterator: " + HEX_STR(*itr));
		if((*itr)!=NULL)
		{
			(*itr)->Print();  //Segfault here
		}
	}
	VAR_SUB();
}








IfElseListStatement::IfElseListStatement(SyntaxNode* ptr) : ControlStatement(ptr)
{
}

IfElseListStatement::~IfElseListStatement()
{
}

ControlStatementType IfElseListStatement::GetStatementType()
{
	return IfElseListStatementType;
}

void IfElseListStatement::Add(RExpression *cond, ControlStatement *stmt)
{
	ConditionalList.push_back( std::pair<RExpression *,ControlStatement *>(cond,stmt));
}

ControlStatement *IfElseListStatement::Reduce() 
{
	FUNC_LOG(__FUNCTION__);
	for(auto itr = ConditionalList.begin(); itr != ConditionalList.end(); itr++)
	{
		DoReduce(itr->second);
	}
	return this;
}
std::vector<GraphVizNodeDecorator *> IfElseListStatement::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "oval";
	NodeLabel ="IfElseList Statement";

	
	for(auto itr = ConditionalList.begin(); itr != ConditionalList.end(); itr++)
	{
		if( itr->first!=NULL)
		{
			returnValue.push_back(itr->first);
		}
		if( itr->second!=NULL)
		{
			returnValue.push_back(itr->second);
		}
	}


	return returnValue;
}


void IfElseListStatement::Print() 
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("IfElseListStatement Length: " + ConditionalList.size());
	for(auto itr = ConditionalList.begin(); itr != ConditionalList.end(); itr++)
	{
		VAR_LOG("If Condition RExp: " + HEX_STR(itr->first));
		if( itr->first!=0)
		{
			itr->first->Print();
		}
		VAR_LOG("If ControlStatements: " + HEX_STR(itr->second));
		if( itr->second!=NULL)
		{
			itr->second->Print() ;
		}
	}
	VAR_SUB();
}






WhileStatement::WhileStatement(SyntaxNode* ptr, RExpression *rexp, ControlStatement *stmt) : ControlStatement(ptr)
{
	Condition =rexp;
	Statements = stmt; 
}
WhileStatement::~WhileStatement()
{
}

ControlStatementType WhileStatement::GetStatementType()
{
	return WhileStatementType;
}

ControlStatement *WhileStatement::Reduce() 
{
	FUNC_LOG(__FUNCTION__);
	DoReduce(Statements);
	return this;
}
std::vector<GraphVizNodeDecorator *> WhileStatement::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "diamond";
	NodeLabel ="While Statement";
	if(Condition != NULL)
	{
		returnValue.push_back(Condition);
	}
	if(Statements != NULL)
	{
		returnValue.push_back(Statements);
	}
	return returnValue;
}

void WhileStatement::Print() 
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("WhileStatement Condition: " + HEX_STR(Condition));
	if(Condition!=NULL)
	{
		Condition->Print();
	}
	VAR_LOG("WhileStatement Statements: " + HEX_STR(Statements));
	if(Statements!=NULL)
	{
		Statements->Print();
	}
	VAR_SUB();
}




ReturnStatement::ReturnStatement(SyntaxNode* ptr, RExpression *rexp) : ControlStatement(ptr)
{
	ReturnValue = rexp;
}

ReturnStatement::~ReturnStatement() 
{
}

ControlStatementType ReturnStatement::GetStatementType()
{
	return ReturnStatementType;
}

std::vector<GraphVizNodeDecorator *> ReturnStatement::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;
	NodeShape = "hexagon";
	NodeLabel ="Return Statement";
	if(ReturnValue != NULL)
	{
		returnValue.push_back(ReturnValue);
	}
	return returnValue;
}

void ReturnStatement::Print()
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("ReturnStatement : " + HEX_STR(ReturnValue));
	if(ReturnValue!=NULL)
	{
		ReturnValue->Print();
	}
	VAR_SUB();
}

AssignmentStatement:: AssignmentStatement(SyntaxNode* ptr, LExpression *lexp, RExpression *rexp) : ControlStatement(ptr)
{
	Address = lexp; 
	Statement = rexp;
}

AssignmentStatement::~AssignmentStatement()
{
}

std::vector<GraphVizNodeDecorator *> AssignmentStatement::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	NodeShape = "octagon";
	NodeLabel ="Assignment Statement";

	std::vector<GraphVizNodeDecorator *> returnValue;
	if(Address!=NULL)
	{
		returnValue.push_back(Address);
	}
	if(Statement!=NULL)
	{
		returnValue.push_back(Statement);
	}
	return returnValue;
}

ControlStatementType AssignmentStatement::GetStatementType()
{
	return AssignmentStatementType;
}

void AssignmentStatement::Print() 
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("AssignmentStatement: LExp " + HEX_STR(Address));
	if(Address!=NULL)
	{
		Address->Print();
	}
	VAR_LOG("AssignmentStatement: RExp " + HEX_STR(Statement));
	if(Statement!=NULL)
	{
		Statement->Print();
	}
	VAR_SUB();
}

SingleStatement::SingleStatement(SyntaxNode* ptr, RExpression *stmt) : ControlStatement(ptr)
{
	Statement = stmt;
}

SingleStatement::~SingleStatement() 
{
}

std::vector<GraphVizNodeDecorator *> SingleStatement::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "invtriangle";
	NodeLabel ="Single Statement";
	returnValue.push_back(Statement);

	return returnValue;
}

ControlStatementType SingleStatement::GetStatementType()
{
	return SingleStatementType;
}


void SingleStatement::Print() 
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("SingleStatement : RExp " + HEX_STR(Statement));
	if(Statement!=NULL)
	{
		Statement->Print();
	}
	VAR_SUB();
}


MethodClass::MethodClass(SyntaxNode* ptr)
{
	NearestSyntax = ptr;
	NameReturn =0;
}

MethodClass::~MethodClass()
{
}

std::vector<GraphVizNodeDecorator *> MethodClass::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "record";

	std::stringstream ss;
	
	ss << " {  <F0> "  << NameReturn->Name << " " << NameReturn->Type ;

	for(auto arg = Arguments.begin(); arg != Arguments.end(); arg++)
	{
		if(*arg!=NULL)
		{
			ss << "|" << (*arg)->Name + " : " + (*arg)->Type + "," ;
			returnValue.push_back(*arg);
		}
	}

	if(Statements!=NULL)
	{
		returnValue.push_back(Statements);
	}
	ss << "} ";

	NodeLabel = ss.str();

	
	return returnValue;
}

void 	MethodClass::Reduce() 
{
	FUNC_LOG(__FUNCTION__);
	ControlStatement::DoReduce(Statements);
}

bool MethodClass::typeCheck()
{
	return true;  //Dummy return statement
}

void MethodClass::Print()
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);

	VAR_LOG("MethodClass : " + NameReturn->Name + " : " + NameReturn->Type);

	for(auto param = Arguments.begin(); param != Arguments.end(); param++)
	{
		VAR_LOG("MethodArgument: " + HEX_STR(*param));
		if((*param) != NULL)
		{
			(*param)->Print();
		}
	}
	
	VAR_LOG("MethodStatements: " + HEX_STR(Statements));
	if(Statements!=NULL)
	{
		Statements->Print();
	}
	VAR_SUB();
}



ClassClass::ClassClass(SyntaxNode* ptr)
{
	TypeName =new NamedType(ptr,ptr->TextMatch,ptr->TextMatch); 
	NearestSyntax=ptr;
	Parent = "Obj";
}
ClassClass::~ClassClass() 
{
}

void 	ClassClass::SetParent(std::string parent)
{
	Parent = parent;
}
void	ClassClass::SetArgs(std::vector<NamedType*> args)
{
	Arguments = args;
}

std::vector<GraphVizNodeDecorator *> ClassClass::GenerateGraphvizChildren()
{
	FUNC_LOG(__FUNCTION__);
	std::vector<GraphVizNodeDecorator *> returnValue;

	NodeShape = "record";

	std::stringstream ss;
	
	ss << " { class "  << TypeName->Name << " : "<< Parent << " "  ;
	for(auto arg = Arguments.begin(); arg != Arguments.end(); arg++)
	{
		if(*arg!=NULL)
		{
			ss << (*arg)->Name + " : " + (*arg)->Type + " " ;
			//returnValue.push_back(*arg);
		}
	}
	int i =0;
	for(auto method = Methods.begin(); method != Methods.end(); method++)
	{
		if((*method) != NULL)
		{
			ss  << " | " << (*method)->NameReturn->Name << " ";
			returnValue.push_back(*method);
		}
	}
	if(Statements!=NULL)
	{
		returnValue.push_back(Statements);
	}

	ss << " } " ;
	NodeLabel = ss.str();

	return returnValue;
}

void	ClassClass::Reduce() 
{
	FUNC_LOG(__FUNCTION__);
	for(auto name_func = Methods.begin(); name_func != Methods.end(); name_func ++)
	{
		if((*name_func)!=NULL)
		{
			(*name_func)->Reduce();
		}
	}
	ControlStatement::DoReduce(Statements);
}

bool ClassClass::typeCheck()
{
	if(TypeName->Name != TypeName->Type)
	{
		ERR_LOG("Error type checking class " + TypeName->Name);
	}

	for(auto itr = Methods.begin(); itr != Methods.end(); itr ++)
	{
		if(!(*itr)->typeCheck())
		{
			return false;
		}
	}

	/*
	Type check the constructor
	*/

	
	

}


std::string SymbolTable::Lookup(std::string name)
{
	if ( SymTable.find(name) == SymTable.end() ) 
	{
	  	ERR_LOG("Symbol not found!");
	}
	return SymTable[name];
}

void SymbolTable::Add(std::string name, std::string type)
{
	if ( SymTable.find(name) != SymTable.end() ) 
	{
	  	if ( SymTable[name] != type )
		{
			ERR_LOG("Identifier: " + name + " of type: " + SymTable[name] + " is not compatible with type: " + type);
		}
		return;
	}
	SymTable[name] = type;
	return;
}

void SymbolTable::ClearTable()
{
	SymTable.clear();
	return;
}



void ClassClass::Print()
{
	VAR_ADD();
	FUNC_LOG(__FUNCTION__);
	VAR_LOG("Class Type: " + TypeName->Type + " : Parent " + Parent);
	
	for(size_t a=0; a < Arguments.size(); a++)
	{ 
		VAR_LOG("Constructor Argument: " + Arguments[a]->Name + " : " + Arguments[a]->Type);
	}
	VAR_LOG("Constructor Statements: " + HEX_STR(Statements));
	if(Statements!= NULL)
	{
		Statements->Print();

	}
	// here print out the methods

	for(auto method = Methods.begin(); method != Methods.end(); method++)
	{ 
		VAR_LOG("MethodClass: " + HEX_STR(*method));
		if((*method)!=NULL)
		{
			(*method)->Print();
		}
	}
	VAR_SUB();
}


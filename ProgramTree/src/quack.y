%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "quack.tab.h"
#include "CompilerDB.hpp"

extern int yylex();
extern int yyparse();
extern int yylineno;
void yyerror(const char *s) { std::cout << "Error on line: " << yylineno << std::endl << s << std::endl; }

int yylex(void);
%}

%union {
	class ProgramNode *node;
	class SemanticNode *semantic;
	class SyntaxNode *syntax;
}

%token CLASS DEFINE EXTENDS 
%token IF ELSEIF ELSE WHILE RETURN COLON ASSIGNMENT SEMICOLON DOT COMMA PLUS MINUS MULTIPLY DIVIDE LPAREN RPAREN LBRACE RBRACE 
%token OPERATOR_AND OPERATOR_OR OPERATOR_NOT OPERATOR_EQUAL OPERATOR_LESS_EQUAL OPERATOR_LESS OPERATOR_GREATER_EQUAL OPERATOR_GREATER 

%token IDENTIFIER FALSE_VALUE TRUE_VALUE NOTHING_VALUE
%token DIGIT_VALUE STRING_LITERAL BAD_STRING_LITERAL 

%type<syntax> CLASS DEFINE EXTENDS FALSE_VALUE TRUE_VALUE NOTHING_VALUE
%type<syntax> IF ELSEIF ELSE WHILE RETURN COLON ASSIGNMENT SEMICOLON DOT COMMA PLUS MINUS MULTIPLY DIVIDE LPAREN RPAREN LBRACE RBRACE 
%type<syntax> OPERATOR_AND OPERATOR_OR OPERATOR_NOT OPERATOR_EQUAL OPERATOR_LESS_EQUAL OPERATOR_LESS OPERATOR_GREATER_EQUAL OPERATOR_GREATER 
%type<syntax> IDENTIFIER 
%type<syntax> DIGIT_VALUE STRING_LITERAL BAD_STRING_LITERAL 


%type<semantic> str_lit int_lit literal actual_arg_list actual_args r_exp l_exp elseiflist else_list index_id return_stmt
%type<semantic> statement statement_block method method_list class_body formal_arg formal_arg_list formal_args
%type<semantic> extends class_signature class_declaration statement_list class_declarations returntype
%type<semantic> start bool_lit nothing_lit

%left MINUS PLUS
%left MULTIPLY DIVIDE
%left OPERATOR_LESS_EQUAL OPERATOR_LESS
%left OPERATOR_GREATER_EQUAL OPERATOR_GREATER
%left OPERATOR_NOT OPERATOR_AND OPERATOR_OR
%left OPERATOR_EQUAL DOT

%%

start: class_declarations { GetDB()->FinalProgram = ClassRSM()-> FinalizeStack(); StmtRSM()->SetReg(new SequentialStatement(LastSyntax()));} statement_list 
									{
										GetDB()->Root =MakeSemanticNode("start",$1,$3);
										GetDB()->ProgramStatements = StmtRSM()->FinalizeRegister();
									}	
	;

class_declarations:							{$$ = MakeSemanticNode("class_declarations");}
	|	class_declaration { ClassRSM()->PushReg(); } class_declarations			
									{
										$$ = MakeSemanticNode("class_declarations",$1,$3);  
									}
	;

class_declaration: class_signature class_body				{$$ = MakeSemanticNode("class_declaration",$1,$2);
									}
	;

class_signature:	CLASS IDENTIFIER {ClassRSM()->SetReg(new ClassClass($2));} formal_args  {ClassRSM()->Top()->Arguments = ArgsRSM()->FinalizeStack();} extends		
									{
										FUNC_LOG("class_signature");	
										$$ = MakeSemanticNode("class_signature",$1,$2,$4,$6);
									}
	;

extends:								{$$ = MakeSemanticNode("extends");		}
	|	EXTENDS IDENTIFIER					{$$ = MakeSemanticNode("extends",$1,$2);	
									/* Set Class Inheritance here. */
										ClassRSM()->Top()->SetParent($2->TextMatch);
										
									}
	;

formal_args: 	LPAREN RPAREN						{$$ = MakeSemanticNode("formal_args",$1,$2);	/* Set the current formal_args value to new */ }
	|	LPAREN formal_arg_list RPAREN				{$$ = MakeSemanticNode("formal_args",$1,$2,$3);	}
	;

formal_arg_list:	formal_arg					{$$ = MakeSemanticNode("formal_arg_list",$1); /* Get the current arg_list and add named type to it*/ }
	|		formal_arg_list COMMA formal_arg		{$$ = MakeSemanticNode("formal_arg_list",$1,$2,$3);	/* Get the current arg_list and add named type to it*/}
	;

formal_arg:	IDENTIFIER COLON IDENTIFIER				{$$ = MakeSemanticNode("formal_arg",$1,$2,$3); 
									ArgsRSM()->Push(new NamedType($1, $1->TextMatch, $3->TextMatch));
									}
	;

class_body:	LBRACE {StmtRSM()->SetReg(new SequentialStatement($1));} statement_list { ClassRSM()->Top()->Statements = StmtRSM()->FinalizeRegister(); } method_list RBRACE		
									{
										$$ = MakeSemanticNode("class_body",$1,$3,$5, $6);	
										ClassRSM()->Top()->Methods = MethRSM()->FinalizeStack();
										/*set class stm_list and mtd_list */
									}
	;

method_list:								{$$ = MakeSemanticNode("method_list"); /*no action needed here, current list has mthods.*/}
	|	method_list {MethRSM()->SetReg(new MethodClass(LastSyntax())); } method					
									{
										$$ = MakeSemanticNode("method_list",$1,$3); /* add current method to current method list*/ 
										MethRSM()->PushReg();									
									}
	;

method:		DEFINE IDENTIFIER formal_args {MethRSM()->Top()->Arguments = ArgsRSM()->FinalizeStack();}
							returntype { StmtRSM()->SetReg(new SequentialStatement($1));} statement_block
									{
										$$ = MakeSemanticNode("method",$1,$2,$3,$5,$7);
											
										MethRSM()->Top()->Statements = StmtRSM()->FinalizeRegister();
										std::string default_return = "$Bottom";

										if($5->Children.size() == 2)
										{
											default_return = ((SyntaxNode*)($5->Children[1]))->TextMatch;
										}
										MethRSM()->Top()->NameReturn = new NamedType($2,$2->TextMatch,default_return);
									}
	;

returntype:								{$$ = MakeSemanticNode("returntype"); /*set current named type to Obj, may not need to be done.*/ }
	|	COLON IDENTIFIER					{$$ = MakeSemanticNode("returntype",$1,$2); /* set current named type to Identifier*/ }
	;

statement_block:	LBRACE 						{ 
										StmtRSM()->PushReg(); 
										StmtRSM()->SetReg(new SequentialStatement($1));} 
				statement_list RBRACE
									{
										FUNC_LOG("statement_block");
										$$ = MakeSemanticNode("statement_block",$1,$3,$4);
										SequentialStatement *ss = StmtRSM()->GetReg(); 
										StmtRSM()->PopReg();
										StmtRSM()->Top()->Sequence.push_back(ss);
									}
	;

statement_list:								{$$ = MakeSemanticNode("statement_list");}
	|	statement_list statement				{$$ = MakeSemanticNode("statement_list",$1,$2);
									}
	;

statement:	IF r_exp 						{ 
										ExpRSM()->PushReg();
										StmtRSM()->PushReg(); 
										StmtRSM()->SetReg(new SequentialStatement($1));
									 } 	
			statement_block 				{
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										ExpRSM()->PopReg();
										RExpression *rexp = (RExpression*) ExpRSM()->GetReg();
										IfElseListStatement *ptr = new IfElseListStatement($1);
										ptr ->Add(rexp,ss);
										StmtRSM()->Top()->Sequence.push_back(ptr);
  									} 
					else_list	
									{	
										FUNC_LOG("If Statement");
										$$ = MakeSemanticNode("statement",$1,$2,$4,$6);
									}
	|	WHILE r_exp 						{ 
										ExpRSM()->PushReg(); 
										StmtRSM()->PushReg(); 
										StmtRSM()->SetReg(new SequentialStatement($1));  }
				 statement_block	
									{
										FUNC_LOG("While Statement");
										$$ = MakeSemanticNode("statement",$1,$2,$4);
										ExpRSM()->PopReg();
										RExpression *rexp = (RExpression *)ExpRSM()->GetReg();
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										StmtRSM()->Top()->Sequence.push_back(new WhileStatement($1,rexp,ss));
									}

	|	RETURN return_stmt SEMICOLON				{
										FUNC_LOG("Return Statment");
										$$ = MakeSemanticNode("statement",$1,$2,$3); 
										StmtRSM()->Top()->Sequence.push_back(new ReturnStatement($1, (RExpression*)ExpRSM()->FinalizeRegister()));
									 }

	|	l_exp index_id 						{ExpRSM()->PushReg();} 
				ASSIGNMENT r_exp SEMICOLON		
									{
										FUNC_LOG("Assignment Statement");
										$$ = MakeSemanticNode("statement",$1,$2,$4,$5,$6);
										RExpression *rexp = (RExpression *)ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										LExpression *lexp = (LExpression *)ExpRSM()->GetReg();
										StmtRSM()->Top()->Sequence.push_back(new AssignmentStatement($4,lexp,rexp));
									}

	|	r_exp SEMICOLON 					{
										FUNC_LOG("Single Statement");
										$$ = MakeSemanticNode("statement",$1,$2);
										StmtRSM()->Top()->Sequence.push_back(new SingleStatement($2,(RExpression*)ExpRSM()->FinalizeRegister()));
									}
	;

return_stmt:								{	$$ = MakeSemanticNode("return_stmt"); }
	|	r_exp							{	$$ = MakeSemanticNode("return_stmt",$1);}
	;

index_id:								{	$$ = MakeSemanticNode("index_id");}
	|	COLON IDENTIFIER					{	$$ = MakeSemanticNode("index_id",$1,$2);}
	;

else_list:								{	$$ = MakeSemanticNode("else_list");}
	|	elseiflist ELSE 					{	StmtRSM()->PushReg(); StmtRSM()->SetReg(new SequentialStatement($2)); } 
				statement_block 
									{	$$ = MakeSemanticNode("else_list",$1,$2,$4);
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										IfElseListStatement* exp = (IfElseListStatement*)(StmtRSM()->Top()->Sequence.back());
										exp->Add(NULL,ss);
									}
	;

elseiflist:								{	$$ = MakeSemanticNode("elseiflist");}
	|	ELSEIF r_exp 						{ 
										ExpRSM()->PushReg(); 
										StmtRSM()->PushReg();
										StmtRSM()->SetReg(new SequentialStatement($1));
									} 
				statement_block 
									{
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										ExpRSM()->PopReg();
										RExpression *rexp = (RExpression*) ExpRSM()->GetReg();
										IfElseListStatement* exp = (IfElseListStatement*)(StmtRSM()->Top()->Sequence.back());
										exp->Add(rexp,ss);
									}
				 elseiflist				{	$$ = MakeSemanticNode("elseiflist",$1,$2,$4,$6); }
	;

l_exp:		IDENTIFIER						{	$$ = MakeSemanticNode("l_exp",$1); 
										ExpRSM()->SetReg(new VariableIdentifier($1,$1->TextMatch,"$Bottom"));}
	|	r_exp DOT IDENTIFIER					{	$$ = MakeSemanticNode("l_exp",$1,$2,$3); }
	;

r_exp:		literal							{	$$ = MakeSemanticNode("r_exp",$1); /* */}
	|	l_exp 							{	$$ = MakeSemanticNode("r_exp",$1); /* */}
	|	l_exp							{
										ExpRSM()->PushReg();
									}
			actual_args
									{		
										ExpRSM()->PopReg();
										Expression *lexp = ExpRSM()->GetReg();
										std::vector<Expression *> args;

										//ExpRSM()->SetReg(new FunctionCall(LastSyntax(),ExpRSM()->GetReg(),)); 

										$$ = MakeSemanticNode("r_exp",$1,$3); 
										FunctionCall *ptr = new FunctionCall(LastSyntax(),lexp,args);
										ExpRSM()->SetReg(ptr);
									}

	|	r_exp PLUS 						{ ExpRSM()->PushReg();}
			   r_exp	
									{
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new MathExpression($2,left_side,"+",right_side));
									}
	|	r_exp MINUS 						{ ExpRSM()->PushReg();} 
			   r_exp					{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new MathExpression($2,left_side,"-",right_side));
									}
	|	r_exp MULTIPLY 						{ ExpRSM()->PushReg();} 
			   r_exp					{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new MathExpression($2,left_side,"*",right_side));
									}
	|	r_exp DIVIDE 						{ ExpRSM()->PushReg();} 
			   r_exp					{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new MathExpression($2,left_side,"/",right_side));
									}
	|	LPAREN r_exp RPAREN					{$$ = MakeSemanticNode("r_exp",$1,$2,$3); }
	|	r_exp OPERATOR_EQUAL 					{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,"==",right_side));
									}
	|	r_exp OPERATOR_LESS_EQUAL 				{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,"<=",right_side));
									}
	|	r_exp OPERATOR_LESS 					{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,"<",right_side));
									}
	|	r_exp OPERATOR_GREATER_EQUAL 				{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,">=",right_side));
									}
	|	r_exp OPERATOR_GREATER 					{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,">",right_side));
									}
	|	r_exp OPERATOR_AND 					{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,"&&",right_side));
									}
	|	r_exp OPERATOR_OR 					{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,"||",right_side));
									}
	|	r_exp OPERATOR_NOT 					{ ExpRSM()->PushReg();} 
			   		r_exp				{				
										$$ = MakeSemanticNode("r_exp",$1,$2,$4); 
										Expression* right_side = ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										Expression* left_side = ExpRSM()->GetReg();
										ExpRSM()->SetReg(new BoolExpression($2,left_side,"!!",right_side));
									}
	;

actual_args:	LPAREN RPAREN						{$$ = MakeSemanticNode("actual_args",$1,$2);}
	|	LPAREN actual_arg_list RPAREN				{$$ = MakeSemanticNode("actual_args",$1,$2,$3);}
	;

actual_arg_list:r_exp 							{ $$ = MakeSemanticNode("actual_arg_list",$1);}
	|	actual_arg_list COMMA r_exp				{ $$ = MakeSemanticNode("actual_arg_list",$1,$2,$3);}
	;


literal:	int_lit							{$$ = MakeSemanticNode("literal",$1);  }
	|	str_lit							{$$ = MakeSemanticNode("literal",$1);  }
	|	bool_lit						{$$ = MakeSemanticNode("literal",$1);}
	|	nothing_lit						{$$ = MakeSemanticNode("literal",$1); }
	;

int_lit:	DIGIT_VALUE						{ $$ = MakeSemanticNode("int_lit",$1);ExpRSM()->SetReg(new TypedValue($1,"Integer",$1->TextMatch));}
		;

str_lit:	STRING_LITERAL						{ $$ = MakeSemanticNode("str_lit",$1);ExpRSM()->SetReg(new TypedValue($1,"String",$1->TextMatch));}
	|	BAD_STRING_LITERAL 					{ $$ = MakeSemanticNode("str_lit",$1);ExpRSM()->SetReg(new TypedValue($1,"String",$1->TextMatch));}
	;



bool_lit:	FALSE_VALUE						{ $$ = MakeSemanticNode("bool_false_lit",$1);  ExpRSM()->SetReg(new TypedValue($1,"Boolean",$1->TextMatch));}
	|	TRUE_VALUE						{ $$ = MakeSemanticNode("bool_true_lit",$1);  ExpRSM()->SetReg(new TypedValue($1,"Boolean",$1->TextMatch));}
	;


nothing_lit:	NOTHING_VALUE						{ $$ = MakeSemanticNode("nothing_lit",$1); ExpRSM()->SetReg(new TypedValue($1,"Nothing",$1->TextMatch));}
	;
%%





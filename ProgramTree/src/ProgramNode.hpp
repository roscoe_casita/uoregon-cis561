#ifndef PROGRAMNODE_HPP
#define PROGRAMNODE_HPP

#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
#include "GraphVizNodeDecorator.hpp"
#include "quack.tab.h"

extern int yylineno;

enum ProgramNodeType
{
	SyntaxNodeType,
	SemanticNodeType
};

//
//
//	This is where the syntax and semantic tree definition begins.
//
//


class ProgramNode: public GraphVizNodeDecorator
{
	public: 
		ProgramNode();
		virtual	~ProgramNode();
	
		std::vector<ProgramNode *> Children;
		virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	public:
		virtual ProgramNodeType NodeType() = 0;
};

class SyntaxNode: public ProgramNode
{
	public:
		SyntaxNode(std::string text_match,yytokentype token_type,int line_num);
		virtual ~SyntaxNode();
		ProgramNodeType NodeType();
	public:
		const int LineNumber;
		const std::string TextMatch;
		const yytokentype TokenType;
};


class SemanticNode: public ProgramNode
{
	public:
		SemanticNode(std::string semantic_name);
		virtual ~SemanticNode();
		virtual ProgramNodeType NodeType();
	public:
		const std::string SemanticName;
		
		
};

SemanticNode * MakeSemanticNode(std::string semantic_name);
SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0);
SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1);
SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2);
SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2,ProgramNode *child3);
SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2,ProgramNode *child3,ProgramNode *child4);
SemanticNode * MakeSemanticNode(std::string semantic_name,ProgramNode *child0,ProgramNode *child1,ProgramNode *child2,ProgramNode *child3,ProgramNode *child4,ProgramNode *child5);








void SET_LOG_FUNC(bool v);
void FUNC_LOG(std::string log);
void ERR_LOG(std::string log);
void VAR_LOG(std::string log);
std::string HEX_STR(void* ptr);
std::string INT_STR(int val);
std::string INT_STR(size_t val);

void VAR_ADD();
void VAR_SUB();

#endif

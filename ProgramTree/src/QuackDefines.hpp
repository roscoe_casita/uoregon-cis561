#ifndef QUACKDEFINES_HPP
#define QUACKDEFINES_HPP

#include "ProgramNode.hpp"

/*
class VariableClass
{
public:
	VariableClass() {NameType =0; CurType = ""; NearestSyntax=0;}
	~VariableClass() {delete NameType;}
public:
	NamedType 				*NameType;
	std::string				CurType;
	SyntaxNode				*NearestSyntax;
};
*/


/*
	Description: Class to associate a particular object's name with its type

	Uses:  Used in MethodClass and ClassClass
*/
class NamedType: public GraphVizNodeDecorator
{
public:	
	NamedType(SyntaxNode* ptr, std::string name,std::string type);
	~NamedType();
	void Print();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	const std::string Name;
	const std::string Type;
	SyntaxNode *NearestSyntax;
};


/*

*/
class Expression: public GraphVizNodeDecorator
{
public:
	Expression(SyntaxNode* ptr);
	~Expression() {}
	virtual void Print() =0;
	virtual	std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren()=0;

public:
	std::string TypeValue;
	SyntaxNode *NearestSyntax;
	ProgramNode *NearestNode;
};

/*

*/
class RExpression:public Expression
{
public:	
	RExpression(SyntaxNode* ptr);
	~RExpression() {}
	virtual void Print() =0;
	virtual	std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren()=0;

};


/*

*/
class LExpression:public RExpression
{
public:
	LExpression(SyntaxNode* ptr);
	~LExpression() {}
	virtual void Print() = 0;
	virtual	std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren()=0;

};

/*

*/
class VariableIdentifier:public LExpression
{
public:
	VariableIdentifier(SyntaxNode* ptr, std::string name,std::string type);
	~VariableIdentifier();
	virtual void Print();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	std::string	Name;
	std::string	Type;
};

/*

*/
class VariableLookup:public LExpression
{
public:
	VariableLookup(SyntaxNode* ptr, Expression *lookup, Expression *variable);
	~VariableLookup();
	virtual void Print();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	RExpression		*Lookup;
	VariableIdentifier	*Variable;
};

/*

*/
class TypedValue: public RExpression
{
public:
	TypedValue(SyntaxNode* ptr, std::string type, std::string value);
	~TypedValue();

	virtual void Print();
	virtual	std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	std::string Type;
	std::string Value;
};

/*

*/
class FunctionCall: public RExpression
{
public:
	FunctionCall(SyntaxNode* ptr, Expression *call, std::vector<Expression*> args);
	~FunctionCall();
	virtual void Print();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	LExpression			*Call;
	std::vector<Expression *>	Arguments;
};

/*

*/
class MathExpression: public RExpression
{
public:
	MathExpression(SyntaxNode* ptr, Expression *left, std::string operation,Expression *right);
	~MathExpression();
	virtual void Print();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();


public:
	Expression			*LeftSide;
	std::string			Operation;
	Expression			*RightSide;
};

/*

*/
class BoolExpression: public RExpression
{
public:
	BoolExpression(SyntaxNode* ptr, Expression *left, std::string operation,Expression *right);
	~BoolExpression() {}
	virtual void Print();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	Expression			*LeftSide;
	std::string			Operator;
	Expression			*RightSide;

};


/*

*/

enum ControlStatementType
{
	SequentialStatementType,
	IfElseListStatementType,
	WhileStatementType,
	ReturnStatementType,
	AssignmentStatementType,
	SingleStatementType
};

/*

*/
class ControlStatement : public GraphVizNodeDecorator
{
public: 
	ControlStatement(SyntaxNode* ptr);
	~ControlStatement() {}
	virtual ControlStatementType GetStatementType() = 0;
	virtual void Print() = 0;
	virtual ControlStatement *Reduce() { return this; }
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren() =0;

public:
	SyntaxNode *NearestSyntax;	
	static void DoReduce(ControlStatement *&ptr);

};

/*
	One statement followed by another.  List of statements
*/
class SequentialStatement: public ControlStatement
{
public: 
	SequentialStatement(SyntaxNode* ptr);
	~SequentialStatement();
	virtual ControlStatementType GetStatementType();
	ControlStatement *Reduce();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

	virtual void Print();
public:
	std::vector<ControlStatement *>	Sequence;

};

/*
	Execute control statements based on the RExpressions
*/

class IfElseListStatement: public ControlStatement
{
public: 
	IfElseListStatement(SyntaxNode* ptr);
	~IfElseListStatement();
	virtual ControlStatementType GetStatementType();

	void Add(RExpression *cond, ControlStatement *stmt);

	virtual void Print() ;
	ControlStatement *Reduce() ;
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
public:
	std::vector< std::pair<RExpression *,ControlStatement *> > ConditionalList;

};

/*
	Execute control statements repeatedly until the Condition is false
*/
class WhileStatement: public ControlStatement
{
public: 
	WhileStatement(SyntaxNode* ptr, RExpression *rexp, ControlStatement *stmt);
	~WhileStatement();
	virtual ControlStatementType GetStatementType();

	ControlStatement *Reduce() ;
	virtual void Print() ;
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

public:
	RExpression *Condition;
	ControlStatement *Statements;
};

/*
	Return statement which includes a ReturnValue to evaluate
*/
class ReturnStatement: public ControlStatement
{
public: 
	ReturnStatement(SyntaxNode* ptr, RExpression *rexp);
	~ReturnStatement();

	virtual ControlStatementType GetStatementType() ;
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	virtual void Print();

public:
	RExpression *ReturnValue;
};


/*
	Evaluate the RExpression and put it in the LExpression location
*/
class AssignmentStatement: public ControlStatement
{
public: 
	AssignmentStatement(SyntaxNode* ptr, LExpression *lexp, RExpression *rexp);
	~AssignmentStatement();

	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

	virtual ControlStatementType GetStatementType();
	virtual void Print();

public:
	LExpression *Address;
	RExpression *Statement;
};


/*
	Single RExpression evaluates to a value
*/
class SingleStatement: public ControlStatement
{
public: SingleStatement(SyntaxNode* ptr, RExpression *stmt);
	~SingleStatement();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

	virtual ControlStatementType GetStatementType();
public:	
	RExpression *Statement;
	virtual void Print();
};

class SymbolTable
{
	std::map<std::string, std::string> SymTable;
	std::string Lookup(std::string id);
	void Add(std::string name, std::string type);

};


/*

*/
class MethodClass: public GraphVizNodeDecorator
{
public:
	MethodClass(SyntaxNode* ptr);
	~MethodClass();
	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();
	void 	Reduce();
	void	Print();
	bool	typeCheck();

public:
	SyntaxNode				*Nearest;
	NamedType				*NameReturn;
	std::vector<NamedType*>			Arguments;
//	std::map<std::string,VariableClass*>	Variables;
	ControlStatement			*Statements;
	SyntaxNode				*NearestSyntax;
};


/*

*/
class ClassClass: public GraphVizNodeDecorator
{
public:
	ClassClass(SyntaxNode* ptr);
	~ClassClass();

	virtual std::vector<GraphVizNodeDecorator *> GenerateGraphvizChildren();

	void	Reduce() ;

	void	Print();
	void 	SetParent(std::string parent);
	void	SetArgs(std::vector<NamedType*> args);
	bool	typeCheck();
	

public:
	NamedType 				*TypeName;
	std::string				Parent;
	std::vector<NamedType*>			Arguments;
	std::vector<MethodClass *>		Methods;
	SyntaxNode				*NearestSyntax;
//	std::map<std::string,VariableClass*>	Members;
	ControlStatement			*Statements;

};

#endif


/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "quack.y" /* yacc.c:339  */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "quack.tab.h"
#include "CompilerDB.hpp"

extern int yylex();
extern int yyparse();
extern int yylineno;
void yyerror(const char *s) { std::cout << "Error on line: " << yylineno << std::endl << s << std::endl; }

int yylex(void);

#line 86 "quack.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "quack.tab.h".  */
#ifndef YY_YY_QUACK_TAB_H_INCLUDED
# define YY_YY_QUACK_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    CLASS = 258,
    DEFINE = 259,
    EXTENDS = 260,
    IF = 261,
    ELSEIF = 262,
    ELSE = 263,
    WHILE = 264,
    RETURN = 265,
    COLON = 266,
    ASSIGNMENT = 267,
    SEMICOLON = 268,
    DOT = 269,
    COMMA = 270,
    PLUS = 271,
    MINUS = 272,
    MULTIPLY = 273,
    DIVIDE = 274,
    LPAREN = 275,
    RPAREN = 276,
    LBRACE = 277,
    RBRACE = 278,
    OPERATOR_AND = 279,
    OPERATOR_OR = 280,
    OPERATOR_NOT = 281,
    OPERATOR_EQUAL = 282,
    OPERATOR_LESS_EQUAL = 283,
    OPERATOR_LESS = 284,
    OPERATOR_GREATER_EQUAL = 285,
    OPERATOR_GREATER = 286,
    IDENTIFIER = 287,
    FALSE_VALUE = 288,
    TRUE_VALUE = 289,
    NOTHING_VALUE = 290,
    DIGIT_VALUE = 291,
    STRING_LITERAL = 292,
    BAD_STRING_LITERAL = 293
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 21 "quack.y" /* yacc.c:355  */

	class ProgramNode *node;
	class SemanticNode *semantic;
	class SyntaxNode *syntax;

#line 171 "quack.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_QUACK_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 188 "quack.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  7
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   221

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  39
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  48
/* YYNRULES -- Number of rules.  */
#define YYNRULES  86
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  137

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   293

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    55,    55,    55,    62,    63,    63,    69,    73,    73,
      73,    80,    81,    88,    89,    92,    93,    96,   101,   101,
     101,   109,   110,   110,   117,   118,   117,   133,   134,   137,
     137,   150,   151,   155,   161,   155,   174,   174,   189,   195,
     195,   206,   213,   214,   217,   218,   221,   222,   222,   232,
     233,   235,   233,   246,   248,   251,   252,   253,   253,   261,
     261,   270,   271,   272,   273,   274,   275,   276,   277,   278,
     279,   280,   281,   284,   285,   288,   289,   293,   294,   295,
     296,   299,   302,   303,   308,   309,   313
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "CLASS", "DEFINE", "EXTENDS", "IF",
  "ELSEIF", "ELSE", "WHILE", "RETURN", "COLON", "ASSIGNMENT", "SEMICOLON",
  "DOT", "COMMA", "PLUS", "MINUS", "MULTIPLY", "DIVIDE", "LPAREN",
  "RPAREN", "LBRACE", "RBRACE", "OPERATOR_AND", "OPERATOR_OR",
  "OPERATOR_NOT", "OPERATOR_EQUAL", "OPERATOR_LESS_EQUAL", "OPERATOR_LESS",
  "OPERATOR_GREATER_EQUAL", "OPERATOR_GREATER", "IDENTIFIER",
  "FALSE_VALUE", "TRUE_VALUE", "NOTHING_VALUE", "DIGIT_VALUE",
  "STRING_LITERAL", "BAD_STRING_LITERAL", "$accept", "start", "$@1",
  "class_declarations", "$@2", "class_declaration", "class_signature",
  "$@3", "$@4", "extends", "formal_args", "formal_arg_list", "formal_arg",
  "class_body", "$@5", "$@6", "method_list", "$@7", "method", "$@8", "$@9",
  "returntype", "statement_block", "$@10", "statement_list", "statement",
  "$@11", "$@12", "$@13", "$@14", "return_stmt", "index_id", "else_list",
  "$@15", "elseiflist", "$@16", "$@17", "l_exp", "r_exp", "$@18", "$@19",
  "actual_args", "actual_arg_list", "literal", "int_lit", "str_lit",
  "bool_lit", "nothing_lit", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293
};
# endif

#define YYPACT_NINF -68

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-68)))

#define YYTABLE_NINF -58

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
       6,     5,    22,   -68,   -68,    28,   -68,   -68,   -68,     6,
     -68,   -68,    34,    76,   -68,   -68,    30,   -68,   103,   103,
     103,   103,   -68,   -68,   -68,   -68,   -68,   -68,   -68,   -68,
      79,     1,   -68,   -68,   -68,   -68,   -68,    76,   -68,    44,
      -8,   -68,    55,    45,   146,   146,    71,   146,   128,    56,
     -68,    67,   -68,    57,   -68,   103,   103,   103,   103,   103,
     103,   103,   103,   103,   103,   103,   -68,    60,    62,   -68,
      63,   -68,    78,    78,   -68,   -68,   -68,    90,    36,   -68,
     -68,   103,   164,   172,   172,    -6,    -6,    -6,   -68,   190,
     190,     9,     9,    80,   -68,   -68,   -68,   -68,   -68,   -68,
     103,   -68,   146,    -5,   164,   -68,   100,   -68,     4,   102,
     103,   -68,    73,   -68,    43,   103,   -68,    98,   -68,   146,
      34,   -68,   146,   -68,   -68,    78,    78,    96,   -68,   -68,
      85,   -68,   115,   -68,    78,   -68,   -68
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,     0,     0,     2,     5,     0,     8,     1,    31,     4,
      18,     7,     0,     3,     6,    31,     0,     9,     0,     0,
      42,     0,    53,    84,    85,    86,    81,    82,    83,    32,
      56,     0,    55,    77,    78,    79,    80,    19,    13,     0,
       0,    15,    11,    56,    33,    36,     0,    43,     0,     0,
      39,     0,    41,     0,    59,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    21,     0,     0,    14,
       0,    10,     0,     0,    38,    64,    45,     0,     0,    58,
      54,     0,    61,    62,    63,    70,    71,    72,    65,    66,
      67,    68,    69,    22,    17,    16,    12,    29,    34,    37,
       0,    73,    75,     0,    60,    20,     0,    31,    46,     0,
       0,    74,     0,    23,     0,     0,    35,     0,    40,    76,
       0,    30,    50,    47,    24,     0,     0,    27,    51,    48,
       0,    25,    49,    28,     0,    52,    26
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -68,   -68,   -68,   116,   -68,   -68,   -68,   -68,   -68,   -68,
      14,   -68,    75,   -68,   -68,   -68,   -68,   -68,   -68,   -68,
     -68,   -68,   -67,   -68,   -10,   -68,   -68,   -68,   -68,   -68,
     -68,   -68,   -68,   -68,    16,   -68,   -68,   -13,   -17,   -68,
     -68,   -68,   -68,   -68,   -68,   -68,   -68,   -68
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     8,     3,     9,     4,     5,    12,    42,    71,
      17,    40,    41,    11,    15,    66,    93,   106,   113,   127,
     134,   131,    98,   107,    13,    29,    72,   108,    73,    77,
      46,    50,   116,   126,   117,   125,   132,    43,    31,    51,
      81,    79,   103,    32,    33,    34,    35,    36
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      30,    44,    45,    47,    48,    37,    99,    68,    53,     1,
     110,   115,   -49,    69,    52,    53,   111,    54,    55,    56,
      57,    61,     7,    53,    30,    58,    59,    60,    61,    62,
      63,    64,    65,    58,    59,    60,    61,     6,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    18,
      10,    38,    19,    20,    16,    67,    21,   101,   128,   129,
      70,   102,    39,    21,   104,   -57,   121,   136,    22,    23,
      24,    25,    26,    27,    28,    22,    23,    24,    25,    26,
      27,    28,    18,   109,    74,    19,    20,    78,    76,    80,
      49,   -44,    94,   119,    39,    96,    21,   114,   122,   -57,
      97,    30,   100,   105,   112,   120,   123,   130,    22,    23,
      24,    25,    26,    27,    28,   118,    53,   133,    54,    55,
      56,    57,   115,    21,     0,    14,    58,    59,    60,    61,
      62,    63,    64,    65,   124,    22,    23,    24,    25,    26,
      27,    28,    53,    95,    54,    55,    56,    57,   135,    75,
       0,     0,    58,    59,    60,    61,    62,    63,    64,    65,
      53,     0,    54,    55,    56,    57,     0,     0,     0,     0,
      58,    59,    60,    61,    62,    63,    64,    65,    53,     0,
       0,     0,    56,    57,     0,     0,    53,     0,    58,    59,
      60,    61,    62,    63,    64,    65,    58,    59,    60,    61,
      62,    63,    64,    65,    53,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    58,    59,    60,    61,     0,     0,
      64,    65
};

static const yytype_int16 yycheck[] =
{
      13,    18,    19,    20,    21,    15,    73,    15,    14,     3,
      15,     7,     8,    21,    13,    14,    21,    16,    17,    18,
      19,    27,     0,    14,    37,    24,    25,    26,    27,    28,
      29,    30,    31,    24,    25,    26,    27,    32,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,     6,
      22,    21,     9,    10,    20,    11,    20,    21,   125,   126,
       5,    78,    32,    20,    81,    20,    23,   134,    32,    33,
      34,    35,    36,    37,    38,    32,    33,    34,    35,    36,
      37,    38,     6,   100,    13,     9,    10,    20,    32,    32,
      11,    12,    32,   110,    32,    32,    20,   107,   115,    20,
      22,   114,    12,    23,     4,    32,     8,    11,    32,    33,
      34,    35,    36,    37,    38,    13,    14,    32,    16,    17,
      18,    19,     7,    20,    -1,     9,    24,    25,    26,    27,
      28,    29,    30,    31,   120,    32,    33,    34,    35,    36,
      37,    38,    14,    68,    16,    17,    18,    19,   132,    21,
      -1,    -1,    24,    25,    26,    27,    28,    29,    30,    31,
      14,    -1,    16,    17,    18,    19,    -1,    -1,    -1,    -1,
      24,    25,    26,    27,    28,    29,    30,    31,    14,    -1,
      -1,    -1,    18,    19,    -1,    -1,    14,    -1,    24,    25,
      26,    27,    28,    29,    30,    31,    24,    25,    26,    27,
      28,    29,    30,    31,    14,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    24,    25,    26,    27,    -1,    -1,
      30,    31
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,    40,    42,    44,    45,    32,     0,    41,    43,
      22,    52,    46,    63,    42,    53,    20,    49,     6,     9,
      10,    20,    32,    33,    34,    35,    36,    37,    38,    64,
      76,    77,    82,    83,    84,    85,    86,    63,    21,    32,
      50,    51,    47,    76,    77,    77,    69,    77,    77,    11,
      70,    78,    13,    14,    16,    17,    18,    19,    24,    25,
      26,    27,    28,    29,    30,    31,    54,    11,    15,    21,
       5,    48,    65,    67,    13,    21,    32,    68,    20,    80,
      32,    79,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    55,    32,    51,    32,    22,    61,    61,
      12,    21,    77,    81,    77,    23,    56,    62,    66,    77,
      15,    21,     4,    57,    63,     7,    71,    73,    13,    77,
      32,    23,    77,     8,    49,    74,    72,    58,    61,    61,
      11,    60,    75,    32,    59,    73,    61
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    39,    41,    40,    42,    43,    42,    44,    46,    47,
      45,    48,    48,    49,    49,    50,    50,    51,    53,    54,
      52,    55,    56,    55,    58,    59,    57,    60,    60,    62,
      61,    63,    63,    65,    66,    64,    67,    64,    64,    68,
      64,    64,    69,    69,    70,    70,    71,    72,    71,    73,
      74,    75,    73,    76,    76,    77,    77,    78,    77,    79,
      77,    77,    77,    77,    77,    77,    77,    77,    77,    77,
      77,    77,    77,    80,    80,    81,    81,    82,    82,    82,
      82,    83,    84,    84,    85,    85,    86
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     3,     0,     0,     3,     2,     0,     0,
       6,     0,     2,     2,     3,     1,     3,     3,     0,     0,
       6,     0,     0,     3,     0,     0,     7,     0,     2,     0,
       4,     0,     2,     0,     0,     6,     0,     4,     3,     0,
       6,     2,     0,     1,     0,     2,     0,     0,     4,     0,
       0,     0,     6,     1,     3,     1,     1,     0,     3,     0,
       4,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     2,     3,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 55 "quack.y" /* yacc.c:1646  */
    { GetDB()->FinalProgram = ClassRSM()-> FinalizeStack(); StmtRSM()->SetReg(new SequentialStatement(LastSyntax()));}
#line 1389 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 56 "quack.y" /* yacc.c:1646  */
    {
										GetDB()->Root =MakeSemanticNode("start",(yyvsp[-2].semantic),(yyvsp[0].semantic));
										GetDB()->ProgramStatements = StmtRSM()->FinalizeRegister();
									}
#line 1398 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 62 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("class_declarations");}
#line 1404 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 63 "quack.y" /* yacc.c:1646  */
    { ClassRSM()->PushReg(); }
#line 1410 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 64 "quack.y" /* yacc.c:1646  */
    {
										(yyval.semantic) = MakeSemanticNode("class_declarations",(yyvsp[-2].semantic),(yyvsp[0].semantic));  
									}
#line 1418 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 69 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("class_declaration",(yyvsp[-1].semantic),(yyvsp[0].semantic));
									}
#line 1425 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 73 "quack.y" /* yacc.c:1646  */
    {ClassRSM()->SetReg(new ClassClass((yyvsp[0].syntax)));}
#line 1431 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 73 "quack.y" /* yacc.c:1646  */
    {ClassRSM()->Top()->Arguments = ArgsRSM()->FinalizeStack();}
#line 1437 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 74 "quack.y" /* yacc.c:1646  */
    {
										FUNC_LOG("class_signature");	
										(yyval.semantic) = MakeSemanticNode("class_signature",(yyvsp[-5].syntax),(yyvsp[-4].syntax),(yyvsp[-2].semantic),(yyvsp[0].semantic));
									}
#line 1446 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 80 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("extends");		}
#line 1452 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 81 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("extends",(yyvsp[-1].syntax),(yyvsp[0].syntax));	
									/* Set Class Inheritance here. */
										ClassRSM()->Top()->SetParent((yyvsp[0].syntax)->TextMatch);
										
									}
#line 1462 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 88 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("formal_args",(yyvsp[-1].syntax),(yyvsp[0].syntax));	/* Set the current formal_args value to new */ }
#line 1468 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 89 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("formal_args",(yyvsp[-2].syntax),(yyvsp[-1].semantic),(yyvsp[0].syntax));	}
#line 1474 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 92 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("formal_arg_list",(yyvsp[0].semantic)); /* Get the current arg_list and add named type to it*/ }
#line 1480 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 93 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("formal_arg_list",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic));	/* Get the current arg_list and add named type to it*/}
#line 1486 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 96 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("formal_arg",(yyvsp[-2].syntax),(yyvsp[-1].syntax),(yyvsp[0].syntax)); 
									ArgsRSM()->Push(new NamedType((yyvsp[-2].syntax), (yyvsp[-2].syntax)->TextMatch, (yyvsp[0].syntax)->TextMatch));
									}
#line 1494 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 101 "quack.y" /* yacc.c:1646  */
    {StmtRSM()->SetReg(new SequentialStatement((yyvsp[0].syntax)));}
#line 1500 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 101 "quack.y" /* yacc.c:1646  */
    { ClassRSM()->Top()->Statements = StmtRSM()->FinalizeRegister(); }
#line 1506 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 102 "quack.y" /* yacc.c:1646  */
    {
										(yyval.semantic) = MakeSemanticNode("class_body",(yyvsp[-5].syntax),(yyvsp[-3].semantic),(yyvsp[-1].semantic), (yyvsp[0].syntax));	
										ClassRSM()->Top()->Methods = MethRSM()->FinalizeStack();
										/*set class stm_list and mtd_list */
									}
#line 1516 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 109 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("method_list"); /*no action needed here, current list has mthods.*/}
#line 1522 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 110 "quack.y" /* yacc.c:1646  */
    {MethRSM()->SetReg(new MethodClass(LastSyntax())); }
#line 1528 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 111 "quack.y" /* yacc.c:1646  */
    {
										(yyval.semantic) = MakeSemanticNode("method_list",(yyvsp[-2].semantic),(yyvsp[0].semantic)); /* add current method to current method list*/ 
										MethRSM()->PushReg();									
									}
#line 1537 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 117 "quack.y" /* yacc.c:1646  */
    {MethRSM()->Top()->Arguments = ArgsRSM()->FinalizeStack();}
#line 1543 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 118 "quack.y" /* yacc.c:1646  */
    { StmtRSM()->SetReg(new SequentialStatement((yyvsp[-4].syntax)));}
#line 1549 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 119 "quack.y" /* yacc.c:1646  */
    {
										(yyval.semantic) = MakeSemanticNode("method",(yyvsp[-6].syntax),(yyvsp[-5].syntax),(yyvsp[-4].semantic),(yyvsp[-2].semantic),(yyvsp[0].semantic));
											
										MethRSM()->Top()->Statements = StmtRSM()->FinalizeRegister();
										std::string default_return = "$Bottom";

										if((yyvsp[-2].semantic)->Children.size() == 2)
										{
											default_return = ((SyntaxNode*)((yyvsp[-2].semantic)->Children[1]))->TextMatch;
										}
										MethRSM()->Top()->NameReturn = new NamedType((yyvsp[-5].syntax),(yyvsp[-5].syntax)->TextMatch,default_return);
									}
#line 1566 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 133 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("returntype"); /*set current named type to Obj, may not need to be done.*/ }
#line 1572 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 134 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("returntype",(yyvsp[-1].syntax),(yyvsp[0].syntax)); /* set current named type to Identifier*/ }
#line 1578 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 137 "quack.y" /* yacc.c:1646  */
    { 
										StmtRSM()->PushReg(); 
										StmtRSM()->SetReg(new SequentialStatement((yyvsp[0].syntax)));}
#line 1586 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 141 "quack.y" /* yacc.c:1646  */
    {
										FUNC_LOG("statement_block");
										(yyval.semantic) = MakeSemanticNode("statement_block",(yyvsp[-3].syntax),(yyvsp[-1].semantic),(yyvsp[0].syntax));
										SequentialStatement *ss = StmtRSM()->GetReg(); 
										StmtRSM()->PopReg();
										StmtRSM()->Top()->Sequence.push_back(ss);
									}
#line 1598 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 150 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("statement_list");}
#line 1604 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 151 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("statement_list",(yyvsp[-1].semantic),(yyvsp[0].semantic));
									}
#line 1611 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 155 "quack.y" /* yacc.c:1646  */
    { 
										StmtRSM()->Top()->Sequence.push_back(new IfElseListStatement((yyvsp[-1].syntax)));
										ExpRSM()->PushReg();
										StmtRSM()->PushReg(); 
										StmtRSM()->SetReg(new SequentialStatement((yyvsp[-1].syntax)));
									 }
#line 1622 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 161 "quack.y" /* yacc.c:1646  */
    {
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										ExpRSM()->PopReg();
										RExpression *rexp = (RExpression*) ExpRSM()->GetReg();
										IfElseListStatement* exp = (IfElseListStatement*)(StmtRSM()->Top()->Sequence.back());
										exp->Add(rexp,ss);
  									}
#line 1635 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 170 "quack.y" /* yacc.c:1646  */
    {	
										FUNC_LOG("If Statement");
										(yyval.semantic) = MakeSemanticNode("statement",(yyvsp[-5].syntax),(yyvsp[-4].semantic),(yyvsp[-2].semantic),(yyvsp[0].semantic));
									}
#line 1644 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 174 "quack.y" /* yacc.c:1646  */
    { 
										ExpRSM()->PushReg(); 
										StmtRSM()->PushReg(); 
										StmtRSM()->SetReg(new SequentialStatement((yyvsp[-1].syntax)));  }
#line 1653 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 179 "quack.y" /* yacc.c:1646  */
    {
										FUNC_LOG("While Statement");
										(yyval.semantic) = MakeSemanticNode("statement",(yyvsp[-3].syntax),(yyvsp[-2].semantic),(yyvsp[0].semantic));
										ExpRSM()->PopReg();
										RExpression *rexp = (RExpression *)ExpRSM()->GetReg();
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										StmtRSM()->Top()->Sequence.push_back(new WhileStatement((yyvsp[-3].syntax),rexp,ss));
									}
#line 1667 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 189 "quack.y" /* yacc.c:1646  */
    {
										FUNC_LOG("Return Statment");
										(yyval.semantic) = MakeSemanticNode("statement",(yyvsp[-2].syntax),(yyvsp[-1].semantic),(yyvsp[0].syntax)); 
										StmtRSM()->Top()->Sequence.push_back(new ReturnStatement((yyvsp[-2].syntax), (RExpression*)ExpRSM()->FinalizeRegister()));
									 }
#line 1677 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 195 "quack.y" /* yacc.c:1646  */
    {ExpRSM()->PushReg();}
#line 1683 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 197 "quack.y" /* yacc.c:1646  */
    {
										FUNC_LOG("Assignment Statement");
										(yyval.semantic) = MakeSemanticNode("statement",(yyvsp[-5].semantic),(yyvsp[-4].semantic),(yyvsp[-2].syntax),(yyvsp[-1].semantic),(yyvsp[0].syntax));
										RExpression *rexp = (RExpression *)ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										LExpression *lexp = (LExpression *)ExpRSM()->GetReg();
										StmtRSM()->Top()->Sequence.push_back(new AssignmentStatement((yyvsp[-2].syntax),lexp,rexp));
									}
#line 1696 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 206 "quack.y" /* yacc.c:1646  */
    {
										FUNC_LOG("Single Statement");
										(yyval.semantic) = MakeSemanticNode("statement",(yyvsp[-1].semantic),(yyvsp[0].syntax));
										StmtRSM()->Top()->Sequence.push_back(new SingleStatement((yyvsp[0].syntax),(RExpression*)ExpRSM()->FinalizeRegister()));
									}
#line 1706 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 213 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("return_stmt"); }
#line 1712 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 214 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("return_stmt",(yyvsp[0].semantic));}
#line 1718 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 217 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("index_id");}
#line 1724 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 218 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("index_id",(yyvsp[-1].syntax),(yyvsp[0].syntax));}
#line 1730 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 221 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("else_list");}
#line 1736 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 222 "quack.y" /* yacc.c:1646  */
    {	StmtRSM()->PushReg(); StmtRSM()->SetReg(new SequentialStatement((yyvsp[0].syntax))); }
#line 1742 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 224 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("else_list",(yyvsp[-3].semantic),(yyvsp[-2].syntax),(yyvsp[0].semantic));
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										IfElseListStatement* exp = (IfElseListStatement*)(StmtRSM()->Top()->Sequence.back());
										exp->Add(NULL,ss);
									}
#line 1753 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 232 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("elseiflist");}
#line 1759 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 233 "quack.y" /* yacc.c:1646  */
    { ExpRSM()->PushReg(); StmtRSM()->PushReg(); StmtRSM()->SetReg(new SequentialStatement((yyvsp[-1].syntax)));}
#line 1765 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 235 "quack.y" /* yacc.c:1646  */
    {
										SequentialStatement *ss = StmtRSM()->GetReg();
										StmtRSM()->PopReg();
										ExpRSM()->PopReg();
										RExpression *rexp = (RExpression*) ExpRSM()->GetReg();
										IfElseListStatement* exp = (IfElseListStatement*)(StmtRSM()->Top()->Sequence.back());
										exp->Add(rexp,ss);
									}
#line 1778 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 243 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("elseiflist",(yyvsp[-5].syntax),(yyvsp[-4].semantic),(yyvsp[-2].semantic),(yyvsp[0].semantic)); }
#line 1784 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 246 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("l_exp",(yyvsp[0].syntax)); 
										ExpRSM()->SetReg(new VariableIdentifier((yyvsp[0].syntax),(yyvsp[0].syntax)->TextMatch));}
#line 1791 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 248 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("l_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].syntax)); }
#line 1797 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 251 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[0].semantic)); /* */}
#line 1803 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 252 "quack.y" /* yacc.c:1646  */
    {	(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[0].semantic)); /* */}
#line 1809 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 253 "quack.y" /* yacc.c:1646  */
    {
										ExpRSM()->SetReg(new FunctionCall(LastSyntax(),(LExpression*)ExpRSM()->GetReg())); 
									}
#line 1817 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 257 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[0].semantic)); 
												FunctionCall *ptr = (FunctionCall*)ExpRSM()->Top();
												//ptr-> Arguments = ExpArgRSM->FinalizeVector();
											}
#line 1826 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 261 "quack.y" /* yacc.c:1646  */
    { ExpRSM()->PushReg();}
#line 1832 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 263 "quack.y" /* yacc.c:1646  */
    {
										(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-3].semantic),(yyvsp[-2].syntax),(yyvsp[0].semantic)); 
										RExpression* right_side = (RExpression*)ExpRSM()->GetReg();
										ExpRSM()->PopReg();
										RExpression* left_side = (RExpression*)ExpRSM()->GetReg();
										ExpRSM()->SetReg(new MathExpression(left_side,"+",right_side));
									}
#line 1844 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 270 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1850 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 271 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1856 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 272 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1862 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 273 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].syntax),(yyvsp[-1].semantic),(yyvsp[0].syntax)); }
#line 1868 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 274 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1874 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 275 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1880 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 276 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1886 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 277 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1892 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 278 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1898 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 279 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1904 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 280 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1910 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 281 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("r_exp",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic)); }
#line 1916 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 284 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("actual_args",(yyvsp[-1].syntax),(yyvsp[0].syntax));}
#line 1922 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 285 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("actual_args",(yyvsp[-2].syntax),(yyvsp[-1].semantic),(yyvsp[0].syntax));}
#line 1928 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 288 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("actual_arg_list",(yyvsp[0].semantic));}
#line 1934 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 289 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("actual_arg_list",(yyvsp[-2].semantic),(yyvsp[-1].syntax),(yyvsp[0].semantic));}
#line 1940 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 293 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("literal",(yyvsp[0].semantic));  }
#line 1946 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 294 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("literal",(yyvsp[0].semantic));  }
#line 1952 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 295 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("literal",(yyvsp[0].semantic));}
#line 1958 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 296 "quack.y" /* yacc.c:1646  */
    {(yyval.semantic) = MakeSemanticNode("literal",(yyvsp[0].semantic)); }
#line 1964 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 299 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("int_lit",(yyvsp[0].syntax));ExpRSM()->SetReg(new TypedValue((yyvsp[0].syntax),"Integer",(yyvsp[0].syntax)->TextMatch));}
#line 1970 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 302 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("str_lit",(yyvsp[0].syntax));ExpRSM()->SetReg(new TypedValue((yyvsp[0].syntax),"String",(yyvsp[0].syntax)->TextMatch));}
#line 1976 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 303 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("str_lit",(yyvsp[0].syntax));ExpRSM()->SetReg(new TypedValue((yyvsp[0].syntax),"String",(yyvsp[0].syntax)->TextMatch));}
#line 1982 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 308 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("bool_false_lit",(yyvsp[0].syntax));  ExpRSM()->SetReg(new TypedValue((yyvsp[0].syntax),"Boolean",(yyvsp[0].syntax)->TextMatch));}
#line 1988 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 309 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("bool_true_lit",(yyvsp[0].syntax));  ExpRSM()->SetReg(new TypedValue((yyvsp[0].syntax),"Boolean",(yyvsp[0].syntax)->TextMatch));}
#line 1994 "quack.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 313 "quack.y" /* yacc.c:1646  */
    { (yyval.semantic) = MakeSemanticNode("nothing_lit",(yyvsp[0].syntax)); ExpRSM()->SetReg(new TypedValue((yyvsp[0].syntax),"Nothing",(yyvsp[0].syntax)->TextMatch));}
#line 2000 "quack.tab.c" /* yacc.c:1646  */
    break;


#line 2004 "quack.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 315 "quack.y" /* yacc.c:1906  */






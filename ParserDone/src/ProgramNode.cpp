#include "ProgramNode.hpp"

ProgramNode::ProgramNode()
{
	NodeLabel = "ProgramNode";
}
ProgramNode::~ProgramNode()
{
	for(std::vector<ProgramNode *>::iterator itr = Children.begin(); itr != Children.end(); itr++)
	{	
		delete (*itr);
	}
}

std::vector<GraphVizNodeDecorator *> ProgramNode::GenerateGraphvizChildren()
{
	std::vector<GraphVizNodeDecorator *> returnValue;
	for(std::vector<ProgramNode *>::iterator itr = Children.begin(); itr != Children.end(); itr++)
	{	
		returnValue.push_back(*itr);
	}
	return returnValue;
}

